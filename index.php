<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить профессиональную косметику из Южной Кореи в Институте имиджа и красоты Fijie (Фиджи). Узнать цены в Москве ☎ +7 (495) 695-25-34 ☎ +7 (495) 502-90-75");
$APPLICATION->SetPageProperty("keywords", "профессиональная косметика, институт красоты Fijie");
$APPLICATION->SetPageProperty("title", "Профессиональная корейская косметика - купить в интернет-магазине");
$APPLICATION->SetTitle("Интернет магазин профессиональной косметики от Института красоты Fijie");
?>

<?$APPLICATION->IncludeComponent(
    "nbrains:slider",
    "new_fijie",
    Array(
        "BTN_SLIDE_CONTROL" => "bottom",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "HEIGHT" => "700",
        "HIDDEN_ARROWS" => "false",
        "IBLOCK_ID" => SLIDER_IBLOCK_ID,
        "FIELD_CODE" => array(
            0 => "SHOW_BUTTON",
            1 => "URL_BUTTON",
            2 => "HEADER_COLOR"
        ),
        "PROPERTY_CODE" => array(
            0 => "SHOW_BUTTON",
            1 => "URL_BUTTON",
            2 => "HEADER_COLOR"
        ),
        "IBLOCK_TYPE" => "slider",
        "PROGRESS_BAR_COLOR" => "8FBB3F",
        "PROGRESS_BAR_HEIGHT" => "4",
        "PROGRESS_BAR_PLACE" => "bottom",
        "RIGHT_PX_TEXT" => "70",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "TIME_SLIDE" => "10",
        "TOP_PX_TEXT" => "135",
        "WIDTH" => "1920"
    )
);?>


<?$APPLICATION->IncludeComponent("fijie:products", ".default", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "PRODUCTS_IBLOCK_CODE" => "clothes",
    "NEW_PRODUCTS" => "Y",
    "BESTSELLERS" => "Y",
    "SALE_PRODUCTS" => "Y",
    "BY" => "AMOUNT",
    "COUNT_ITEMS" => 20,
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "",

    "ADD_SECTION_CHAIN" => "Y",
    "ADD_ELEMENT_CHAIN" => "Y",
    "SET_STATUS_404" => "N",
    "DETAIL_DISPLAY_NAME" => "N",
    "USE_ELEMENT_COUNTER" => "Y",
    "USE_FILTER" => "N",
    "FILTER_NAME" => "",
    "FILTER_VIEW_MODE" => "VERTICAL",
    "USE_COMPARE" => "N",
    "PRICE_CODE" => array(
        0 => "BASE",
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "PRICE_VAT_SHOW_VALUE" => "N",
    "PRODUCT_PROPERTIES" => "",
    "USE_PRODUCT_QUANTITY" => "Y",
    "CONVERT_CURRENCY" => "N",
    "QUANTITY_FLOAT" => "N",
    "OFFERS_CART_PROPERTIES" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
    ),
    "SHOW_TOP_ELEMENTS" => "N",
    "SECTION_COUNT_ELEMENTS" => "N",
    "SECTION_TOP_DEPTH" => "1",
    "SECTIONS_VIEW_MODE" => "TILE",
    "SECTIONS_SHOW_PARENT_NAME" => "N",
    "PAGE_ELEMENT_COUNT" => "15",
    "LINE_ELEMENT_COUNT" => "3",
    "ELEMENT_SORT_FIELD" => "desc",
    "ELEMENT_SORT_ORDER" => "asc",
    "ELEMENT_SORT_FIELD2" => "id",
    "ELEMENT_SORT_ORDER2" => "desc",
    "LIST_PROPERTY_CODE" => array(
        0 => "NEWPRODUCT",
        1 => "SALELEADER",
        2 => "SPECIALOFFER",
        3 => "ML",
    ),
    "INCLUDE_SUBSECTIONS" => "Y",
    "LIST_META_KEYWORDS" => "UF_KEYWORDS",
    "LIST_META_DESCRIPTION" => "UF_META_DESCRIPTION",
    "LIST_BROWSER_TITLE" => "UF_BROWSER_TITLE",
    "LIST_OFFERS_FIELD_CODE" => array(
        0 => "NAME",
        1 => "PREVIEW_PICTURE",
        2 => "DETAIL_PICTURE",
        3 => "",
    ),
    "PROPERTY_CODE" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
        3 => "MORE_PHOTO_LIST",
        4 => "MK",
    ),
    "LIST_OFFERS_PROPERTY_CODE" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
        3 => "MORE_PHOTO_LIST",
        4 => "ARTNUMBER",
        5 => "",
    ),
    "LIST_OFFERS_LIMIT" => "0",
    "SECTION_BACKGROUND_IMAGE" => "UF_BACKGROUND_IMAGE",
    "DETAIL_PROPERTY_CODE" => array(
        0 => "NEWPRODUCT",
        1 => "MANUFACTURER",
        2 => "MATERIAL",
    ),
    "DETAIL_META_KEYWORDS" => "KEYWORDS",
    "DETAIL_META_DESCRIPTION" => "META_DESCRIPTION",
    "DETAIL_BROWSER_TITLE" => "TITLE",
    "DETAIL_OFFERS_FIELD_CODE" => array(
        0 => "NAME",
        1 => "",
    ),
    "DETAIL_OFFERS_PROPERTY_CODE" => array(
        0 => "ARTNUMBER",
        1 => "SIZES_SHOES",
        2 => "SIZES_CLOTHES",
        3 => "COLOR_REF",
        4 => "MORE_PHOTO_LIST",
        5 => "",
    ),
    "DETAIL_BACKGROUND_IMAGE" => "BACKGROUND_IMAGE",
    "LINK_IBLOCK_TYPE" => "",
    "LINK_IBLOCK_ID" => "",
    "LINK_PROPERTY_SID" => "",
    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
    "USE_ALSO_BUY" => "Y",
    "ALSO_BUY_ELEMENT_COUNT" => "4",
    "ALSO_BUY_MIN_BUYES" => "1",
    "OFFERS_SORT_FIELD" => "sort",
    "OFFERS_SORT_ORDER" => "desc",
    "OFFERS_SORT_FIELD2" => "id",
    "OFFERS_SORT_ORDER2" => "desc",
    "PAGER_TEMPLATE" => "round",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Товары",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
    "PAGER_SHOW_ALL" => "N",
    "ADD_PICT_PROP" => "MORE_PHOTO_LIST",
    "LABEL_PROP" => array(
        0 => "NEWPRODUCT",
    ),
    "PRODUCT_DISPLAY_MODE" => "Y",
    "OFFER_ADD_PICT_PROP" => "MORE_PHOTO_LIST",
    "OFFER_TREE_PROPS" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
        3 => "",
    ),
    "SHOW_DISCOUNT_PERCENT" => "N",
    "SHOW_OLD_PRICE" => "Y",
    "MESS_BTN_BUY" => "Купить",
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
    "MESS_BTN_COMPARE" => "Сравнение",
    "MESS_BTN_DETAIL" => "Подробнее",
    "MESS_NOT_AVAILABLE" => "Нет в наличии",
    "DETAIL_USE_VOTE_RATING" => "Y",
    "DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
    "DETAIL_USE_COMMENTS" => "Y",
    "DETAIL_BLOG_USE" => "Y",
    "DETAIL_VK_USE" => "N",
    "DETAIL_FB_USE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "USE_STORE" => "Y",
    "BIG_DATA_RCM_TYPE" => "personal",
    "FIELDS" => array(
        0 => "SCHEDULE",
        1 => "STORE",
        2 => "",
    ),
    "USE_MIN_AMOUNT" => "N",
    "STORE_PATH" => "/store/#store_id#",
    "MAIN_TITLE" => "Наличие на складах",
    "MIN_AMOUNT" => "10",
    "DETAIL_BRAND_USE" => "Y",
    "DETAIL_BRAND_PROP_CODE" => array(
        0 => "BRAND_REF",
        1 => "",
    ),
    "COMPATIBLE_MODE" => "N",
    "SIDEBAR_SECTION_SHOW" => "N",
    "SIDEBAR_DETAIL_SHOW" => "N",
    "SIDEBAR_PATH" => "/catalog/sidebar.php",
    "COMPONENT_TEMPLATE" => "catalog_card",
    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
    "LABEL_PROP_MOBILE" => array(
    ),

    "PROPERTY_CODE_MOBILE" => array(
    ),
    "PRODUCT_BLOCKS_ORDER" => "props,price,sku,quantityLimit,quantity,buttons",
    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
    "ENLARGE_PRODUCT" => "STRICT",
    "SHOW_SLIDER" => "Y",
    "SLIDER_INTERVAL" => "3000",
    "SLIDER_PROGRESS" => "N",
),
    false
);?>

<!--About-->
<section class="about_us" data-aos="fade-up" data-aos-delay="200">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="about_title" data-aos="fade-up" data-aos-delay="600"> <img src="<?=SITE_TEMPLATE_PATH?>/img/about_001.jpg" alt="" data-aos="fade-right" data-aos-delay="1000">
                    <div class="title">О Fijie</div>
                </div>
                <p data-aos="fade-left" data-aos-delay="800">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/about/index_about1.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </p>
                <p data-aos="fade-left" data-aos-delay="900">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/about/index_about2.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                 </p>
            </div>
            <div class="col-6">
                <div class="play_video" name="video" data-aos="fade-left" data-aos-delay="1000">
                    <div class="play_icon" data-fancybox>
                        <a data-fancybox="" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk">Воспроизвести видео</a>
                    </div>
                    <div class="video_title" data-fancybox>
                        <a data-fancybox="" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk">Смотреть видео о нас</a>
                    </div>
                </div>
                <p data-aos="fade-left" data-aos-delay="1200">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/about/index_about3.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </p>
                <div class="about_bottom" data-aos="fade-up" data-aos-delay="600">
                    <div class="overlay" data-aos="fade-up" data-aos-delay="300"> <a href="/about/" class="btn white iconlink">О магазине <span class="lnr lnr-arrow-right"></span></a> </div>
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/about_002.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<!--Advantages-->
<section class="advantages">
    <div class="container">
        <div class="row">
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="200">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="300">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages1.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="400">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages2.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="500">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages3.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-12 center" data-aos="fade-up" data-aos-delay="600">
                <hr>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6 center" data-aos="fade-up" data-aos-delay="200">
                <p>Принимаем к оплате</p>
                <img src="<?=SITE_TEMPLATE_PATH?>/img/pay.png" alt="">
            </div>
            <div class="col-6 center" data-aos="fade-up" data-aos-delay="300">
                <p>Доставка товара</p>
                <img src="<?=SITE_TEMPLATE_PATH?>/img/sdekresize.jpeg"  width="179" height="25" alt="">
            </div>
        </div>
    </div>
</section>

<!--Article post-->
<section class="article_post">
    <div class="container">
        <div class="row">
            <div class="col-6 left_layout" data-aos="fade-right" data-aos-delay="500"> <img src="<?=SITE_TEMPLATE_PATH?>/img/cell_fusion.png" alt="" class="js-tilt" data-tilt-perspective="1500"> </div>
            <div class="col-6 center right_layout" data-aos="fade-left" data-aos-delay="1000">
                <div class="article_wrapper">
                    <div class="overflow">
                        <div class="title" data-aos="fade-down" data-aos-delay="1200">
                            <?$APPLICATION->IncludeFile(
                                SITE_TEMPLATE_PATH."/include/article/index_article_title.php",
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </div>
                        <div class="descr" data-aos="fade-up" data-aos-delay="1300">
                            <?$APPLICATION->IncludeFile(
                                SITE_TEMPLATE_PATH."/include/article/index_article_descr.php",
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </div>
                        <div class="link_pulse" data-aos="fade-up" data-aos-delay="1400">
                            <?$APPLICATION->IncludeFile(
                                SITE_TEMPLATE_PATH."/include/article/index_article_button.php",
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?$APPLICATION->IncludeComponent("fijie:brands", ".default", array(
    "SORT_FIELD" => "SORT",
    "SORT_BY" => "ASC",
    "BRANDS_IBLOCK_CODE" => "brands",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "8"
),
    false
);?>


<?$APPLICATION->IncludeComponent("fijie:blog.message.list", "rowblock", array(
    "SORT_FIELD" => "ACTIVE_FROM",
    "SORT_BY" => "DESC",
    "BRANDS_IBLOCK_CODE" => "blog",
    "CACHE_TYPE" => "A",
    "FILTER" => ["PROPERTY_TYPEIMG_VALUE"=>"Горизонтальная"],
    "COUNT_RECORDS" => "3",
    "CACHE_TIME" => "3600",
),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>