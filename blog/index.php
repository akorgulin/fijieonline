<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use Bitrix\Main\Application,
	Bitrix\Main\Context,
	Bitrix\Main\Request,
	Bitrix\Main\Server;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
// Или более краткая форма:
$request = Context::getCurrent()->getRequest();

if($request->get("bxajaxid")) {
	$APPLICATION->restartBuffer();
} else {
	$APPLICATION->SetTitle("Блог");
}
$blog = "fijiblognew";
$category = "";
$day = "";
$month = "";
$year = "";

?>

<?if($request->get("id")) {
	$_REQUEST["id"] = $request->get("id");?>
    <?$APPLICATION->IncludeComponent("fijie:blog.post.detail", "", array(
        "SORT_FIELD" => "ACTIVE_FROM",
        "SORT_BY" => "DESC",
        "BRANDS_IBLOCK_CODE" => "blog",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "COUNT_RECORDS" => "",
    ),
        false
    );?>
    <?
} else { ?>
	<?$APPLICATION->IncludeComponent("fijie:blog.message.list", "", array(
		"SORT_FIELD" => "ACTIVE_FROM",
		"SORT_BY" => "DESC",
		"BRANDS_IBLOCK_CODE" => "blog",
		"CACHE_TYPE" => "A",
		"COUNT" => "12",
		"CACHE_TIME" => "3600",
		"COUNT_RECORDS" => "",
	),
		false
	);?>
<?}?>

<?
if($request->get("bxajaxid")) {

} else {
?>
<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
        "USE_PERSONALIZATION" => "Y",
        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
        "SHOW_HIDDEN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<? } ?>
