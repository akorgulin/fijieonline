<?
if(!$_REQUEST["id"]) {
    define("ERROR_404","Y");
    define("BLOG_DETAIL","Y");
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


if($_REQUEST["id"]) {?>
    <?$APPLICATION->IncludeComponent("fijie:blog.post.detail", "", array(
        "SORT_FIELD" => "ACTIVE_FROM",
        "SORT_BY" => "DESC",
        "BRANDS_IBLOCK_CODE" => "blog",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "COUNT_RECORDS" => "",
    ),
        false
    );?>
	<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
            "USE_PERSONALIZATION" => "Y",
            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
            "SHOW_HIDDEN" => "Y",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "3600"
        )
	);?><?
}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>