<?
define("HIDE_SIDEBAR", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Избранные товары");
?>

<!--Favorites-->
<section class="content_wrapper nobackground clearfix">
    <?$APPLICATION->IncludeComponent(
        "h2o:favorites.list",
        "new",
        array(
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => "2",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "Y",
            "DATE_FORMAT" => "d.m.Y",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_TOP_PAGER" => "Y",
            "FAVORITES_COUNT" => "20",
            "FILTER_NAME" => "",
            "NAV_TEMPLATE" => "",
            "SET_TITLE" => "Y",
            "SORT_BY" => "DATE_INSERT",
            "SORT_ORDER" => "DESC",
            "COMPONENT_TEMPLATE" => "new",
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => CATALOG_IBLOCK_ID,
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "Y",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER2" => "desc",
            "BASKET_URL" => "/personal/cart/",
            "PRICE_CODE" => array(
				"0" => "BASE"
            ),
			"OFFERS_CART_PROPERTIES" => array(
                0 => "SIZES_SHOES",
				1 => "SIZES_CLOTHES",
				2 => "COLOR_REF",
                3 => "ML",
            ),
            "PROPERTY_CODE" => array(
                0 => "NEWPRODUCT",
                1 => "SALELEADER",
                2 => "SPECIALOFFER",
            ),
            "OFFERS_FIELD_CODE" => array(
                0 => "NEWPRODUCT",
				1 => "PREVIEW_PICTURE",
				2 => "DETAIL_PICTURE",
                3 => "SALELEADER",
                4 => "SPECIALOFFER",
				5 => "NAME",
            ),
            "OFFERS_PROPERTY_CODE" => array(
                0 => "SIZES_SHOES",
                1 => "SIZES_CLOTHES",
                2 => "COLOR_REF",
                3 => "MORE_PHOTO_LIST",
                4 => "ARTNUMBER",
				5 => "DESCRIPTION",

            ),
			"OFFER_TREE_PROPS" => array(
                0 => "SIZES_SHOES",
                1 => "SIZES_CLOTHES",
                2 => "COLOR_REF",
			),
            "OFFERS_LIMIT" => "0",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPERTIES" => array(
            )
        ),
        false
    );?>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>