<?
define("HIDE_SIDEBAR", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Sale;
use Bitrix\Main;

//Обновляем корзинное колличество товара на актуальное
foreach($_POST["order"] as $keyStr=>$val) {

	if(strstr($keyStr,"quantity_")) {
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("catalog");
		CModule::IncludeModule("sale");

		$basket = Sale\Basket::loadItemsForFUser(
			Sale\Fuser::getId(),
			Main\Context::getCurrent()->getSite()
		);

		/** @var Sale\BasketItem $basketItem */
		foreach ($basket as $basketItem) {
			$basketItem->setField('QUANTITY',$_POST["order"]["quantity_".$basketItem->getId()]);
		}

		$basket->save();
	}

}

?>
<?$APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "", array(
    "PAY_FROM_ACCOUNT" => "Y",
    "COUNT_DELIVERY_TAX" => "N",
    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
    "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
    "ALLOW_AUTO_REGISTER" => "Y",
    "SEND_NEW_USER_NOTIFY" => "Y",
    "DELIVERY_NO_AJAX" => "Y",
    "TEMPLATE_LOCATION" => "popup",
    "PROP_1" => array(
    ),
    "PATH_TO_BASKET" => "/personal/cart/",
    "PATH_TO_PERSONAL" => "/personal/order/",
    "PATH_TO_PAYMENT" => "/personal/order/payment/",
    "PATH_TO_ORDER" => "/personal/order/make/",
    "SET_TITLE" => "N" ,
    "SHOW_ACCOUNT_NUMBER" => "Y",
    "DELIVERY_NO_SESSION" => "Y",
    "COMPATIBLE_MODE" => "Y",
    "BASKET_POSITION" => "before",
    "BASKET_IMAGES_SCALING" => "adaptive",
    "SERVICES_IMAGES_SCALING" => "adaptive",
    "USER_CONSENT" => "Y",
    "USER_CONSENT_ID" => "1",
    "USER_CONSENT_IS_CHECKED" => "Y",
    "USER_CONSENT_IS_LOADED" => "Y"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>