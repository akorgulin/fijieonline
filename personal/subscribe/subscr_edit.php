<?
define("NEED_AUTH", false);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
    <section class="content_wrapper">
        <div class="wrapper">
            <div class="grid x3 clearfix">
                <? $APPLICATION->IncludeComponent("fijie:subscribe.simple", ".default", Array(
                    "AJAX_MODE" => "N",
                    "SHOW_HIDDEN" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "SET_TITLE" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                )); ?>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>