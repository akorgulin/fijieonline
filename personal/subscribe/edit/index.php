<?
define("NEED_AUTH", false);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent("fijie:subscribe.simple", "empt", Array(
    "AJAX_MODE" => "N",
    "SHOW_HIDDEN" => "Y",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "SET_TITLE" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
));
?>