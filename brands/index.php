<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Бренды");
?>
<?$APPLICATION->IncludeComponent("fijie:brands.list", "brands", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "BRANDS_IBLOCK_CODE" => "brands",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "",
),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>