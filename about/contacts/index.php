<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задайте вопрос");
?>

    <section class="content_wrapper nobackground clearfix">
        <div class="container page_sidebar">
            <div class="row">
                <div class="col-8">
                    <h1 class="page_header" data-aos="fade-up" data-aos-delay="300">Контакты:</h1>
                    <p>
                        <b>Телефон:</b> 8 (903) 797 16 60
                    </p>
                    <p>
                        <b>Адрес:</b> г. Москва, пер. 7-ой Ростовский, д. 11
                    </p>
                    <p>
                        Название компании: ООО "ИНТЕРНЕТ-МАГАЗИН"СИЯНИЕ"<br>
                    </p>
                    <p>
                        Юридический Адрес:&nbsp;105005, г. Москва, Плетешковский переулок, д. 18-20, корп. 1, помещение 6П
                    </p>
                    <p>
                        Фактический адрес: 119121 г. Москва, пер. 7-ой Ростовский, д.11
                    </p>
                    <p>
                        ИНН: 7701403924<br>
                    </p>
                    <p>
                        КПП:&nbsp;770101001
                    </p>
                    <p>
                        ОГРН: 1147746898859<br>
                    </p>
                    <p>
                        Банк:&nbsp;ПАО "СБЕРБАНК РОССИИ", г. МОСКВА
                    </p>
                    <p>
                        БИК:&nbsp;044525225
                    </p>
                    <p>
                        Р/С:&nbsp;40702810338000006553
                    </p>
                    <p>
                    </p>
                    <p>
                        К/С:&nbsp;30101810400000000225</p>
                    <p>
                    </p>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2246.0285523310063!2d37.57091561593004!3d55.74063288055013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54bb96279d4eb%3A0x2a9897020f642af!2zNy3QuSDQoNC-0YHRgtC-0LLRgdC60LjQuSDQv9C10YAuLCAxMSwg0JzQvtGB0LrQstCwLCAxMTkxMjE!5e0!3m2!1sru!2sru!4v1556284285439!5m2!1sru!2sru"
                            width="640" height="490" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </iframe><br/>
                    <a href="https://maps.google.ru/maps?f=q&amp;source=embed&amp;hl=ru&amp;geocode=&amp;q=%D0%B3.+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+%D1%83%D0%BB.+2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F,+%D0%B4.+38%D0%90&amp;aq=&amp;sll=55,103&amp;sspn=90.84699,270.527344&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB.,+38,+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+127287&amp;ll=55.805478,37.569551&amp;spn=0.023154,0.054932&amp;z=14&amp;iwloc=A">
                        <br>
                        <small><a href="https://goo.gl/maps/QENBpwYuaP4h1ATG8">Просмотреть увеличенную карту</a></small>
                        <h2>Задать вопрос</h2>

    <?$APPLICATION->IncludeComponent(
        "fijie:main.feedback",
        "fijie",
        Array(
            "COMPONENT_TEMPLATE" => "fijie",
            "EMAIL_TO" => "info@fijieonline.ru",
            "EVENT_MESSAGE_ID" => array(0=>"7",),
            "OK_TEXT" => "Спасибо, ваше обращение принято.",
            "REQUIRED_FIELDS" => array(),
            "USE_CAPTCHA" => "N"
        )
    );?>
                </div>
                <div class="col-4 sidebar">
            <?$APPLICATION->IncludeComponent(
                "fijie:blog.message.list",
                "section",
                Array(
                    "BRANDS_IBLOCK_CODE" => "blog",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "COUNT_RECORDS" => "4",
                    "SORT_BY" => "DESC",
                    "SORT_FIELD" => "ACTIVE_FROM"
                )
            );?>
                </div>
            </div>
        </div>
    </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>