<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Задайте вопрос");
?><!--Category--> <section class="content_wrapper nobackground clearfix">
<div class="container page_sidebar">
	<div class="row">
		<div class="col-8">
			<h1 class="page_header" data-aos="fade-up" data-aos-delay="300">Контакты:</h1><ul>
				<li><b>Телефон:</b> <a href="tel:+79037971660">+7 (903) 797-16-60</a>
 </li>
				<li><b>Адрес:</b> г. Москва, пер. 7-ой Ростовский, д. 11
 </li>
				<li><b>График работы</b>: Понедельник - Пятница с 10:00 до 19:00
 </li>
			</ul>
			<hr>
			 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2246.0285523310063!2d37.57091561593004!3d55.74063288055013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54bb96279d4eb%3A0x2a9897020f642af!2zNy3QuSDQoNC-0YHRgtC-0LLRgdC60LjQuSDQv9C10YAuLCAxMSwg0JzQvtGB0LrQstCwLCAxMTkxMjE!5e0!3m2!1sru!2sru!4v1556284285439!5m2!1sru!2sru"
                        width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> <br>
 <a href="https://maps.google.ru/maps?f=q&source=embed&hl=ru&geocode=&q=%D0%B3.+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+%D1%83%D0%BB.+2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F,+%D0%B4.+38%D0%90&aq=&sll=55,103&sspn=90.84699,270.527344&t=m&ie=UTF8&hq=&hnear=2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB.,+38,+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+127287&ll=55.805478,37.569551&spn=0.023154,0.054932&z=14&iwloc=A">
 <small></small></a><small>
			<hr>
			<h3>Реквизиты</h3><ul>
				<li>Название компании: ООО "ИНТЕРНЕТ-МАГАЗИН"СИЯНИЕ"
				</li>
				<li>Юридический Адрес: 105005, г. Москва, Плетешковский переулок, д. 18-20, корп. 1, помещение 6П
				</li>
				<li>Фактический адрес: 119121 г. Москва, пер. 7-ой Ростовский, д.11
				</li>
				<li>ИНН: 7701403924<br>
				</li>
				<li>КПП: 770101001
				</li>
				<li>ОГРН: 1147746898859</li>
				<li>Банк: ПАО "СБЕРБАНК РОССИИ", г. МОСКВА</li>
				<li> БИК: 044525225</li>
				<li>Р/С: 40702810338000006553
				</li>
				<li>К/С: 30101810400000000225</li>
			</ul></small>
<hr>
			<h2>Задать вопрос</h2>
			 <?$APPLICATION->IncludeComponent(
	"fijie:main.feedback",
	"fijie",
	Array(
		"COMPONENT_TEMPLATE" => "fijie",
		"EMAIL_TO" => "info@fijieonline.ru",
		"EVENT_MESSAGE_ID" => array(0=>"7",),
		"OK_TEXT" => "Спасибо, ваше обращение принято.",
		"REQUIRED_FIELDS" => array(),
		"USE_CAPTCHA" => "N"
	)
);?>
		</div>
		<div class="col-4 sidebar">
			 <?$APPLICATION->IncludeComponent(
	"fijie:blog.message.list",
	"section",
	Array(
		"BRANDS_IBLOCK_CODE" => "blog",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COUNT_RECORDS" => "4",
		"SORT_BY" => "DESC",
		"SORT_FIELD" => "ACTIVE_FROM"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:subscribe.form",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
		"SHOW_HIDDEN" => "Y",
		"USE_PERSONALIZATION" => "Y"
	)
);?>
<!--Footer--><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>