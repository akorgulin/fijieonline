<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ответы на вопросы по разделам");
?>

<?$APPLICATION->IncludeComponent("fijie:faq", ".default", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "BRANDS_IBLOCK_CODE" => "faq",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => ""
),
    false
);?>



<?$APPLICATION->IncludeComponent(
    "vr:callback",
    "block_callback",
    Array(
        "EMAIL_TO" => "",
        "IBLOCK_ID" => CALLBACK_IBLOCK_ID,
        "IBLOCK_TYPE" => "recall",
        "INCLUDE_JQUERY" => "N",
        "MAIL_TEMPLATE" => "FEEDBACK_FORM",
        "PROPERTY_FIO" => "TITLE",
        "PROPERTY_FORM_NAME" => "TITLE",
        "PROPERTY_PAGE" => "TITLE"
    )
);?>

<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
        "USE_PERSONALIZATION" => "Y",
        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
        "SHOW_HIDDEN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>