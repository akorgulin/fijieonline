<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?>
<!--About-->
<section class="about_us about_page" data-aos="fade-up" data-aos-delay="200">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/about/top_block/header.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
                <div class="about_description">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/about/top_block/description.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
                <div class="play_video" name="video" data-aos="fade-left" data-aos-delay="1000">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/about/top_block/play.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
            </div>
            <div class="col-6">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/about/top_block/img.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
        </div>
    </div>
</section>


<!-- Logos area -->
<section class="logos_area">
    <div class="container">
        <div class="row">
            <?$APPLICATION->IncludeComponent("fijie:brands", "about", array(
                "SORT_FIELD" => "SORT",
                "SORT_BY" => "ASC",
                "BRANDS_IBLOCK_CODE" => "brands",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600"
            ),
                false
            );?>
            <?$APPLICATION->IncludeComponent("fijie:brands", "professionals", array(
                "SORT_FIELD" => "SORT",
                "SORT_BY" => "ASC",
                "BRANDS_IBLOCK_CODE" => "brands_professionals",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600"
            ),
                false
            );?>
        </div>
    </div>
</section>


<!--Advantages-->
<section class="advantages full_background" data-aos="fade-up" data-aos-delay="200">
    <div class="container">
        <div class="row">
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="200">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="300">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages1.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="400">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages2.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-3 center" data-aos="fade-up" data-aos-delay="500">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/advantages/index_advantages3.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="col-12 center" data-aos="fade-up" data-aos-delay="600">
                <hr>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6 center" data-aos="fade-up" data-aos-delay="200">
                <p>Принимаем к оплате</p>
                <img src="<?=SITE_TEMPLATE_PATH?>/img/pay.png" alt="">
            </div>
            <div class="col-6 center" data-aos="fade-up" data-aos-delay="300">
                <p>Доставка товара</p>
                <img src="<?=SITE_TEMPLATE_PATH?>/img/sdekresize.jpeg"  width="179" height="25" alt="">
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>