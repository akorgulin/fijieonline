<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задайте вопрос");
?>

<!--Pay product-->
<section class="content_wrapper nobackground clearfix">
    <div class="container page_sidebar">
        <div class="row">
            <div class="col-8">
                <h1 class="page_header aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">Оплата товара:</h1>
				<p>При оформлении заказа оплата возможна любой банковской картой действующей на территории Российской Федерации.</p>
				<p>При оформлении заказа Pay Pass</p>
				<p>При доставке курьером в пределах МКАД оплата возможна наличными денежными средствами.</p>
            </div>
            <div class="col-4 sidebar">
                <?$APPLICATION->IncludeComponent("fijie:blog.message.list", "section", array(
                    "SORT_FIELD" => "ACTIVE_FROM",
                    "SORT_BY" => "DESC",
                    "BRANDS_IBLOCK_CODE" => "blog",
                    "CACHE_TYPE" => "A",
                    "COUNT_RECORDS" => "4",
                    "CACHE_TIME" => "3600",
                ),
                    false
                );?>
            </div>
        </div>
    </div>
</section>


<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
        "USE_PERSONALIZATION" => "Y",
        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
        "SHOW_HIDDEN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);?>
<!--Footer-->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>