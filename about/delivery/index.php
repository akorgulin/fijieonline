<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка товаров");
?><!--Pay product--> <section class="content_wrapper nobackground clearfix">
<div class="container page_sidebar">
	<div class="row">
		<div class="col-12">
			<h1 class="page_header aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">Доставка:</h1>
			<ul>
				<li>Доставка Курьером компании в пределах МКАД - бесплатно. </li>
				<li>Доставка по всей России и за пределы МКАД курьером СДЭК (стоимость определяется при оформлении заказа и зависит от региона доставки и объема заказа)</li>
				<li> Бесплатная доставка по всей России при заказе на сумму от 15 000 руб.</li>
			</ul>
			<hr>
			<h1>
			Скидки:</h1>
			<p>
				Предлагаем Вам накопительную систему скидок. Скидка фиксируется на все время сотрудничества с нами.
			</p>
			<ul>
				<li> От 20 000 руб – 5%</li>
				<li> От 30 000 руб – 10%</li>
				<li> От 40 000 руб. – 15%.</li>
			</ul>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"vr:callback",
	"block_callback",
	Array(
		"EMAIL_TO" => "",
		"IBLOCK_ID" => CALLBACK_IBLOCK_ID,
		"IBLOCK_TYPE" => "recall",
		"INCLUDE_JQUERY" => "N",
		"MAIL_TEMPLATE" => "FEEDBACK_FORM",
		"PROPERTY_FIO" => "TITLE",
		"PROPERTY_FORM_NAME" => "TITLE",
		"PROPERTY_PAGE" => "TITLE"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:subscribe.form",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
		"SHOW_HIDDEN" => "Y",
		"USE_PERSONALIZATION" => "Y"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>