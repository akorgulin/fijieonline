<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?if(!empty($arResult["ERROR_MESSAGE"]))
{
    foreach($arResult["ERROR_MESSAGE"] as $v)
        ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
    ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
<form name="contact" action="<?=POST_FORM_ACTION_URI?>" method="POST" onsubmit="yaCounter55176931.reachGoal('question'); return true;">
    <?=bitrix_sessid_post()?>
    <div class="forms-grid">
        <div class="forms-col-6 aos-init aos-animate left" data-aos="fade-right" data-aos-delay="350">
            <div class="border-form block">
                <div class="without_icon_input">
                    <div class="icon"><span class="lnr lnr-user"></span></div>
                    <div class="soa-property-container">
                        <input type="text" name="theme_name" placeholder="<?=GetMessage("MFT_THEME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("THEME", $arParams["REQUIRED_FIELDS"])):?><?endif?>" value="<?=$arResult["THEME_NAME"]?>"/>
                    </div>
                </div>
            </div>
        </div><br><br><br>
        <div class="forms-col-6 aos-init aos-animate left" data-aos="fade-right" data-aos-delay="350">
            <div class="border-form block">
                <div class="without_icon_input">
                    <div class="icon"><span class="lnr lnr-user"></span></div>
                    <div class="soa-property-container">
                        <input type="text" name="user_name" placeholder="<?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><?endif?>" value="<?=$arResult["AUTHOR_NAME"]?>"/>
                    </div>
                </div>
            </div>
        </div><br><br><br>
        <div class="forms-col-6 aos-init aos-animate left" data-aos="fade-left" data-aos-delay="400">
            <div class="border-form block">
                <div class="without_icon_input">
                    <div class="icon"><span class="lnr lnr-envelope"></span></div>
                    <div class="soa-property-container">
                        <input type="text" placeholder="<?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><?endif?>" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>"/><br/>
                    </div>
                </div>
            </div>
        </div><br><br><br>
        <div class="forms-col-6 aos-init aos-animate left" data-aos="fade-left" data-aos-delay="400">
            <div class="border-form block">
                <textarea placeholder="<?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><?endif?>" name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea><br/>
            </div>
        </div><br><br><br><br><br>
        <?if($arParams["USE_CAPTCHA"] == "Y"):?>
        <br><br><br>
        <div class="forms-col-6 aos-init aos-animate left" data-aos="fade-left" data-aos-delay="400">
            <div class="border-form">
                    <strong><?=GetMessage("MFT_CAPTCHA")?></strong>
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA"><br/>
                    <strong><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></strong><br/>
                    <input type="text" name="captcha_word" size="30" maxlength="50" value=""/><br/>
            </div>
        </div><br><br><br><br><br><br>
        <?else:?>
        <br><br><br>
        <?endif;?>

        <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">

        <p class="buttons_preview">
            <a href="javascript:void();" onclick="javascript:$('form[name=contact] input[type=submit]').click();" class="btn iconlink aos-init aos-animate"><?=GetMessage("MFT_SUBMIT")?></a>
        </p>
    </div>

    <input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" class="bx_bt_button bx_big shadow hidden-xs">
</form>
