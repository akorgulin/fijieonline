<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use \Bitrix\Main\Data\Cache;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
	$arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {	
	$arParams["COUNT_RECORDS"] = false;
}

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];


$cacheId = serialize(array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arParams));
$cache = Cache::createInstance();

if ($cache->initCache(3600, $cacheId)) {
    $arResult = $cache->getVars();
} else if ($cache->startDataCache()) {

    Loader::includeModule("highloadblock");

    if( Loader::includeModule("iblock") )
    {
        if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";

        if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";

        $arParams['AJAX'] = $_REQUEST['AJAX'];

        //$arParams['nPageSize'] = (int)$_REQUEST['nPageSize'];
        if($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;

        $arResult['ORDABC'] = (int)$_REQUEST['abc'];
        $arResult['BRAND_ID'] = (int)$_REQUEST['id'];
        $arResult['BRAND_CODE'] = $_REQUEST['elmid'];
        $arResult['LETTER'] = mb_substr($_REQUEST['let'], 0, 1);

        $arFilter = array(
            'IBLOCK_CODE' => $arParams['BRANDS_IBLOCK_CODE']
        );

        $IBLOCK_ID = 0;
        $arSelect = array('NAME', 'CODE','IBLOCK_ID');

        $rsElement = CIBlockElement::GetList(array('NAME' => 'ASC'), $arFilter, false, false, $arSelect);
        $arResult['LAT'] = array();
        $arResult['RUS'] = array();
        if ($data = $rsElement -> Fetch())
        {
            $IBLOCK_ID = $data['IBLOCK_ID'];

        }

        $arSelect = array(
            'ID',
            'NAME',
            'CODE',
            'PREVIEW_PICTURE',
            'DETAIL_TEXT',
            'PROPERTY_HIDDEN',
            'PROPERTY_BRAND_CODE'
        );

        if($arResult['ORDABC'] > 0) $arNavStartParams = false;
        else {

            $arNavStartParams = array(
                "nPageSize" => $arParams['nPageSize'],
                //"iNumPage" => $iNumPage,
                "bShowAll" => false
            );

        }
        //  If the specified number of valid entries that do not need a navigation
        if ($arParams["COUNT_RECORDS"] > 0 ) {
            $arNavStartParams = false;
            $arNavStartParams = array(
                "nTopCount" => $arParams["COUNT_RECORDS"],
                "bShowAll" => false
            );
        }


        $rsElement = CIBlockElement::GetList(
            array($arParams['SORT_FIELD'] => $arParams['SORT_BY']),
            $arFilter,
            false,
            $arNavStartParams,
            $arSelect
        );
        while($data = $rsElement -> GetNext())
        {
            if($data["PROPERTY_HIDDEN_VALUE"]=="Y") {
                $arResult['BRANDS'][$data["PROPERTY_BRAND_CODE_VALUE"]] = $data["PROPERTY_HIDDEN_VALUE"];
            } else {
                $data["PREVIEW_PICTURE_SRC"] = CFile::GetPath($data["PREVIEW_PICTURE"]);
                $arResult['BRANDS_LIST'][$data["PROPERTY_BRAND_CODE_VALUE"]] = $data;
            }
        }

        $arResult['BRANDS_PROPERTY'] = [];
        $res = CIBlock::GetProperties(CATALOG_IBLOCK_ID, Array(), Array("CODE"=>"BRAND_REF"));
        if($res_arr = $res->Fetch()) {
            $dbHblock = HL\HighloadBlockTable::getList(
                array(
                    "filter" => array("NAME" => "BrandReference", "TABLE_NAME" => $res_arr["USER_TYPE_SETTINGS"]["TABLE_NAME"])
                )
            );
            while ($hldata = $dbHblock->Fetch())
            {
                $ID = $hldata["ID"];
                $hldata = HL\HighloadBlockTable::getById($ID)->fetch();
                $hlentity = HL\HighloadBlockTable::compileEntity($hldata);
                $entity_data_class = $hlentity->getDataClass();
                $entity_table_name = $hldata['TABLE_NAME'];
                $sTableID = 'tbl_'.$entity_table_name;

                $rsData = $entity_data_class::getList(array(
                    "select" => array('*'), //выбираем все поля
                    "order" => array("UF_SORT"=>"ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                ));

                while ($arEnum = $rsData->fetch())
                {

                    $boolPict = true;
                    if (!isset($arEnum['UF_NAME']))
                    {
                        $boolName = false;
                        break;
                    }

                    if (!isset($arEnum['UF_FILE']) || (int)$arEnum['UF_FILE'] <= 0)
                        $boolPict = false;

                    if ($boolPict)
                    {
                        $arEnum['PREVIEW_PICTURE'] = CFile::GetFileArray($arEnum['UF_FILE']);
                        if (empty($arEnum['PREVIEW_PICTURE']))
                            $boolPict = false;
                    }

                    $descrExists = (isset($arEnum['UF_DESCRIPTION']) && (string)$arEnum['UF_DESCRIPTION'] !== '');
                    if ($boolPict)
                    {
                        if ($descrExists)
                        {
                            $width = $arParams["WIDTH_SMALL"];
                            $height = $arParams["HEIGHT_SMALL"];
                            $type = "PIC_TEXT";
                        }
                        else
                        {
                            $width = $arParams["WIDTH"];
                            $height = $arParams["HEIGHT"];
                            $type = "ONLY_PIC";
                        }

                        $arEnum['PREVIEW_PICTURE']['WIDTH'] = (int)$arEnum['PREVIEW_PICTURE']['WIDTH'];
                        $arEnum['PREVIEW_PICTURE']['HEIGHT'] = (int)$arEnum['PREVIEW_PICTURE']['HEIGHT'];
                        if (
                            $arEnum['PREVIEW_PICTURE']['WIDTH'] > $width
                            || $arEnum['PREVIEW_PICTURE']['HEIGHT'] > $height
                        )
                        {
                            $arEnum['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
                                $arEnum['PREVIEW_PICTURE'],
                                array("width" => $width, "height" => $height),
                                BX_RESIZE_IMAGE_PROPORTIONAL,
                                true
                            );

                            $arEnum['PREVIEW_PICTURE']['SRC'] = $arEnum['PREVIEW_PICTURE']['src'];
                            $arEnum['PREVIEW_PICTURE']['WIDTH'] = $arEnum['PREVIEW_PICTURE']['width'];
                            $arEnum['PREVIEW_PICTURE']['HEIGHT'] = $arEnum['PREVIEW_PICTURE']['height'];
                        }
                    }
                    elseif ($descrExists)
                    {
                        $type = "ONLY_TEXT";
                    }
                    else //Nothing to show
                    {
                        continue;
                    }
                    $arResult['BRANDS_LIST'][$arEnum["UF_XML_ID"]]["URL"] = $arEnum["UF_LINK"];
                    $arEnum["IMGMENU"] = CFile::GetPath($arEnum["UF_IMG_MENU"]);
                    if($arResult['BRANDS'][$arEnum["UF_XML_ID"]]!="Y") {
                        if($arEnum["IMGMENU"]) {
                            $arResult['BRANDS_PROPERTY'][$arEnum["ID"]] = $arEnum;
                        }
                    }
                }

            }
        }

        $arResult["NAV_STRING"] = $rsElement -> GetPageNavStringEx($navComponentObject, "", "bootstrap");


    }
    if( !empty($arResult['BRAND_CODE']) )
        foreach($arResult['BRANDS'] as $val)
            $APPLICATION -> AddChainItem($val['NAME'], "");

	$cache->endDataCache($arResult);

}
    $this->IncludeComponentTemplate();
?>