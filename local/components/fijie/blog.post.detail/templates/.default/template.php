<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CUtil::InitJSCore(array("image"));

$className = "blog-post";
$className .= " blog-post-first";
$className .= " blog-post-alt";
$className .= " blog-post-year-".$arResult["Post"]["DATE_PUBLISH_Y"];
$className .= " blog-post-month-".IntVal($arResult["Post"]["DATE_PUBLISH_M"]);
$className .= " blog-post-day-".IntVal($arResult["Post"]["DATE_PUBLISH_D"]);

?>
<!--Category-->
<section class="content_wrapper nobackground clearfix">
    <?$APPLICATION->IncludeComponent("fijie:blog.message.groups", "", array(
        "SORT_FIELD" => "ACTIVE_FROM",
        "SORT_BY" => "DESC",
        "BRANDS_IBLOCK_CODE" => "blog",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "COUNT_RECORDS" => "",
    ),
        false
    );?>
    <div class="container page_sidebar">
        <div class="row">
            <div class="col-8 <? $className?>" id="blg-post-<?=$arResult["BLOG_MESSAGE"][0]["ID"]?>">

                <script type="text/javascript">
                    BX.viewImageBind(
                        'blg-post-<?=$arResult["BLOG_MESSAGE"][0]["ID"]?>',
                        {showTitle: false},
                        {tag:'IMG', attr: 'data-bx-image'}
                    );
                </script>
                <h1 class="page_header" data-aos="fade-up" data-aos-delay="300"><?=$arResult["BLOG_MESSAGE"][0]["NAME"]?></h1>
                <div class="post_date" data-aos="fade-up" data-aos-delay="400"><?=$arResult["BLOG_MESSAGE"][0]["ACTIVE_FROM"]?></div>
                <?php if ($arResult["BLOG_MESSAGE"][0]["DETAIL_PICTURE"]){
                    $arFile = CFile::GetFileArray($arResult["BLOG_MESSAGE"][0]["DETAIL_PICTURE"]);
                    ?>
                    <div class="post_preview" data-aos="fade-up" data-aos-delay="500">
                        <img src="<?=$arFile["SRC"]?>" width="800">
                    </div>
                <?} ?>
                <p><?=$arResult["BLOG_MESSAGE"][0]["DETAIL_TEXT"]?></p>
                <a href="/blog/" class="btn mint iconlink iconleft"><span class="lnr lnr-arrow-left"></span> Вернуться в блог</a>
            </div>
            <div class="col-4 sidebar">
                <?$APPLICATION->IncludeComponent("fijie:blog.message.list", "section", array(
                    "SORT_FIELD" => "ACTIVE_FROM",
                    "SORT_BY" => "DESC",
                    "BRANDS_IBLOCK_CODE" => "blog",
                    "CACHE_TYPE" => "A",
                    "COUNT_RECORDS" => "4",
                    "CACHE_TIME" => "3600", 
                ),
                    false
                );?>
            </div>
        </div>
    </div>
</section>
