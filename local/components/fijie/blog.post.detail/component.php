<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
	$arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {	
	$arParams["COUNT_RECORDS"] = false;
}
//var_dump($arParams);
$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];

if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arParams)))
{
    Loader::includeModule("highloadblock");
	if( Loader::includeModule("iblock") )
	{
		if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";
		
		if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";
		
		$arParams['AJAX'] = $_REQUEST['AJAX'];
		
		//$arParams['nPageSize'] = (int)$_REQUEST['nPageSize'];
		if($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;
		
		$arFilter = array(
			'ACTIVE' => "Y",
			'IBLOCK_CODE' => $arParams['BRANDS_IBLOCK_CODE']
		);
	
		$IBLOCK_ID = 0;
		$arSelect = array('NAME', 'CODE','IBLOCK_ID');
	
		$rsElement = CIBlock::GetList(array('NAME' => 'ASC'), ["CODE"=>$arParams['BRANDS_IBLOCK_CODE']], false, false, $arSelect);
		$arResult['LAT'] = array();
		$arResult['RUS'] = array();
		if ($data = $rsElement -> Fetch())
		{
			$IBLOCK_ID = $data['IBLOCK_ID'];
			$arResult['HEADER'] = $data['DESCRIPTION'];
		}
		if($_REQUEST["id"]) {
			$arFilter["CODE"] = $_REQUEST["id"];
		}
		if($arResult['ORDABC'] > 0) $arNavStartParams = false;
		else {
			
			$arNavStartParams = array(
				"nPageSize" => $arParams['nPageSize'],
				//"iNumPage" => $iNumPage,
				"bShowAll" => false
			);
		
		}
		//  If the specified number of valid entries that do not need a navigation
		if ($arParams["COUNT_RECORDS"] > 0 ) {
			$arNavStartParams = false;
			$arNavStartParams = array(
				"nTopCount" => $arParams["COUNT_RECORDS"],
				"bShowAll" => false
			);
		}
 
		$rsElement = CIBlockElement::GetList(
			array($arParams['SORT_FIELD'] => $arParams['SORT_BY']),
			$arFilter,
			false,
			$arNavStartParams
		); 
		if($data = $rsElement -> GetNextElement())
		{
			$arElement = $data->GetFields();
			$arElement["PROPERTIES"] = $data->GetProperties();
			$arResult['BLOG_MESSAGE'][] = $arElement;
		}
		//dump($arResult);
		$arResult["NAV_STRING"] = $rsElement -> GetPageNavStringEx($navComponentObject, "", "bootstrap");
	
	}
	//
	$this->IncludeComponentTemplate();

}
?>