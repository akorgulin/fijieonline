<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SF_BSS_NAME"),
	"DESCRIPTION" => GetMessage("SF_BSS_DESCRIPTION"),
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "service",
		"CHILD" => array(
			"ID" => "system_field_edit",
			"NAME" => GetMessage("SF_BSS_SERVICE")
		)
	),
);
?>