<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @var array $arParams
 * @var array $arResult
 */

foreach (GetModuleEvents("main", "system.field.edit.file", true) as $arEvent)
{
	if (ExecuteModuleEventEx($arEvent, array($arResult, $arParams)))
		return;
}

?>
<div id="main_<?=$arParams["arUserField"]["FIELD_NAME"]?>">
<?
$postFix = ($arParams["arUserField"]["MULTIPLE"] == "Y" ? "[]" : "");

if($arParams["arUserField"]["MULTIPLE"] == "Y" && $arParams["SHOW_BUTTON"] != "N" && $arParams["bVarsFromForm"])
{
	//multiple - we have additional hidden to clone, need to remove it
	array_pop($arResult["VALUE"]);
}

foreach ($arResult["VALUE"] as $res):

	?>
	<div class="fields files drop item_file_<?=$arParams["arUserField"]["ID"]?>">
        <div class="cont" style="color: rgb(142, 153, 165);">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="76px" height="70px">
                <path fill-rule="evenodd" fill="rgb(234, 160, 162)" d="M70.905,50.020 C67.592,53.664 63.138,55.844 58.371,56.187 C58.309,56.187 58.262,56.187 58.215,56.187 L46.619,56.187 C45.447,56.187 44.509,55.253 44.509,54.085 C44.509,52.917 45.447,51.982 46.619,51.982 L58.137,51.982 C65.670,51.406 71.780,44.678 71.780,36.939 C71.780,29.635 66.529,23.235 59.575,22.051 C58.637,21.895 57.918,21.132 57.824,20.182 C56.949,11.057 49.338,4.173 40.118,4.173 C33.257,4.173 26.943,8.160 24.020,14.342 C23.552,15.324 22.411,15.791 21.379,15.417 C20.535,15.106 19.629,14.950 18.707,14.950 C14.393,14.950 10.877,18.454 10.877,22.752 C10.877,23.702 11.033,24.589 11.346,25.430 C11.690,26.396 11.315,27.470 10.424,27.984 C6.595,30.211 4.220,34.323 4.220,38.730 C4.220,45.551 9.596,51.608 15.988,51.998 L29.365,51.998 C30.537,51.998 31.475,52.932 31.475,54.100 C31.475,55.268 30.537,56.202 29.365,56.202 L15.925,56.202 C15.878,56.202 15.847,56.202 15.800,56.202 C11.549,55.969 7.564,53.960 4.548,50.565 C1.610,47.248 -0.000,43.059 -0.000,38.745 C-0.000,33.404 2.610,28.389 6.908,25.275 C6.751,24.465 6.658,23.639 6.658,22.783 C6.658,16.165 12.065,10.776 18.707,10.776 C19.488,10.776 20.270,10.854 21.020,10.994 C22.755,7.973 25.208,5.404 28.162,3.504 C31.710,1.215 35.851,-0.000 40.102,-0.000 C45.603,-0.000 50.885,2.040 54.949,5.746 C58.606,9.079 60.997,13.470 61.809,18.283 C69.999,20.432 75.984,28.202 76.000,36.939 C76.000,41.766 74.203,46.407 70.905,50.020 ZM26.834,41.361 C26.302,41.361 25.755,41.143 25.349,40.739 C24.521,39.913 24.521,38.590 25.349,37.764 L36.523,26.630 C36.914,26.225 37.461,26.007 38.008,26.007 C38.555,26.007 39.102,26.240 39.492,26.630 L50.667,37.764 C51.495,38.590 51.495,39.913 50.667,40.739 C49.838,41.564 48.510,41.564 47.697,40.739 L40.118,33.186 L40.118,67.898 C40.118,69.066 39.180,70.000 38.008,70.000 C36.836,70.000 35.898,69.066 35.898,67.898 L35.898,33.186 L28.318,40.739 C27.912,41.159 27.365,41.361 26.834,41.361 Z"></path>
            </svg>
            <div class="desc">нажмите сюда и выберите файлы</div>
            <div class="browse">выбрать файл</div>
        </div>
        <output id="list"></output>
		<input type="hidden" name="<?=$arParams["arUserField"]["~FIELD_NAME"]?>_old_id<?=$postFix?>" value="<?=$res?>" />
        <?=CFile::InputFile($arParams["arUserField"]["FIELD_NAME"], 0, $res, false, 0, "", 'multiple="true" id="'.$arParams["arUserField"]["~FIELD_NAME"].'"', 0, "", 'multiple="true" value="'.$res.'"', true, isset($arParams['SHOW_FILE_PATH']) ? $arParams['SHOW_FILE_PATH'] : true);?>
<?
$arFile = CFile::GetFileArray($res);
if($arFile)
{
	if(CFile::IsImage($arFile["SRC"], $arFile["CONTENT_TYPE"]))
	{
	    ?><script type="text/javascript">
        $(document).ready(function ($) {
            $("div.item_file_<?=$arParams["arUserField"]["ID"]?> div.cont").remove();
        });
        </script><?
		echo CFile::ShowImage(
			$arFile,
			isset($arParams["FILE_MAX_WIDTH"]) ? (int)$arParams["FILE_MAX_WIDTH"] : 0,
			isset($arParams["FILE_MAX_HEIGHT"]) ? (int)$arParams["FILE_MAX_HEIGHT"] : 0,
			" style='width:auto'",
			'',
			false,
			0,
			0,
			0,
			!empty($arParams['FILE_URL_TEMPLATE']) ? $arParams['FILE_URL_TEMPLATE'] : ''
		);
	}
	else
	{
        ?><script type="text/javascript">
        $(document).ready(function ($) {
            $("div.item_file_<?=$arParams["arUserField"]["ID"]?> div.cont").remove();
        });
        </script><?
		if($arParams['FILE_URL_TEMPLATE'] <> '')
		{
			$src = CComponentEngine::MakePathFromTemplate($arParams['FILE_URL_TEMPLATE'], array('file_id' => $arFile["ID"]));
		}
		else
		{
			$src = $arFile["SRC"];
		}
		echo '<a href="'.htmlspecialcharsbx($src).'">'.htmlspecialcharsbx($arFile["FILE_NAME"]).'</a> ('.CFile::FormatSize($arFile["FILE_SIZE"]).')';
	}
}
?>
	</div>
	<?
endforeach;
?>
</div>
<?if ($arParams["arUserField"]["MULTIPLE"] == "Y" && $arParams["SHOW_BUTTON"] != "N"):?>
<div style="display:none" id="main_add_<?=$arParams["arUserField"]["FIELD_NAME"]?>" class="fields files drop hidden-xs">
    <div class="cont" style="color: rgb(142, 153, 165);">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="76px" height="70px">
            <path fill-rule="evenodd" fill="rgb(234, 160, 162)" d="M70.905,50.020 C67.592,53.664 63.138,55.844 58.371,56.187 C58.309,56.187 58.262,56.187 58.215,56.187 L46.619,56.187 C45.447,56.187 44.509,55.253 44.509,54.085 C44.509,52.917 45.447,51.982 46.619,51.982 L58.137,51.982 C65.670,51.406 71.780,44.678 71.780,36.939 C71.780,29.635 66.529,23.235 59.575,22.051 C58.637,21.895 57.918,21.132 57.824,20.182 C56.949,11.057 49.338,4.173 40.118,4.173 C33.257,4.173 26.943,8.160 24.020,14.342 C23.552,15.324 22.411,15.791 21.379,15.417 C20.535,15.106 19.629,14.950 18.707,14.950 C14.393,14.950 10.877,18.454 10.877,22.752 C10.877,23.702 11.033,24.589 11.346,25.430 C11.690,26.396 11.315,27.470 10.424,27.984 C6.595,30.211 4.220,34.323 4.220,38.730 C4.220,45.551 9.596,51.608 15.988,51.998 L29.365,51.998 C30.537,51.998 31.475,52.932 31.475,54.100 C31.475,55.268 30.537,56.202 29.365,56.202 L15.925,56.202 C15.878,56.202 15.847,56.202 15.800,56.202 C11.549,55.969 7.564,53.960 4.548,50.565 C1.610,47.248 -0.000,43.059 -0.000,38.745 C-0.000,33.404 2.610,28.389 6.908,25.275 C6.751,24.465 6.658,23.639 6.658,22.783 C6.658,16.165 12.065,10.776 18.707,10.776 C19.488,10.776 20.270,10.854 21.020,10.994 C22.755,7.973 25.208,5.404 28.162,3.504 C31.710,1.215 35.851,-0.000 40.102,-0.000 C45.603,-0.000 50.885,2.040 54.949,5.746 C58.606,9.079 60.997,13.470 61.809,18.283 C69.999,20.432 75.984,28.202 76.000,36.939 C76.000,41.766 74.203,46.407 70.905,50.020 ZM26.834,41.361 C26.302,41.361 25.755,41.143 25.349,40.739 C24.521,39.913 24.521,38.590 25.349,37.764 L36.523,26.630 C36.914,26.225 37.461,26.007 38.008,26.007 C38.555,26.007 39.102,26.240 39.492,26.630 L50.667,37.764 C51.495,38.590 51.495,39.913 50.667,40.739 C49.838,41.564 48.510,41.564 47.697,40.739 L40.118,33.186 L40.118,67.898 C40.118,69.066 39.180,70.000 38.008,70.000 C36.836,70.000 35.898,69.066 35.898,67.898 L35.898,33.186 L28.318,40.739 C27.912,41.159 27.365,41.361 26.834,41.361 Z"></path>
        </svg>
        <div class="desc">нажмите сюда и выберите файлы</div>
        <div class="browse">выбрать файл</div>
    </div>
    <output id="list"></output>
	<input type="hidden" name="<?=$arParams["arUserField"]["~FIELD_NAME"]?>_old_id[]" value="" />
    <?=CFile::InputFile($arParams["arUserField"]["FIELD_NAME"], 0, "", false, 0, "", 'multiple="true" id="'.$arParams["arUserField"]["~FIELD_NAME"].'"', 0, "",'')?>
</div>
<input type="button" class="hidden-xs" value="<?=GetMessage("USER_TYPE_PROP_ADD")?>" onClick="addElementFile('<?=$arParams["arUserField"]["FIELD_NAME"]?>', this)">
<?endif;?>
