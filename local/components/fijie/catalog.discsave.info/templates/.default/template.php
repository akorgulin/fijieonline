<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult)) {
} else {
    $this->SetViewTarget('class_discsave');
    ?>class="item x2"<?
    $this->EndViewTarget();
    ?>
    <div class="item" data-aos="fade-left" data-aos-delay="250">
    <div class="border">
        <div class="discount">
            <h6 class="area_title"><? echo $arResult[0]['NAME']; ?></h6>
            <p><? echo GetMessage('BX_CMP_CDI_TPL_MESS_SIZE'); ?></p>
            <div class="discount_lines"><?
                foreach ($arResult as &$arDiscountSave) {

                    ?>
                    <div class="line">
                        <div class="scroll_area"></div>
                        <div class="scroll_area x2"></div>
                        <div class="scroll_area x3"></div>
                        <div class="descr">
                            <?
                            if ('P' == $arDiscountSave['VALUE_TYPE']) {
                                echo $arDiscountSave['VALUE']; ?>&nbsp;%<?
                            } else {
                                echo CCurrencyLang::CurrencyFormat($arDiscountSave['VALUE'], $arDiscountSave['CURRENCY'], true);
                            }
                            if (isset($arDiscountSave['NEXT_LEVEL']) && is_array($arDiscountSave['NEXT_LEVEL'])) {
                                $strNextLevel = '';
                                if ('P' == $arDiscountSave['NEXT_LEVEL']['VALUE_TYPE']) {
                                    $strNextLevel = $arDiscountSave['NEXT_LEVEL']['VALUE'] . '&nbsp;%';
                                } else {
                                    $strNextLevel = CCurrencyLang::CurrencyFormat($arDiscountSave['NEXT_LEVEL']['VALUE'], $arDiscountSave['CURRENCY'], true);
                                }

                                ?>
                                <br/><? echo str_replace(array('#SIZE#', '#SUMM#'), array($strNextLevel, CCurrencyLang::CurrencyFormat(($arDiscountSave['NEXT_LEVEL']['RANGE_FROM'] - $arDiscountSave['RANGE_SUMM']), $arDiscountSave['CURRENCY'], true)), GetMessage('BX_CMP_CDI_TPL_MESS_NEXT_LEVEL')); ?><?
                            }
                            ?>
                        </div>
                    </div>
                    <?
                } ?></div>
        </div>
    </div>
    </div><?
}
?>