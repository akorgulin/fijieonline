<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$cnt = count($arResult["SEARCH"]); 
if ($cnt > 0) { ?>
    <div class="result_search">
        <h1 class="page_heading small dark" data-aos="flip-up" data-aos-delay="400">Результаты поиска</h1>
        <p data-aos="fade-up" data-aos-delay="500">По запросу: «<?= $arResult["REQUEST"]["QUERY"] ?>» найдено <span
                    class="searchstring"><?= $cnt ?>  товара</span></p>
        <div class="forms-grid search_result clearfix" data-aos="fade-up" data-aos-delay="600">
            <div class="forms-col-6">
                <div class="search_form">
                    <form action="/catalog/search/">
                        <input type="text" name="q" placeholder="Введите поисковой запрос" class="search_area_input">
                        <input type="submit" name="button" value="Найти" class="search btn mint">
                    </form>
                </div>
            </div>
        </div>
    </div>
<? } ?>
<? return $arResult; ?>




