<?
use Bitrix\Main\Loader;
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();


if( CModule::IncludeModule("iblock") ) {
} else {
	die("IBLOCK MODULE NOT INSTALLED");
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

// Если нет валидного кеша (то есть нужно запросить данные и сделать валидный кеш)
if ($this->StartResultCache(false))
{
    if($arParams["ELEMENT_ID"]){
        COption::SetOptionString("novagr.jwshop","main_banners_element_priority_id",$arParams["ELEMENT_ID"]);
    }
    $novaGroupBanners = new Novagroup_Classes_General_Banners(NOVAGROUP_MODULE_ID);
    $arResult = $novaGroupBanners->getActive();

    if($arResult["PROPERTY_VIEW_VALUE"]=="slider_horizontal") {
        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "CODE",
            "XML_ID",
            "NAME",
            "ACTIVE",
            "DATE_ACTIVE_FROM",
            "DATE_ACTIVE_TO",
            "SORT",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "DETAIL_TEXT",
            "DETAIL_TEXT_TYPE",
            "DATE_CREATE",
            "CREATED_BY",
            "TIMESTAMP_X",
            "MODIFIED_BY",
            "TAGS",
            "IBLOCK_SECTION_ID",
            "DETAIL_PAGE_URL",
            "LIST_PAGE_URL",
            "DETAIL_PICTURE",
            "PREVIEW_PICTURE",
            "PROPERTY_*",
            "CATALOG_QUANTITY",
            "CATALOG_GROUP_1",
            "PROPERTY_PERCENT",
            "PROPERTY_VIEW",
            "PROPERTY_BANNER1",
            "PROPERTY_LINK_BANNER1",
            "PROPERTY_LINE_TEXT_1",
            "PROPERTY_LINE_TEXT_2"
        );
        $dbElement = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"=PROPERTY_VIEW"=>'slider_horizontal'), false, false, $arSelect);
        while($arElement = $dbElement->Fetch()){
            $arResult["ITEMS"][] = $arElement;
        }
    }

   //Если прицеплен товар для рекламы к банеру показываем цену и кнопку купить.
    if($arResult["PROPERTY_ID_PRODUCT_VALUE"]>0) {

        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "CODE",
            "XML_ID",
            "NAME",
            "ACTIVE",
            "DATE_ACTIVE_FROM",
            "DATE_ACTIVE_TO",
            "SORT",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "DETAIL_TEXT",
            "DETAIL_TEXT_TYPE",
            "DATE_CREATE",
            "CREATED_BY",
            "TIMESTAMP_X",
            "MODIFIED_BY",
            "TAGS",
            "IBLOCK_SECTION_ID",
            "DETAIL_PAGE_URL",
            "LIST_PAGE_URL",
            "DETAIL_PICTURE",
            "PREVIEW_PICTURE",
            "PROPERTY_*",
            "CATALOG_QUANTITY",
            "CATALOG_GROUP_1"
        );
        $arElement = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$arResult["PROPERTY_ID_PRODUCT_IBLOCK_ID"],"ID"=>$arResult["PROPERTY_ID_PRODUCT_VALUE"]), false, false, $arSelect)->Fetch();

        $arParams['CONVERT_CURRENCY']="Y";
        $arParams['CURRENCY_ID']  = "RUB";
        $arConvertParams = array();
        if ($arParams['CONVERT_CURRENCY'] == 'Y')
        {

            if (!Loader::includeModule('currency'))
            {
                $arParams['CONVERT_CURRENCY'] = 'N';
                $arParams['CURRENCY_ID'] = '';
            }
            else
            {
                $arResultModules['currency'] = true;
                $currencyIterator = CurrencyTable::getList(array(
                    'select' => array('CURRENCY'),
                    'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
                ));
                if ($currency = $currencyIterator->fetch())
                {
                    $arParams['CURRENCY_ID'] = $currency['CURRENCY'];
                    $arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];
                }
                else
                {
                    $arParams['CONVERT_CURRENCY'] = 'N';
                    $arParams['CURRENCY_ID'] = '';
                }
                unset($currency, $currencyIterator);
            }
        }

        $arElement['CONVERT_CURRENCY'] = $arConvertParams;

        $arResultPrices = CIBlockPriceTools::GetCatalogPrices($arResult["PROPERTY_ID_PRODUCT_IBLOCK_ID"], array('BASE', 'RETAIL', 'DISCOUNT'));
        $arResultPricesAllow = CIBlockPriceTools::GetAllowCatalogPrices($arResultPrices);

        $arElement["CAT_PRICES"] = $arResultPrices;
        $arElement['PRICES_ALLOW'] = $arResultPricesAllow;

        $arResult["CODE"] = $arResult["PROPERTY_ID_PRODUCT_CODE"];

        $arResult["PRICES"] = CIBlockPriceTools::GetItemPrices($arResult["PROPERTY_ID_PRODUCT_IBLOCK_ID"], $arResultPrices, $arElement, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);

        if($arResult["PROPERTY_ID_PRODUCT_IBLOCK_SECTION_ID"]):
            $nav = CIBlockSection::GetNavChain(false, $arResult["PROPERTY_ID_PRODUCT_IBLOCK_SECTION_ID"]);
            while($arSection = $nav->Fetch()):
                $arResult["SECTION_CODE"] = $arSection["CODE"];
            endwhile;
        endif;
    }

	$this -> IncludeComponentTemplate();
}


?>