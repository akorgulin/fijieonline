<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$com_path = '//'.SITE_SERVER_NAME.'/local/components/fijie/sale.basket.basket.line/templates/.default';
$this->addExternalCss($com_path."/basket_line.css");
$this->addExternalJS($com_path."/basket_line.js");
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?><div class="bx-hdr-profile">
    <?if (!$compositeStub && $arParams['SHOW_AUTHOR'] == 'Y'):?>
	<div class="bx-basket-block">
        <?$APPLICATION->IncludeComponent(
            "h2o:favorites.line",
            "",
            Array(
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "N",
                "URL_LIST" => "/personal/favorites/"
            )
        );?>
        <?if ($USER->IsAuthorized()):
			$name = trim($USER->GetFullName());
			if (! $name)
				$name = trim($USER->GetLogin());
			if (strlen($name) > 15)
				$name = substr($name, 0, 12).'...';
			?>
            <a href="<?=$arParams['PATH_TO_PROFILE']?>" class="iconlink mobile_restyle aos-init aos-animate" data-aos="fade-left" data-aos-delay="300"><span class="hidden_mobile"><?=GetMessage('TSB1_PROFILE')?><?htmlspecialcharsbx($name)?></span></a>
            <?php /*<a href="?logout=yes" class="iconlink mobile_restyle aos-init aos-animate" data-aos="fade-left" data-aos-delay="300"><span class="hidden_mobile"><?=GetMessage('TSB1_LOGOUT')?></span></a>*/ ?>
		<?else:
			$arParamsToDelete = array(
				"login",
				"login_form",
				"logout",
				"register",
				"forgot_password",
				"change_password",
				"confirm_registration",
				"confirm_code",
				"confirm_user_id",
				"logout_butt",
				"auth_service_id",
				"clear_cache"
			);

			$currentUrl = urlencode($APPLICATION->GetCurPageParam("", $arParamsToDelete));

			
			$pathToAuthorize = $arParams['PATH_TO_AUTHORIZE'];
			$pathToAuthorize .= (stripos($pathToAuthorize, '?') === false ? '?' : '&');
			$pathToAuthorize .= 'login=yes&backurl='.$currentUrl;
			//$pathToAuthorize
			?>
            <a data-fancybox="" data-src="#enter" href="javascript:;" class="iconlink mobile_restyle aos-init aos-animate" data-aos="fade-left" data-aos-delay="600"><span class="lnr lnr-lock"></span><span class="hidden_mobile"><?=GetMessage('TSB1_LOGIN')?></span></a>
			<?

            if ($arParams['AJAX'] == 'N')
            {
                ?><script type="text/javascript"><?=$cartId?>.currentUrl = '<?=$currentUrl?>';</script><?
            }
            else
            {
                $currentUrl = '#CURRENT_URL#';
            }


			if ($arParams['SHOW_REGISTRATION'] === 'Y')
			{
				$pathToRegister = $arParams['PATH_TO_REGISTER'];
				$pathToRegister .= (stripos($pathToRegister, '?') === false ? '?' : '&');
				$pathToRegister .= 'register=yes&backurl='.$currentUrl;
				/*
				<a href="<?=$pathToRegister?>">
					<?=GetMessage('TSB1_REGISTER')?>
				</a>*/

			}
			?>
		<?endif?>
	</div>
<?endif?>
	<div class="bx-basket-block">
        <a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="iconlink cart_link mobile_restyle aos-init aos-animate" data-aos="fade-left" data-aos-delay="300">
         <span class="lnr lnr-cart"></span><span class="hidden_mobile"><?= GetMessage('TSB1_CART') ?></span>
        <?
		if (!$compositeStub)
		{
			if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' || $arParams['SHOW_EMPTY_VALUES'] == 'Y')
			{

			    //if($arResult['NUM_PRODUCTS']>0) {
                    echo '<span class="num">'.$arResult['NUM_PRODUCTS'].'</span>';
                //}

				if ($arParams['SHOW_TOTAL_PRICE'] == 'Y')
				{
					?>
					<br <? if ($arParams['POSITION_FIXED'] == 'Y'): ?>class="hidden-xs"<? endif; ?>/>
					<span>
						<?=GetMessage('TSB1_TOTAL_PRICE')?> <strong><?=$arResult['TOTAL_PRICE']?></strong>
					</span>
					<?
				}
			}
		}

		if ($arParams['SHOW_PERSONAL_LINK'] == 'Y'):?>
			<div style="padding-top: 4px;">
			<span class="icon_info"></span>
			<a href="<?=$arParams['PATH_TO_PERSONAL']?>"><?=GetMessage('TSB1_PERSONAL')?></a>
			</div>
		<?endif?>
       </a>

	</div>
</div>