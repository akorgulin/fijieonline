<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CUtil::InitJSCore(array("image"));

?>
<div class="wrapper center">
    <h1 class="page_heading small dark" data-aos="flip-up" data-aos-delay="400"><?=$arResult['HEADER']?></h1>

    <div class="cat_list_open" data-aos="flip-up" data-aos-delay="500"><span class="lnr lnr-chevron-down"></span>Показать
        категории
    </div>
    <ul class="categories_list clearfix" data-aos="flip-up" data-aos-delay="500">
        <?foreach ($arResult['BLOG_GROUPS'] as $ind => $grouper_id) {
            $group = $arResult['BLOG_GROUPS'][$ind]; ?>
            <li <?if($_REQUEST["group"]==$group["CODE"]){?>class="current"<?}?>><a href="/blog/group/<?=$group["CODE"]?>/"><?=$group["NAME"]?></a></li>
        <?}?>
    </ul>
</div>
