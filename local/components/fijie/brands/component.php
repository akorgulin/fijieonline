<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
	$arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {	
	$arParams["COUNT_RECORDS"] = false;
}

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];

if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arParams)))
{
    Loader::includeModule("highloadblock");

    if( Loader::includeModule("iblock") )
    {
        if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";

        if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";

        $arParams['AJAX'] = $_REQUEST['AJAX'];

        //$arParams['nPageSize'] = (int)$_REQUEST['nPageSize'];
        if($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;

        $arResult['ORDABC'] = (int)$_REQUEST['abc'];
        $arResult['BRAND_ID'] = (int)$_REQUEST['id'];
        $arResult['BRAND_CODE'] = $_REQUEST['elmid'];
        $arResult['LETTER'] = mb_substr($_REQUEST['let'], 0, 1);

        $arFilter = array(
            'ACTIVE' => "Y",
            'IBLOCK_CODE' => $arParams['BRANDS_IBLOCK_CODE']
        );

        $IBLOCK_ID = 0;
        $lat = range('A', 'Z');
        $rus = array(GetMessage("NOVAGR_SHOP_A"),GetMessage("NOVAGR_SHOP_B"),GetMessage("NOVAGR_SHOP_V"),GetMessage("NOVAGR_SHOP_G"),GetMessage("NOVAGR_SHOP_D"),GetMessage("NOVAGR_SHOP_E"),GetMessage("NOVAGR_SHOP_J"),GetMessage("NOVAGR_SHOP_Z"),GetMessage("NOVAGR_SHOP_I"),GetMessage("NOVAGR_SHOP_K"),GetMessage("NOVAGR_SHOP_L"),GetMessage("NOVAGR_SHOP_M"),GetMessage("NOVAGR_SHOP_N"),GetMessage("NOVAGR_SHOP_O"),GetMessage("NOVAGR_SHOP_P"),GetMessage("NOVAGR_SHOP_R"),GetMessage("NOVAGR_SHOP_S"),GetMessage("NOVAGR_SHOP_T"),GetMessage("NOVAGR_SHOP_U"),GetMessage("NOVAGR_SHOP_F"),GetMessage("NOVAGR_SHOP_H"),GetMessage("NOVAGR_SHOP_C"),GetMessage("NOVAGR_SHOP_C1"),GetMessage("NOVAGR_SHOP_S1"),GetMessage("NOVAGR_SHOP_S2"),GetMessage("NOVAGR_SHOP_E1"),GetMessage("NOVAGR_SHOP_U1"),GetMessage("NOVAGR_SHOP_A1"));
        $arSelect = array('NAME', 'CODE','IBLOCK_ID');

        $rsElement = CIBlockElement::GetList(array('NAME' => 'ASC'), $arFilter, false, false, $arSelect);
        $arResult['LAT'] = array();
        $arResult['RUS'] = array();
        while ($data = $rsElement -> Fetch())
        {
            $IBLOCK_ID = $data['IBLOCK_ID'];
            $let = mb_substr($data['NAME'], 0, 1);
            if( in_array($let, $lat) && !in_array($let, $arResult['LAT']) )
                $arResult['LAT'][] = $let;
            if( in_array($rus, $lat) && !in_array($let, $arResult['RUS']) )
                $arResult['RUS'][] = $let;

        }

        if(!empty($arResult['LETTER']))
        {
            $arFilter['NAME'] = $arResult['LETTER'].'%';
            $APPLICATION -> AddChainItem(GetMessage("SIMBOL_LABEL")." '".$arResult['LETTER']."'", "");
        }

        if( !empty($arResult['BRAND_CODE']) )
            $arFilter['CODE'] = $arResult['BRAND_CODE'];

        $arSelect = array(
            'ID',
            'NAME',
            'CODE',
            'PREVIEW_PICTURE',
            'DETAIL_TEXT',
            'PROPERTY_BRAND_CODE',
        );

        if($arResult['ORDABC'] > 0) $arNavStartParams = false;
        else {

            $arNavStartParams = array(
                "nPageSize" => $arParams['nPageSize'],
                //"iNumPage" => $iNumPage,
                "bShowAll" => false
            );

        }
        //  If the specified number of valid entries that do not need a navigation
        if ($arParams["COUNT_RECORDS"] > 0 ) {
            $arNavStartParams = false;
            $arNavStartParams = array(
                "nTopCount" => $arParams["COUNT_RECORDS"],
                "bShowAll" => false
            );
        }

        if($this->__templateName == "professionals") {
            $arFilter["PROPERTY_PROFESSIONAL_VALUE"] = "Для профессионалов";
            $arFilter["IBLOCK_ID"] = $IBLOCK_ID;
            $rsElementProf = CIBlockElement::GetList(
                array($arParams['SORT_FIELD'] => $arParams['SORT_BY']),
                $arFilter,
                false,
                $arNavStartParams,
                $arSelect
            );
            while($data = $rsElementProf -> GetNext())
            {
                $arResult['BRANDS']["PROFESSIONAL"][$data["PROPERTY_BRAND_CODE_VALUE"]] = $data;
                $PREVIEW_PICTURE_ID[$data['PREVIEW_PICTURE']] = $data['PREVIEW_PICTURE'];
            }
            $arFilter["PROPERTY_PROFESSIONAL_VALUE"] = false;
        }


        $rsElement = CIBlockElement::GetList(
            array($arParams['SORT_FIELD'] => $arParams['SORT_BY']),
            $arFilter,
            false,
            $arNavStartParams,
            $arSelect
        );
        while($data = $rsElement -> GetNext())
        {
            $arResult['TOP_BRANDS'][$data["PROPERTY_BRAND_CODE_VALUE"]] = $data;
            $PREVIEW_PICTURE_ID[$data['PREVIEW_PICTURE']] = $data['PREVIEW_PICTURE'];
        }


        $arResult['BRANDS_PROPERTY'] = [];
        $res = CIBlock::GetProperties(CATALOG_IBLOCK_ID, Array(), Array("CODE"=>"BRAND_REF"));
        if($res_arr = $res->Fetch()) {
            $dbHblock = HL\HighloadBlockTable::getList(
                array(
                    "filter" => array("NAME" => "BrandReference", "TABLE_NAME" => $res_arr["USER_TYPE_SETTINGS"]["TABLE_NAME"])
                )
            );
            while ($hldata = $dbHblock->Fetch())
            {
                $ID = $hldata["ID"];
                $hldata = HL\HighloadBlockTable::getById($ID)->fetch();
                $hlentity = HL\HighloadBlockTable::compileEntity($hldata);
                $entity_data_class = $hlentity->getDataClass();
                $entity_table_name = $hldata['TABLE_NAME'];
                $sTableID = 'tbl_'.$entity_table_name;

                $rsData = $entity_data_class::getList(array(
                    "select" => array('*'), //выбираем все поля
                    "order" => array("UF_SORT"=>"ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
                ));

                while ($arEnum = $rsData->fetch())
                {

                    $boolPict = true;
                    if (!isset($arEnum['UF_NAME']))
                    {
                        $boolName = false;
                        break;
                    }

                    if (!isset($arEnum['UF_FILE']) || (int)$arEnum['UF_FILE'] <= 0)
                        $boolPict = false;

                    if ($boolPict)
                    {
                        $arEnum['PREVIEW_PICTURE'] = CFile::GetFileArray($arEnum['UF_FILE']);
                        if (empty($arEnum['PREVIEW_PICTURE']))
                            $boolPict = false;
                    }

                    $descrExists = (isset($arEnum['UF_DESCRIPTION']) && (string)$arEnum['UF_DESCRIPTION'] !== '');
                    if ($boolPict)
                    {
                        if ($descrExists)
                        {
                            $width = $arParams["WIDTH_SMALL"];
                            $height = $arParams["HEIGHT_SMALL"];
                            $type = "PIC_TEXT";
                        }
                        else
                        {
                            $width = $arParams["WIDTH"];
                            $height = $arParams["HEIGHT"];
                            $type = "ONLY_PIC";
                        }

                        $arEnum['PREVIEW_PICTURE']['WIDTH'] = (int)$arEnum['PREVIEW_PICTURE']['WIDTH'];
                        $arEnum['PREVIEW_PICTURE']['HEIGHT'] = (int)$arEnum['PREVIEW_PICTURE']['HEIGHT'];
                        if (
                            $arEnum['PREVIEW_PICTURE']['WIDTH'] > $width
                            || $arEnum['PREVIEW_PICTURE']['HEIGHT'] > $height
                        )
                        {
                            $arEnum['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
                                $arEnum['PREVIEW_PICTURE'],
                                array("width" => $width, "height" => $height),
                                BX_RESIZE_IMAGE_PROPORTIONAL,
                                true
                            );

                            $arEnum['PREVIEW_PICTURE']['SRC'] = $arEnum['PREVIEW_PICTURE']['src'];
                            $arEnum['PREVIEW_PICTURE']['WIDTH'] = $arEnum['PREVIEW_PICTURE']['width'];
                            $arEnum['PREVIEW_PICTURE']['HEIGHT'] = $arEnum['PREVIEW_PICTURE']['height'];
                        }
                    }
                    elseif ($descrExists)
                    {
                        $type = "ONLY_TEXT";
                    }
                    else //Nothing to show
                    {
                        continue;
                    }

                    $arResult['BRANDS']["PROFESSIONAL"][$arEnum["UF_XML_ID"]]["URL"] = $arEnum["UF_LINK"];
                    $arResult['TOP_BRANDS'][$arEnum["UF_XML_ID"]]["URL"] = $arEnum["UF_LINK"];
                    $arResult['BRANDS_PROPERTY'][$arEnum["ID"]] = $arEnum;
                }

            }
        }


        if($this->__templateName == ".default") {
            $rows = count($arResult['TOP_BRANDS']) / 4;
            $brands = [];
            for ($i = 0; $i <= $rows;) {
                $start = $i * 4;
                $brands[$i] = array_slice($arResult['TOP_BRANDS'], $start, 4);
                $i++;
            }
            $arResult['BRANDS'] = $brands;
        }


        $arResult["NAV_STRING"] = $rsElement -> GetPageNavStringEx($navComponentObject, "", "bootstrap");


        if( ($arResult['BRAND_ID'] == 0) && ($arResult['ORDABC'] == 0))
        {
            $arFilter = array(
                'ACTIVE' => "Y",
                'IBLOCK_CODE' => $arParams['BRANDS_IBLOCK_CODE'],
                'PROPERTY_TOP_VALUE' => "Y",
            );
            $arSelect = array(
                'ID',
                'NAME',
                'PREVIEW_PICTURE',
                'DETAIL_TEXT',
                'PROPERTY_TOP',
                'CODE'
            );
            $arNavStartParams = array('nTopCount' => 1);
            $rsElement = CIBlockElement::GetList(array('RAND' => ''), $arFilter, false, $arNavStartParams, $arSelect);
            if($data = $rsElement -> GetNext())
            {
                $arResult['TOP'] = $data;
                $PREVIEW_PICTURE_ID[$data['PREVIEW_PICTURE']] = $data['PREVIEW_PICTURE'];
            }
        }

        if($arResult['ORDABC'] == 0)
        {
            $arFilter = "";
            foreach($PREVIEW_PICTURE_ID as $subval) $arFilter .= $subval.",";
            $rsFile = CFile::GetList(false, array('@ID' => $arFilter));
            while($sub_data = $rsFile -> GetNext())
            {
                $PREVIEW_PICTURE_SRC[$sub_data['ID']]
                    = "/upload/".$sub_data['SUBDIR']."/".$sub_data['FILE_NAME'];
            }
            foreach($PREVIEW_PICTURE_ID as $key => $val)
                $arResult['PREVIEW_PICTURE'][$key] = $PREVIEW_PICTURE_SRC[$val];
        }

    }
    if( !empty($arResult['BRAND_CODE']) )
        foreach($arResult['BRANDS'] as $val)
            $APPLICATION -> AddChainItem($val['NAME'], "");

    $this->setResultCacheKeys(array(
        "ID",
        "IBLOCK_TYPE_ID",
        "LIST_PAGE_URL",
        "NAV_CACHED_DATA",
        "NAME",
        "SECTION",
        "ELEMENTS",
        "IPROPERTY_VALUES",
        "ITEMS_TIMESTAMP_X",
    ));
    $this->IncludeComponentTemplate();
}
?>