<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
	$arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {	
	$arParams["COUNT_RECORDS"] = false;
}
if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";
if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";
$arParams['AJAX'] = $_REQUEST['AJAX'];
if($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;

//$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];

//if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arParams)))
//{
	$cache = \Bitrix\Main\Data\Cache::createInstance();
    if($cache->initCache(36000, 'arResult', '/brands.menu/listglobal')) {
       $arResult = $cache->getVars();
    } else {
		Loader::includeModule("highloadblock");
	
		if( Loader::includeModule("iblock") )
		{
			$arResult['BRAND_ID'] = (int)$_REQUEST['id'];
			$arResult['BRAND_CODE'] = $_REQUEST['elmid'];
			
			$arFilter = array(
				'ACTIVE' => "Y",
				'IBLOCK_CODE' => $arParams['BRANDS_IBLOCK_CODE']
			);
		
			$arSelect = array(
				'ID',
				'NAME',
				'CODE',
				'PREVIEW_PICTURE',
				'DETAIL_TEXT',
			);
		
			$IBLOCK_ID = 0;
			$IBLOCK_ID = CATALOG_IBLOCK_ID;
		
			if( !empty($arResult['BRAND_CODE']) )
				$arFilter['CODE'] = $arResult['BRAND_CODE'];
		
			
			if($arResult['ORDABC'] > 0) $arNavStartParams = false;
			else {
				
				$arNavStartParams = array(
					"nPageSize" => $arParams['nPageSize'],
					//"iNumPage" => $iNumPage,
					"bShowAll" => false
				);
			
			}
			//  If the specified number of valid entries that do not need a navigation
			if ($arParams["COUNT_RECORDS"] > 0 ) {
				$arNavStartParams = false;
				$arNavStartParams = array(
					"nTopCount" => $arParams["COUNT_RECORDS"],
					"bShowAll" => false
				);
			}
		
		
			/*Lib brands */
			$res = CIBlock::GetProperties($IBLOCK_ID, Array(), Array("CODE"=>"BRAND_REF"));
			if($res_arr = $res->Fetch()) {
				$dbHblock = HL\HighloadBlockTable::getList(
					array(
						"filter" => array("NAME" => "BrandReference", "TABLE_NAME" => $res_arr["USER_TYPE_SETTINGS"]["TABLE_NAME"])
					)
				);
				if ($hldata = $dbHblock->Fetch())
				{
					$ID = $hldata["ID"];
					$hldata = HL\HighloadBlockTable::getById($ID)->fetch();
					$hlentity = HL\HighloadBlockTable::compileEntity($hldata);
					$entity_data_class = $hlentity->getDataClass();
					$entity_table_name = $hldata['TABLE_NAME'];
					$sTableID = 'tbl_'.$entity_table_name;
		
				}
			}
		
			$arResult['BRANDS_PROPERTY'] = [];
			$arResult['BRANDS_LIST'] = [];
		
			$arFilter = array(
				'ACTIVE' => "Y",
				'IBLOCK_CODE' => $arParams['BRANDS_IBLOCK_CODE'],
				'INCLUDE_SUBSECTIONS' => "Y"
				//'PROPERTY_TOP_VALUE' => "Y",
			);
			$arSelect = array(
				'ID',
				'NAME',
				'PREVIEW_PICTURE',
				'DETAIL_TEXT',
				'PROPERTY_TOP',
				'CODE',
				'PROPERTY_BRAND_REF',
				'IBLOCK_SECTION_ID',
				'IBLOCK_ID',
			);
			$rsElement = CIBlockElement::GetList(array($arParams['SORT_FIELD']  => $arParams['SORT_BY']), $arFilter,false,false,$arSelect);
			while($dataBrands = $rsElement -> GetNextElement())
			{
				$dtaFields = $dataBrands->GetFields();
				$FirstLevelGroup = CIBlockSection::GetNavChain($dtaFields["IBLOCK_ID"], $dtaFields["IBLOCK_SECTION_ID"])->Fetch();
		
				if($dtaFields["PROPERTY_BRAND_REF_VALUE"]) {
					$rsData = $entity_data_class::getList(array(
						"select" => array('*'), //выбираем все поля
						"filter" => array(array("UF_DESCRIPTION"=>$dtaFields["PROPERTY_BRAND_REF_VALUE"])), //выбираем все поля
						"order" => array("UF_SORT"=>"ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
					));
		
					if ($arEnum = $rsData->fetch()) {
						$dtaFields["LINK"] = $arEnum["UF_LINK"];
						$dtaFields["NAME_BRAND"] = $arEnum["UF_NAME"];
						$arResult['BRANDS_LIST'][$FirstLevelGroup["ID"]][$dtaFields["NAME_BRAND"]] = $dtaFields;
					}
				}
		
				$arResult['BRANDS_PROPERTY'][$FirstLevelGroup["ID"]] = $FirstLevelGroup;
			}
		}
	
		/*$this->SetResultCacheKeys(array(
				"BRAND_CODE",
				"BRAND_ID",
				"BRANDS_LIST",
				"BRANDS_PROPERTY",
		));*/
		return $arResult;
	
		$cache->startDataCache();
		$cache->endDataCache($arResult);
    }
//}
?>