<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<section class="content_wrapper nobackground no_padding_bottom">
    <div class="wrapper center">
        <h1 class="page_heading small" data-aos="flip-up" data-aos-delay="400"><?= $APPLICATION->ShowTitle() ?></h1>
    </div>
    <div class="wrapper">
        <div class="white_wrapper" data-aos="fade-up" data-aos-delay="250">
            <? if (count($arResult['FAQ']) > 0) { ?>
                <div class="spoiler_wrapper">
                    <? foreach ($arResult['FAQ'] as $itemFaq) { ?>
                        <div class="spoiler">
                            <div class="spoiler_title"><? echo $itemFaq["NAME"]; ?></div>
                            <div class="spoiler_content withouticon">
                                <span class="lnr lnr-bullhorn"></span>
                                <? if($itemFaq["DETAIL_TEXT_TYPE"]=="text") {
                                    echo $itemFaq["~DETAIL_TEXT"];
                                } else { ?>
                                    <div class="grid x3 clearfix">
                                       <? echo $itemFaq["~DETAIL_TEXT"]; ?>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    <? } ?>
                </div>
            <? } else { ?>
                <div class="spoiler_wrapper empty_faq">
                    <p align="center" class="alert-message aos-init aos-animate col-12" data-aos="fade-up"
                       data-aos-delay="500">
                        Нет вопросов для отображения
                    </p>
                </div>
            <? } ?>
        </div>
    </div>
</section>