<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request,
    Bitrix\Main\Server;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
// Или более краткая форма:
$request = Context::getCurrent()->getRequest();
$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$arParams["bxAjaxIdBbitrix"] = $bxajaxid;
$component->bxajaxid = $bxajaxid;
CUtil::InitJSCore(array("image"));
if($request->get("bxajaxid")) {} else {?><script type="text/javascript"> var bxAjaxIdBbitrix = '<?=$bxajaxid?>';</script><? } ?>

<section class="content_wrapper nobackground clearfix">
    <?$APPLICATION->IncludeComponent("fijie:blog.message.groups", "", array(
        "SORT_FIELD" => "ACTIVE_FROM",
        "SORT_BY" => "DESC",
        "BRANDS_IBLOCK_CODE" => "blog",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "COUNT_RECORDS" => "",
    ),
        false
    );?>
    <div class="container">
            <div class="grid_blocks category_blog clearfix" id="comp_<?= $bxajaxid ?>">
                <? if (count($arResult["BLOG_MESSAGE"]) > 0) {
                foreach ($arResult["BLOG_MESSAGE"] as $ind => $CurPost) {
                    $className = "blog-post";
                    if ($ind == 0)
                    $className .= " blog-post-first";
                    elseif (($ind + 1) == count($arResult["BLOG_MESSAGE"]))
                    $className .= " blog-post-last";
                    if ($ind % 2 == 0)
                    $className .= " blog-post-alt";
                    $className .= " blog-post-year-" . $CurPost["DATE_PUBLISH_Y"];
                    $className .= " blog-post-month-" . IntVal($CurPost["DATE_PUBLISH_M"]);
                    $className .= " blog-post-day-" . IntVal($CurPost["DATE_PUBLISH_D"]);

                    $horisontal = false;
                    $class= false;

                    if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="vertical") {
                        $horisontal = false;
                        $class = "without_overlay without_overlay aos-init aos-animate";
                    } else if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="horizont") {
                        $horisontal = true;
                        $class= "";
                    }

                    $arPostField = $CurPost;

                    if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="vertical") {
                        $arFile = CFile::GetFileArray($arPostField["PREVIEW_PICTURE"]);
                        $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($arPostField["ACTIVE_FROM"]));

                        $html = "";
                        $html = '<div class="image aos-init aos-animate" data-aos="fade-left" data-aos-delay="300">';
                        $html .= '<div class="overlay">';
                        $html .= '<div class="position_wrap">';
                        $html .= '<div class="date aos-init aos-animate" data-aos="fade-left" data-aos-delay="350">'. $itemDate .'</div>';
                        $html .= '<div class="post_title aos-init aos-animate" data-aos="fade-left" data-aos-delay="400"><a href="/blog/post/fijiblognew/'.$CurPost["CODE"].'/" title="'. $CurPost["NAME"] .'">'.$CurPost["NAME"].'</a></div>';
                        $html .= '</div>';
                        $html .= '</div>';
                        $html .= '<img src="'.$arFile["SRC"].'">';
                        $html .= '</div>';
                    } else if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="horizont") {
                        $arFile = CFile::GetFileArray($arPostField["PREVIEW_PICTURE"]);
                        $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($arPostField["ACTIVE_FROM"]));
                        $html = "";
                        $html = '<div class="image" data-aos="fade-left" data-aos-delay="300">';
                        $html .= '<a href="/blog/post/fijiblognew/'.$CurPost["CODE"] .'/"><img src="'.$arFile["SRC"].'" alt="" class="js-tilt"></a>';
                        $html .= '</div>';
                        $html .= '<div class="date" data-aos="fade-left" data-aos-delay="350">'.$itemDate.'</div>';
                        $html .= '<div class="post_title" data-aos="fade-left" data-aos-delay="400"><a href="/blog/post/fijiblognew/'.$CurPost["CODE"] .'/" title="'. $CurPost["NAME"] .'">'. $CurPost["NAME"] .'</a></div>';
                        $html .= '<div class="description" data-aos="fade-left" data-aos-delay="500">'.$CurPost["PREVIEW_TEXT"].'</div>';
                    }
                    ?>
                    <div class="item" data-aos="fade-up" data-aos-delay="400">
                        <div class="post_item <?=$class?>" data-aos="fade-left" data-aos-delay="200">
                            <?=$html?>
                        </div>
                    </div>
                <?}


                } elseif (!empty($arResult["BLOG_MESSAGE"])) {
                    echo GetMessage("BLOG_BLOG_BLOG_NO_AVAIBLE_MES");
                }

                ?>
            </div>
    </div>
</section>
