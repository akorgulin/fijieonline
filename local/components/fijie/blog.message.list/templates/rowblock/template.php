<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request,
    Bitrix\Main\Server;


CUtil::InitJSCore(array("image"));
?>
<!--Category-->
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog_head clearfix">
                    <div class="title" data-aos="flip-up" data-aos-delay="200"><?=$arResult["HEADER"]?></div>
                    <div class="morelink" data-aos="fade-left" data-aos-delay="400"> <a href="/blog/" class="btn white iconlink">все записи из блога <span class="lnr lnr-arrow-right"></span></a> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <? if (count($arResult["BLOG_MESSAGE"]) > 0) {
            foreach ($arResult["BLOG_MESSAGE"] as $ind => $CurPost) {
                $className = "blog-post";
                if ($ind == 0)
                $className .= " blog-post-first";
                elseif (($ind + 1) == count($arResult["BLOG_MESSAGE"]))
                $className .= " blog-post-last";
                if ($ind % 2 == 0)
                $className .= " blog-post-alt";
                $className .= " blog-post-year-" . $CurPost["DATE_PUBLISH_Y"];
                $className .= " blog-post-month-" . IntVal($CurPost["DATE_PUBLISH_M"]);
                $className .= " blog-post-day-" . IntVal($CurPost["DATE_PUBLISH_D"]);

                $horisontal = false;
                $class= false;

                if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="vertical") {
                    $horisontal = false;
                    $class = "without_overlay without_overlay aos-init aos-animate";
                } else if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="horizont") {
                    $horisontal = true;
                    $class= "";
                }

                $arPostField = $CurPost;

                if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="vertical") {
                    $arFile = CFile::GetFileArray($arPostField["PREVIEW_PICTURE"]);
                    $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($arPostField["ACTIVE_FROM"]));

                    $html = "";
                    $html = '<div class="image aos-init aos-animate" data-aos="fade-left" data-aos-delay="300">';
                    $html .= '<div class="overlay">';
                    $html .= '<div class="position_wrap">';
                    $html .= '<div class="date aos-init aos-animate" data-aos="fade-left" data-aos-delay="350">'. $itemDate .'</div>';
                    $html .= '<div class="post_title aos-init aos-animate" data-aos="fade-left" data-aos-delay="400"><a href="/blog/post/fijiblognew/'.$CurPost["CODE"].'/" title="'. $CurPost["NAME"] .'">'.$CurPost["NAME"].'</a></div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<img src="'.$arFile["SRC"].'">';
                    $html .= '</div>';
                } else if($CurPost["PROPERTIES"]["TYPEIMG"]["VALUE_XML_ID"]=="horizont") {
                    $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($CurPost["ACTIVE_FROM"]));
                    $arFile = CFile::GetFileArray($CurPost["PREVIEW_PICTURE"]);
                    $html = "";
                    $html = '<div class="image" data-aos="fade-left" data-aos-delay="300">';
                    $html .= '<a href="/blog/post/fijiblognew/'.$CurPost["CODE"].'/"><img src="'.$arFile["SRC"].'" alt="" class="js-tilt"></a>';
                    $html .= '</div>';
                    $html .= '<div class="date">'.$itemDate.'</div>';
                    $html .= '<div class="description" data-aos="fade-left" data-aos-delay="500">'.$CurPost["PREVIEW_TEXT"].'</div>';
                }
                ?>
                <div class="col-4">
                    <div class="post_item" data-aos="fade-left" data-aos-delay="600">
                        <?=$html?>
                    </div>
                </div>
            <?}


            } elseif (!empty($arResult["BLOG_MESSAGE"])) {
                echo GetMessage("BLOG_BLOG_BLOG_NO_AVAIBLE_MES");
            }

            ?>
        </div>
    </div>
</section>
