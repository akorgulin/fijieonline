<?php
define("CATALOG_IBLOCK_ID",2);
define("NEWS_IBLOCK_ID",1);
define("BRANDS_IBLOCK_ID",5);
define("FAQ_IBLOCK_ID",2);
define("SLIDER_IBLOCK_ID",4);
define("DELIVERY_IBLOCK_ID",16);
define("GUARANTEE_IBLOCK_ID",15);
define("CALLBACK_IBLOCK_ID",6);


function AddOrderProperty($prop_id, $value, $order) {
    if (!strlen($prop_id)) {
        return false;
    }
    if (CModule::IncludeModule('sale')) {
        if ($arOrderProps = CSaleOrderProps::GetByID($prop_id)) {
            $db_vals = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $order, 'ORDER_PROPS_ID' => $arOrderProps['ID']));
            if ($arVals = $db_vals->Fetch()) {
                return CSaleOrderPropsValue::Update($arVals['ID'], array(
                    'NAME' => $arVals['NAME'],
                    'CODE' => $arVals['CODE'],
                    'ORDER_PROPS_ID' => $arVals['ORDER_PROPS_ID'],
                    'ORDER_ID' => $arVals['ORDER_ID'],
                    'VALUE' => $value,
                ));
            } else {
                return CSaleOrderPropsValue::Add(array(
                    'NAME' => $arOrderProps['NAME'],
                    'CODE' => $arOrderProps['CODE'],
                    'ORDER_PROPS_ID' => $arOrderProps['ID'],
                    'ORDER_ID' => $order,
                    'VALUE' => $value,
                ));
            }
    }
    }
}

function dump($var, $vardump = false, $return = false)
{
    static $dumpCnt;

    if (is_null($dumpCnt)) {
        $dumpCnt = 0;
    }
    ob_start();

    echo '<b>DUMP #' . $dumpCnt . ':</b> ';
    echo '<p>';
    echo '<pre>';
    if ($vardump) {
        var_dump($var);
    } else {
        print_r($var);
    }
    echo '</pre>';
    echo '</p>';

    $cnt = ob_get_contents();
    ob_end_clean();
    $dumpCnt++;
    if ($return) {
        return $cnt;
    } else {
        echo $cnt;
    }
}

function deleteGET($url, $name, $amp = true) {
    $url = str_replace("&amp;", "&", $url); // Заменяем сущности на амперсанд, если требуется
    list($url_part, $qs_part) = array_pad(explode("?", $url), 2, ""); // Разбиваем URL на 2 части: до знака ? и после
    parse_str($qs_part, $qs_vars); // Разбиваем строку с запросом на массив с параметрами и их значениями
    unset($qs_vars[$name]); // Удаляем необходимый параметр
    if (count($qs_vars) > 0) { // Если есть параметры
      $url = $url_part."?".http_build_query($qs_vars); // Собираем URL обратно
      if ($amp) $url = str_replace("&", "&amp;", $url); // Заменяем амперсанды обратно на сущности, если требуется
    }
    else $url = $url_part; // Если параметров не осталось, то просто берём всё, что идёт до знака ?
    return $url; // Возвращаем итоговый URL
}

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error(){

    if(!defined('ADMIN_SECTION') && defined('ERROR_404') && ERROR_404=='Y' || CHTTP::GetLastStatus() == "404 Not Found"){
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        define("error404chain",true);

        if(!$_REQUEST["id"] && BLOG_DETAIL=="Y") {
            LocalRedirect("/404.php",301);
        }
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header.php");
        include($_SERVER["DOCUMENT_ROOT"].'/404.php');
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer.php");
    }
    return false;
}

AddEventHandler('main', 'OnBeforeProlog', '_CheckBasketPage', 1);
function _CheckBasketPage(){

    if(!defined('ADMIN_SECTION') && CHTTP::GetLastStatus() != "404 Not Found"){
        global $APPLICATION;
        $dir = $APPLICATION->GetCurDir();
        if($dir=="/personal/cart/" || $dir=="/personal/"){
            if ($_GET["BasketDelete"] and CModule::IncludeModule("sale"))
            {
                if(count($_POST["arItemsSelected"])>0 && $_POST["arItemsSelected"][0]>0) {
                    foreach($_POST["arItemsSelected"] as $i=>$id):
                        //CSaleBasket::Delete($id);
                    endforeach;
                } elseif($_GET["BasketDelete"]=="yes") {
                    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
                }
            }
        }
    }
    return false;
}
?>