<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<!--Footer-->
<section class="footer">
    <div class="container">
        <div class="row">
            <div class="logo_header footer_logo" data-aos="flip-left" data-aos-delay="500">
                <a href="#">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/company_title.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </a></div>
            <div class="col-4" data-aos="fade-left" data-aos-delay="200">
                <div class="title" data-aos="fade-right" data-aos-delay="400">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/catalog_title.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "catalog",
                    array(
                        "ROOT_MENU_TYPE" => "left",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "catalog",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>
            <div class="col-4" data-aos="fade-left" data-aos-delay="400">
                <div class="title" data-aos="fade-right" data-aos-delay="600">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/shops_title.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "shops",
                    array(
                        "ROOT_MENU_TYPE" => "shops",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "shops",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>
            <div class="col-4" data-aos="fade-left" data-aos-delay="600">
                <div class="title" data-aos="fade-right" data-aos-delay="800">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/contact_title.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
                <div class="footer_contacts">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/contacts.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
                <div class="socials">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/social.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
            </div>
        </div>
    </div>
    <div class="downline">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="left">
                        <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/include/copyright.php",
                            Array(),
                            Array("MODE"=>"text")
                        );?>
                    </div>
                    <div class="right">
                        <div class="developer">
                            <?$APPLICATION->IncludeFile(
                                SITE_TEMPLATE_PATH."/include/author.php",
                                Array(),
                                Array("MODE"=>"text")
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Popup start-->

<!-- Auth form popup-->
<?php
$APPLICATION->IncludeComponent("bitrix:system.auth.form",
    "header-ajax",
    array(
        "REGISTER_URL" => "/register/",
        "PROFILE_URL" => "/personal/profile/",
        "FORGOT_URL" => "/personal/private/?forgot_password=yes",
        "SHOW_ERRORS" => "Y"
    ),
    false
); ?>
<!--Popup end-->


<script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/aos.js"></script>
<script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/tilt.jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    AOS.init({
        disable: 'phone'
    });
    const tilt = $('.js-tilt').tilt();

    $(document).ready(
        function () {
            $('input[type=checkbox],input[type=radio]').iCheck({
                checkboxClass: 'icheckbox_square-pink',
                radioClass: 'iradio_square-pink',
                increaseArea: '20%' // optional
            });

            <? if($APPLICATION->getCurDir()!="/personal/cart/") { ?>
                deleteCookie("giftID");
            <? } ?>


            $('#searchbox').keypress(function(e){
                if(e.which == 13){//Enter key pressed
                    $('form[name=searchq] input').val($(this).val());
                    $('form[name=searchq]').submit();
                }
            });
        }
    );
</script>

<?if($APPLICATION->GetCurDir()=="/blog/") { ?>
    <script type="text/javascript">
        $('.content_wrapper').imagesLoaded(function() {
            $('.category_blog').masonry({
                itemSelector: '.item'
            });
        });

        $(document).change(function () {
            $('.content_wrapper').imagesLoaded(function() {
                $('.category_blog').masonry({
                    itemSelector: '.item'
                });
            });
        });
    </script>
<? } ?>

<a href="javascript:" id="return-to-top"><span class="lnr lnr-chevron-up"></span></a>

</body>
</html>