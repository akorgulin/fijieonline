<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CUtil::InitJSCore();
CJSCore::Init(array("fx","jquery","masked_input","phone_number"));
//$curPage = $APPLICATION->GetCurPage(true);
CAjax::Init();
/* Редирект в корзину для ошибочных ссылок покупки без которых товары Ajax-ом в корзину не кладутся*/
if($APPLICATION->getCurDir()=="/basket/personal/cart/" || $APPLICATION->getCurDir()=="/basket/personal/") {
    LocalRedirect("/personal/cart/",302);
}
if($_REQUEST["logout"]=="yes") {
    LocalRedirect("/",301);
}
/* Для медработников проверяем свойство пользователя и ставим ему куку */
/*if(CUser::IsAuthorized()) {
    $arFilter = array("ID" => CUser::GetID());
    $arParams["SELECT"] = array("UF_MEDICAL");
    $arRes = CUser::GetList($by,$desc,$arFilter,$arParams);
    if ($res = $arRes->Fetch()) {
        if($res["UF_MEDICAL"]) {

            \Bitrix\Main\Context::getCurrent()->getResponse()->addCookie(
                new \Bitrix\Main\Web\Cookie('medicalFilter', true, time() - 3600)
            );

            \Bitrix\Main\Context::getCurrent()->getResponse()->addCookie(
                new \Bitrix\Main\Web\Cookie('medicalFilter', true)
            );
        }
    }
}*/


?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Language" content="ru">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="yandex-verification" content="371fe47d40d335fb" />
    <title><?php $APPLICATION->ShowTitle()?> | Fijie</title>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146927597-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-146927597-1');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(55176931, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/55176931" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" data-skip-moving="true">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter39212700 = new Ya.Metrika({
                        id:39212700,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/39212700" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

    <?php
    echo '<meta http-equiv="Content-Type" content="text/html; charset='.LANG_CHARSET.'"'.(true ? ' /':'').'>'."\n";
    $APPLICATION->ShowMeta("robots", false, true);
    $APPLICATION->ShowMeta("keywords", false, true);
    $APPLICATION->ShowMeta("description", false, true);
    $APPLICATION->ShowCSS(true, true);
    ?>

    <!--CSS-->
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/css/aos.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/icheck/skins/square/pink.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/fancybox3/jquery.fancybox.min.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/slick/slick.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/slick/slick-theme.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/css/simple-grid.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/css/style.css" rel="stylesheet" type="text/css" />
    <link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/css/card.css" rel="stylesheet" type="text/css" />

    <!--JavaScript-->
    <script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <?php
    $APPLICATION->ShowHeadStrings();
    $APPLICATION->ShowHeadScripts();
    ?>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/slick/slick.min.js" type="text/javascript"></script>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/jquery.matchHeight.js" type="text/javascript"></script>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/icheck/icheck.js"></script>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/template.js" type="text/javascript"></script>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/fancybox3/jquery.fancybox.min.js"></script>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/owlcarousel/owl.carousel.min.js"></script>

    <?if($APPLICATION->GetCurDir()=="/blog/") { ?>
    <script src="//unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/masonry.pkgd.min.js" type="text/javascript"></script>
    <? } ?>
    <script src="//<?=SITE_SERVER_NAME?><?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<meta name="google-site-verification" content="yUrqQCj1JCC5d3U93SaSv814-WgQL3HS2dMb3A6k0kw" />
</head>
<body <?if($APPLICATION->getCurPage()!="/") { ?>class="inside <?if(strstr($APPLICATION->getCurPage(),"/about/")){?>about<?}?> <?if(strstr($APPLICATION->getCurPage(),"/catalog/")){?>card<?}?>"<? } else {?>class="main"<?}?> >
<?$APPLICATION->IncludeComponent(
    "h2o:favorites.add",
    "",
    Array()
);?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<!-- overlay !-->
<div class="search_area fade">
    <div class="close-btn" id="close-search">
        <span class="lnr lnr-cross"></span>
    </div>
    <form name="searchq" action="/catalog/search/"><input type="hidden" name="q" value=""></form>
    <input  placeholder="Поисковой запрос + Enter" id="searchbox" name="qs" type="search" />
</div>
<!--- /overlay -->
<!--Topline-->
<div class="topline" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <div class="col-6 left">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include/header_phone.php",
                    Array(),
                    Array("MODE"=>"text")
                );?>
                <?$APPLICATION->IncludeComponent(
                    "vr:callback",
                    "",
                    Array(
                        "EMAIL_TO" => "",
                        "IBLOCK_ID" => CALLBACK_IBLOCK_ID,
                        "IBLOCK_TYPE" => "recall",
                        "INCLUDE_JQUERY" => "N",
                        "MAIL_TEMPLATE" => "FEEDBACK_FORM",
                        "PROPERTY_FIO" => "TITLE",
                        "PROPERTY_FORM_NAME" => "TITLE",
                        "PROPERTY_PAGE" => "TITLE"
                    )
                );?>
            </div>
            <div class="col-6 right">
                <?$APPLICATION->IncludeComponent("fijie:sale.basket.basket.line", "", array(
                    "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                    "SHOW_PERSONAL_LINK" => "N",
                    "SHOW_NUM_PRODUCTS" => "Y",
                    "SHOW_TOTAL_PRICE" => "N",
                    "SHOW_PRODUCTS" => "N",
                    "POSITION_FIXED" =>"N",
                    "SHOW_AUTHOR" => "Y",
                    "PATH_TO_REGISTER" => SITE_DIR."login/",
                    "PATH_TO_PROFILE" => SITE_DIR."personal/private/"
                ),
                    false,
                    array()
                );?>
            </div>
        </div>
    </div>
</div>
<!--Header-->
<div class="container header">
    <div class="row">
        <div class="col-12">
            <div class="logo_header" data-aos="flip-left" data-aos-delay="500">
                <a href="/">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/company_title.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </a>
            </div>
            <div class="navigation_area">
                <div class="search" data-aos="fade-right" data-aos-delay="500"> <span class="lnr lnr-magnifier"></span> </div>
                <div class="main_menu" data-aos="fade-left" data-aos-delay="500"> <a href="#" class="toggle-menu hidden-lg"><span class="burger"></span><span class="title">Открыть меню</span></a>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "desktop",
                        array(
                            "ROOT_MENU_TYPE" => "topmenu",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "1",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>



