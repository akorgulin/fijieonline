<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$com_path = '/bitrix/components/nbrains/slider/';
$this->addExternalCss($com_path."front/css/masterslider.css");
$this->addExternalJS($com_path."front/js/masterslider.js");
$arBg = 'style="background-color:#CODE#;"';
?>
<!--Slider-->
<div class="owl-carousel owl-theme main_slider sliderowl" data-aos="fade-up" data-aos-delay="200">
    <?foreach($arResult["ITEMS"] as $arItem){   ?>
        <? if(strlen($arItem["PREVIEW_TEXT"]) > 1){ ?>
        <? $code = "#".$arItem["PROPERTIES"]["COLOR"]["VALUE"];  ?>
        <div class="item" <?=str_replace("#CODE#",$code,$arBg)?>>
            <div class="center_box">

                <?if(is_array($arItem["PROPERTIES"]["PRODUCTS"]["VALUE"])) { ?>
                    <?if($product1=CIBlockElement::GetByID($arItem["PROPERTIES"]["PRODUCTS"]["VALUE"][0])->Fetch()) { ?>
                        <div class="parallax_01_img pulse" data-aos="fade-right" data-aos-delay="900" style="background:url(<?=CFile::GetPath($product1["PREVIEW_PICTURE"])?>)"></div>
                    <? } ?>
                    <?if($product2=CIBlockElement::GetByID($arItem["PROPERTIES"]["PRODUCTS"]["VALUE"][1])->Fetch()) { ?>
                        <div class="parallax_02_img pulse" data-aos="fade-right" data-aos-delay="700" style="background:url(<?=CFile::GetPath($product2["PREVIEW_PICTURE"])?>)"></div>
                    <? } ?>
                    <?if($product3=CIBlockElement::GetByID($arItem["PROPERTIES"]["PRODUCTS"]["VALUE"][2])->Fetch()) { ?>
                        <div class="parallax_03_img pulse" data-aos="fade-left" data-aos-delay="900" style="background:url(<?=CFile::GetPath($product3["PREVIEW_PICTURE"])?>)"></div>
                    <? } ?>
                    <?if($product4=CIBlockElement::GetByID($arItem["PROPERTIES"]["PRODUCTS"]["VALUE"][3])->Fetch()) { ?>
                        <div class="parallax_04_img pulse" data-aos="fade-up" data-aos-delay="900" style="background:url(<?=CFile::GetPath($product4["PREVIEW_PICTURE"])?>)"></div>
                    <? } ?>
                <? } else if(is_array($arItem["PROPERTIES"]["PICTURES"]["VALUE"])) { ?>
                    <?if($arItem["PROPERTIES"]["PICTURES"]["VALUE"][0]) { ?>
                        <div class="parallax_01_img pulse" data-aos="fade-right" data-aos-delay="900" style="background:url(<?=CFile::GetPath($arItem["PROPERTIES"]["PICTURES"]["VALUE"][0])?>)"></div>
                    <? } ?>
                    <?if($arItem["PROPERTIES"]["PICTURES"]["VALUE"][1]) { ?>
                        <div class="parallax_02_img pulse" data-aos="fade-right" data-aos-delay="900" style="background:url(<?=CFile::GetPath($arItem["PROPERTIES"]["PICTURES"]["VALUE"][1])?>)"></div>
                    <? } ?>
                    <?if($arItem["PROPERTIES"]["PICTURES"]["VALUE"][2]) { ?>
                        <div class="parallax_03_img pulse" data-aos="fade-right" data-aos-delay="900" style="background:url(<?=CFile::GetPath($arItem["PROPERTIES"]["PICTURES"]["VALUE"][2])?>)"></div>
                    <? } ?>
                    <?if($arItem["PROPERTIES"]["PICTURES"]["VALUE"][3]) { ?>
                        <div class="parallax_04_img pulse" data-aos="fade-right" data-aos-delay="900" style="background:url(<?=CFile::GetPath($arItem["PROPERTIES"]["PICTURES"]["VALUE"][3])?>)"></div>
                    <? } ?>
                <? } ?>
                <div class="center_block">
                    <?if($arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"]) { $headerBg = 'style="color:#'.$arItem["PROPERTIES"]["HEADER_COLOR"]["VALUE"].';"';  }?>
                    <div <?=$headerBg?> class="slider_title" data-aos="fade-left" data-aos-delay="300"><?=$arItem["NAME"]?></div>
                    <div class="slider_description" data-aos="fade-up" data-aos-delay="600">
                        <?=trim(html_entity_decode($arItem["PREVIEW_TEXT"]));?>
                    </div>
                    <?if($arItem["PROPERTIES"]["SHOW_BUTTON"]["VALUE"]=="Y") { ?>
                    <div class="slider_button" data-aos="fade-up" data-aos-delay="1200">
                        <?if($arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"]) { ?>
                            <a href="<?=$arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"]?>">подробнее<span class="lnr lnr-arrow-down"></span></a>
                        <? } ?>
                    </div>
                    <? } ?>
                </div>
            </div>
        </div>
        <? } else { ?>
        <div class="item" style="background: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>) no-repeat 0 0; background-size: cover; ">
            <?if($arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"]) { ?>
                <a style="width:100%; height: 100%; display:block; " href="<?=$arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"]?>">&nbsp;</a>
            <? } ?>
        </div>
        <? } ?>
    <?}?>
</div>
<script type="text/javascript">
    $('div.sliderowl').owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
</script>
