<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->createFrame()->begin("");
$com_path = '//'.SITE_SERVER_NAME.'/local/templates/fijie/components/h2o/favorites.line/.default';
$this->addExternalCss($com_path."/line.css");
$this->addExternalJS($com_path."/line.js");
?>
<a href="<?= $arParams['URL_LIST'] ?>" class="iconlink aos-init aos-animate favorites-ico" data-aos="fade-left" data-aos-delay="900">
    <span class="lnr lnr-heart"></span><? if ($arParams['URL_LIST'] != "") { ?><span class="num"><?= intval($arResult['COUNT']) ?></span><? } else { ?><span class="num hidden"><?= $arResult['COUNT'] ?></span><? } ?>
</a>