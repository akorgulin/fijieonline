function updateFavoritesLine(){
	var link = '?AJAX_CALL_FAVORITES_LINE=Y';
	jQuery.ajax(link, {
		type: "POST",
		success: function (data) {
			var obj = jQuery("<div />").html(data);
			$num = obj.find(".favorites-ico span.num").html();
			jQuery(".favorites-ico").html('<span class="lnr lnr-heart"></span>'+'<span class="num">'+$num+'</span>');
		},
		dataType: "html"
	});
}