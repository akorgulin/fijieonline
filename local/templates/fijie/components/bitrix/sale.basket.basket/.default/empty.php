<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>
<script type="text/javascript">
    var basketJSParams = {'SALE_DELETE':'','SALE_DELAY':'','SALE_TYPE':'','TEMPLATE_FOLDER':'/local/templates/fijie/components/bitrix/sale.basket.basket/.default','DELETE_URL':'/personal/cart/?basketAction=delete&id=#ID#','DELAY_URL':'/personal/cart/?basketAction=delay&id=#ID#','ADD_URL':'/personal/cart/?basketAction=add&id=#ID#'};
</script>
<section class="content_wrapper cart_page nobackground">
    <div class="wrapper center">
        <h1 class="page_heading small" data-aos="flip-up" data-aos-delay="400"><?=Loc::getMessage("SBB_EMPTY_BASKET_TITLE")?></h1>
    </div>
    <div class="wrapper center emptycart">
        <?
        if (!empty($arParams['EMPTY_BASKET_HINT_PATH']))
        {
            ?>
        <p class="alert-message" data-aos="fade-up" data-aos-delay="500">
                <?=Loc::getMessage(
                    'SBB_EMPTY_BASKET_HINT',
                    [
                        '#A1#' => '<a href="'.$arParams['EMPTY_BASKET_HINT_PATH'].'">',
                        '#A2#' => '</a>',
                    ]
                )?>
            </p>
            <?
        }
        ?>
        <div class="buttons_link">
            <a href="/" class="btn mint iconlink iconleft" data-aos="fade-up" data-aos-delay="1000"><span class="lnr lnr-arrow-left"></span>На главную</a>
            <a href="/catalog/shoes/" class="btn mint iconlink" data-aos="fade-up" data-aos-delay="1200">Каталог<span class="lnr lnr-arrow-right"></span></a>
        </div>
    </div>
</section>