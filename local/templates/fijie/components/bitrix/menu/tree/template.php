<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$brandsList = $APPLICATION->IncludeComponent("fijie:brands_menu", "bannermenu", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "BRANDS_IBLOCK_CODE" => "clothes",
    "CACHE_TYPE" => "Y",
	"CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "5"
),
    false
);

$brands_list = [];
$brandsRubriks = array_keys($brandsList['BRANDS_PROPERTY']);
if(count($brandsRubriks)>0) {
    
    foreach($brandsRubriks as $iRow=>$section_id) {
        $brands_list[$section_id] .= '<ul>';

        foreach($brandsList['BRANDS_LIST'][$section_id] as $iRow=>$itemBrandRow) {
            $href = "";
            if($itemBrandRow["LINK"]) {
                $href=" href='".$itemBrandRow["LINK"]."' ";
            }

            $brandRow = '<li><a '.$href.'>'.ucFirst($itemBrandRow["NAME_BRAND"]).'</a></li>';
            $brands_list[$section_id] .= $brandRow;
        }

        $brands_list[$section_id] .= '</ul>';
    }
}

?>

<?if (!empty($arResult)):?>
<ul class="general">

<?
$blockSubmenu = '<div class="col-4 brands">
    <div class="menu_title">По брендам</div>
</div>
<div class="col-4">
    <div class="banners_images">#ITEMS#</div>
</div>';

$itemBanners = [];
$previousLevel = 0;
$parent_number = 0;
$brands = [];

foreach($arResult as $arItem){
    $arFilterS = Array('ID'=>$arItem["PARAMS"]["ID_ELEMENT"], 'IBLOCK_ID'=>2, 'GLOBAL_ACTIVE'=>'Y', 'UF_BACKGROUND_IMAGE');

    $res = CIBlockSection::GetList(Array($by=>$order), $arFilterS, false, Array("ID","IBLOCK_SECTION_ID",'SECTION_PAGE_URL','UF_*'));
    if ($section = $res->fetch())
    {
        if($section["UF_BACKGROUND_IMAGE"]>0){
            $itemBanners[] = '<div class="section_item_'.$section["IBLOCK_SECTION_ID"].'"><div class="item"><a href="'.$section['SECTION_PAGE_URL'].'"><img src="'.CFile::GetPath($section["UF_BACKGROUND_IMAGE"]).'" alt=""></a></div></div>';
        }
    }

    $list = "<ul></ul>";
    if($brands_list[$arItem["PARAMS"]["ID_ELEMENT"]]!=NULL) { $list = $brands_list[$arItem["PARAMS"]["ID_ELEMENT"]]; }
    $brands[$arItem["PARAMS"]["ID_ELEMENT"]] = $list;
}

$blockSubmenu = str_replace("#ITEMS#",implode("",$itemBanners),$blockSubmenu);
$pushBlocks = "";


$blockSubmenuReplaced = false;
$parentReplaced = false;
foreach($arResult as $arItem):

    ?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></div>".($blockSubmenu)."</div></div></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>
			<li<?if($arItem["CHILD_SELECTED"] !== true):?> class="menu-close"<?endif?> data-id="<?=$arItem["PARAMS"]["ID_ELEMENT"]?>">
				<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                    <div <?if($parent_number==0){?>style="display: block;"<?}else{?>style="display: none;"<?}?> class="tab-content tab-content-<?=$arItem["PARAMS"]["ID_ELEMENT"]?>" id="<?=$arItem["PARAMS"]["ID_ELEMENT"]?>">
                        <div class="container">
                                <div class="row">
                                        <div class="col-4">
                                                <div class="menu_title"><?=$arItem["TEXT"]?></div>
                                                <ul>
                    <?$parent_number++;?>
	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>
				<li data-id="<?=$arItem["PARAMS"]["ID_ELEMENT"]?>">
					<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				</li>
		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></div>".($blockSubmenu)."</div></div></div></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<script type="text/javascript">
    /*Перестраиваем наше мегаменю согласно вёрстке*/
    $(document).ready(function() {
        var brands = <?=json_encode($brands)?>;
        /*console.log(brands);*/
        $("ul.general>li").each(function(index) {
            var content = $(this).find(">a+div.tab-content");
            var id = $(this).data("id");
            var brandslist = "";
            if(content.length==0) {

                if(brands[id]!="<ul></ul>" && brands[id]!=undefined) {
                    brandslist = '<div class="menu_title">По брендам</div>';
                    brandslist += brands[id];
                }
                $("ul.general").before($('<div style="display: none;" class="tab-content tab-content-'+id+'" id="'+id+'">' +
                    '<div class="container">' +
                    '<div class="row ">' +
                        '<div class="col-4 brands">' + brandslist +
                        '</div>' +
                        '<div class="col-4">' +
                                '<div class="banners_images empty_banners">' +
                                    '<div class="section_item_7"><div class="item">&nbsp;</div>' +
                                    '<div class="item">&nbsp;</div></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '</div></div>'));


            } else if(content.length==1) {

                var $sectionitems = content.find("div.banners_images div.section_item_"+id).clone();
                $("ul.general div.tab-content-"+id+" div.banners_images").empty();

                if($sectionitems.length==0) {
                    $("ul.general div.tab-content-"+id+" div.banners_images").addClass("empty_banners");
                    $("ul.general div.tab-content-"+id+" div.banners_images").append($('<div class="item">&nbsp;</div>'));
                    $("ul.general div.tab-content-"+id+" div.banners_images").append($('<div class="item">&nbsp;</div>'));
                } else {
                    $("ul.general div.tab-content-"+id+" div.banners_images").removeClass("empty_banners");
                    $("ul.general div.tab-content-"+id+" div.banners_images").append($sectionitems);
                }

                $("ul.general div.tab-content-"+id+" div.brands").empty();
                if(brands[id]!="<ul></ul>" && brands[id]!=undefined) {

                    brandslist = '<div class="menu_title">По брендам</div>';
                    brandslist += brands[id];
                    $("ul.general div.tab-content-"+id+" div.brands").append(brandslist);
                }

                $("ul.general").before(content);
            }
        });
        $("#v-nav").prepend($("ul.general"));
    });
</script>
<?endif?>