<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->AddChainItem($arResult["NAME"]);
?>
<section class="gr_section">
	<div class="width_site">
		<div class="dop_menu cf">
			<? $APPLICATION->IncludeComponent(
				"bitrix:breadcrumb",
				"chain_card",
				Array(
					"COMPONENT_TEMPLATE" => ".default",
					"START_FROM" => "0",
					"PATH" => "",
					"SITE_ID" => "s1"
				)
			);?>
		</div>
		<div class="personal_account">
			<div class="name_block">
				<span class="block_marble shadow2 cr30">~ ������ ~</span>
			</div>

			<h1><?=$arResult["NAME"]?></h1>

			<div class="text_block shadow_block cr30">
				<article>
					<?echo $arResult["DETAIL_TEXT"];?>
				</article>
			</div><br>
            <? Novagroup_Classes_General_Main::getView('catalog.element','yashare'); ?>
		</div>
	</div>
</section>
