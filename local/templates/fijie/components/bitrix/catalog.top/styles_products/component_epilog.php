<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var string $this $templateFolder */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
switch ($arParams['VIEW_MODE'])
{
	case 'BANNER':
		$APPLICATION->AddHeadScript($templateFolder.'/cript.js');
		$APPLICATION->SetAdditionalCSS($templateFolder.'/style.css');
	case 'SLIDER':
		$APPLICATION->AddHeadScript($templateFolder.'/script.js');
		$APPLICATION->SetAdditionalCSS($templateFolder.'/style.css');
		break;
	case 'SECTION':
	default:
		$APPLICATION->AddHeadScript($templateFolder.'/script.js');
		$APPLICATION->SetAdditionalCSS($templateFolder.'/style.css');
		break;
}

\Bitrix\Main\Page\Asset::getInstance()->addJs("/local/templates/jevelir_new_adapt/js/zoomsl-3.0.min.js");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/product.js');

if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}
?>