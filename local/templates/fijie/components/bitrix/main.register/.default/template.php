<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


?>

<!--Authorisation page-->
<section class="content_wrapper registration">
    <div class="parallax_01" data-aos="fade-down" data-aos-delay="350"></div>
    <div class="parallax_02" data-aos="fade-up" data-aos-delay="450"></div>
    <div class="parallax_03" data-aos="fade-down" data-aos-delay="450"></div>
    <div class="parallax_04" data-aos="fade-right" data-aos-delay="450"></div>
    <div class="container page_registration_wrapper">
        <div class="row">
            <div class="col-12 center">

                <? if ($USER->IsAuthorized()): ?>

                    <p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>

                <? else: ?>
                    <div class="close_popup shadow_popup cr50pr"></div>
                    <?
                    if (count($arResult["ERRORS"]) > 0):
                        foreach ($arResult["ERRORS"] as $key => $error)
                            if (intval($key) == 0 && $key !== 0)
                                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);

                        ShowError(implode("<br />", $arResult["ERRORS"]));

                    elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
                        ?>
                        <p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
                    <? endif ?>
                    <h1 class="page_heading aos-init aos-animate" data-aos="flip-up"
                        data-aos-delay="200"><?= GetMessage("AUTH_REGISTER_NEW") ?></h1>

                    <p class="description aos-init aos-animate" data-aos="fade-up" data-aos-delay="300"><? echo GetMessage("REGISTER_TEXT_REGISTER") ?></p>

                    <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform"
                          enctype="multipart/form-data" onsubmit="yaCounter55176931.reachGoal('register'); return true;">
                        <?
                        if ($arResult["BACKURL"] <> ''):
                            ?>
                            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                        <?
                        endif;

                        list($arResult["SHOW_FIELDS"][3], $arResult["SHOW_FIELDS"][8]) = array($arResult["SHOW_FIELDS"][8], $arResult["SHOW_FIELDS"][3]);
                        list($arResult["SHOW_FIELDS"][8], $arResult["SHOW_FIELDS"][9]) = array($arResult["SHOW_FIELDS"][9], $arResult["SHOW_FIELDS"][8]);
                        $login = randString(7, array(
                            "abcdefghijklnmopqrstuvwxyz",
                            "ABCDEFGHIJKLNMOPQRSTUVWX­YZ",
                            "0123456789",
                        ));
                        $new_password = randString(7);
                        $arResult["SHOW_FIELDS"][10] = $arResult["SHOW_FIELDS"][2];
                        $arResult["SHOW_FIELDS"][11] = $arResult["SHOW_FIELDS"][9];
                        unset($arResult["SHOW_FIELDS"][2]);
                        unset($arResult["SHOW_FIELDS"][9]);
                        ?>

                        <div class="forms-grid">
                            <? foreach ($arResult["SHOW_FIELDS"] as $i => $FIELD) { ?>
                                <?
                                $class = (($i % 2) ? "right" : "left");
                                if($i==10) $class="right";
                                if($i==11) $class="left";
                                if($i==3) $class="left";
                                if($i==7) $class="right";
                                if($i==8) $class="left";
                                ?>
                                <div class="forms-col-6 <?= $class ?> <?if($FIELD=="LOGIN"){?>hidden-xs<?}?>" data-aos="fade-right" data-aos-delay="350">
                                    <div class="border-form">
                                        <div class="without_icon_input">
                                            <? if ($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true) { ?>

                                                <? echo GetMessage("main_profile_time_zones_auto") ?>
                                                <? if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"): ?>
                                                <span class="starrequired">*</span>
                                            <? endif ?>
                                                <select name="REGISTER[AUTO_TIME_ZONE]"
                                                        onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
                                                    <option value=""><? echo GetMessage("main_profile_time_zones_auto_def") ?></option>
                                                    <option value="Y"<?= $arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : "" ?>><? echo GetMessage("main_profile_time_zones_auto_yes") ?></option>
                                                    <option value="N"<?= $arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : "" ?>><? echo GetMessage("main_profile_time_zones_auto_no") ?></option>
                                                </select>

                                                <? echo GetMessage("main_profile_time_zones_zones") ?>
                                                <select name="REGISTER[TIME_ZONE]"<? if (!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"' ?>>
                                                    <? foreach ($arResult["TIME_ZONE_LIST"] as $tz => $tz_name): ?>
                                                        <option value="<?= htmlspecialcharsbx($tz) ?>"<?= $arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : "" ?>><?= htmlspecialcharsbx($tz_name) ?></option>
                                                    <? endforeach ?>
                                                </select>

                                            <? } else { ?>
                                            <?
                                            switch ($FIELD) {
                                            case "PASSWORD":
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                            <input size="30" type="password"
                                                   placeholder="<?= GetMessage("REGISTER_FIELD_" . $FIELD) ?>"
                                                   name="REGISTER[<?= $FIELD ?>]"
                                                   value="<?= $new_password ?>" autocomplete="off"/>

                                            <?
                                            if ($arResult["SECURE_AUTH"]): ?>
                                                <span class="bx-auth-secure" id="bx_auth_secure" title="<?
                                                echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                                                        <div class="bx-auth-secure-icon"></div>
                                                    </span>
                                                <noscript>
                                                    <span class="bx-auth-secure" title="<?
                                                    echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                                    </span>
                                                </noscript>
                                                <script type="text/javascript">
                                                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                                </script>
                                            <?
                                            endif ?>
                                            <?
                                            break;
                                            case "CONFIRM_PASSWORD":
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                            <input size="30" type="password"
                                                   placeholder="<?= GetMessage("REGISTER_FIELD_" . $FIELD) ?>"
                                                   name="REGISTER[<?= $FIELD ?>]"
                                                   value="<?= $new_password ?>" autocomplete="off"/>
                                            <?
                                            break;
                                            case "LOGIN":
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                            <input size="30" type="password"
                                                   placeholder="<?= GetMessage("REGISTER_FIELD_" . $FIELD) ?>"
                                                   name="REGISTER[<?= $FIELD ?>]"
                                                   value="<?= $login ?>" autocomplete="off"/>
                                            <?
                                            break;

                                            case "PERSONAL_GENDER":
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <select name="REGISTER[<?= $FIELD ?>]">
                                                    <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                                    <option value="M"<?= $arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                                    <option value="F"<?= $arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                                                </select><?
                                            break;

                                            case "PERSONAL_COUNTRY":
                                            case "WORK_COUNTRY":
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <select name="REGISTER[<?= $FIELD ?>]"><?
                                                foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {
                                                    ?>
                                                    <option value="<?= $value ?>"<?
                                                    if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<? endif ?>><?= $arResult["COUNTRIES"]["reference"][$key] ?></option>
                                                    <?
                                                }
                                                ?></select><?
                                            break;

                                            case "PERSONAL_PHOTO":
                                            case "WORK_LOGO":
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input size="30" type="file" name="REGISTER_FILES_<?= $FIELD ?>" /><?
                                                break;

                                                case "PERSONAL_NOTES":
                                            case "WORK_NOTES":
                                                ?><textarea cols="30" rows="5"
                                                            name="REGISTER[<?= $FIELD ?>]"><?= $arResult["VALUES"][$FIELD] ?></textarea><?
                                            break;
                                            default:
                                            ?>
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <?
                                            if ($FIELD == "PERSONAL_BIRTHDAY") { ?>
                                                <small><?= $arResult["DATE_FORMAT"] ?></small><br/><? } ?>
                                                <input size="30" type="text" placeholder="<?
                                                if ($FIELD == "PERSONAL_BIRTHDAY"):?><?= $arResult["DATE_FORMAT"] ?><? endif; ?><?= GetMessage("REGISTER_FIELD_" . $FIELD) ?>"
                                                       name="REGISTER[<?= $FIELD ?>]"
                                                       value="<?= $arResult["VALUES"][$FIELD] ?>"
                                                       autocomplete="off"/><?
                                                if ($FIELD == "PERSONAL_BIRTHDAY")
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:main.calendar',
                                                        '',
                                                        array(
                                                            'SHOW_INPUT' => 'N',
                                                            'FORM_NAME' => 'regform',
                                                            'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                                                            'SHOW_TIME' => 'N'
                                                        ),
                                                        null,
                                                        array("HIDE_ICONS" => "Y")
                                                    );
                                            } ?>
                                            <? } ?>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>

                            <? // ********************* User properties ***************************************************?>
                            <? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>

                                <? strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB") ?>
                                <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
                                    <div class="for_input shadow_input cr13">
                                        <? $arUserField["EDIT_FORM_LABEL"] ?>
                                        :<? if ($arUserField["MANDATORY"] == "Y"): ?>
                                        <span class="starrequired">*</span><? endif; ?></td>
                                        <td>
                                            <? $APPLICATION->IncludeComponent(
                                                "bitrix:system.field.edit",
                                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y")); ?>
                                    </div>
                                <? endforeach; ?>
                            <? endif; ?>
                            <? // ******************** /User properties ***************************************************?>
                            <?
                            /* CAPTCHA */
                            if ($arResult["USE_CAPTCHA"] == "Y") { ?>
                                <?
                                GetMessage("REGISTER_CAPTCHA_TITLE") ?>
                                <div class="forms-col-6 left" data-aos="fade-right" data-aos-delay="350">
                                    <div class="border-form">
                                        <div class="without_icon_input">
                                            <div class="icon"><span class="lnr lnr-user"></span></div>
                                            <input type="text" name="captcha_word" maxlength="50" value=""
                                                   placeholder="<?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="forms-col-6 right" data-aos="fade-right" data-aos-delay="350">
                                    <div class="border-form">
                                        <div class="without_icon_input">
                                            <div class="captcha">
                                                <div align="left">
                                                    <input type="hidden" name="captcha_sid"
                                                           value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                                                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"
                                                         width="220" height="48" alt="CAPTCHA"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?
                            }
                            /* !CAPTCHA */
                            ?>
                        </div>

                        <div class="buttons_link">
                        <input type="submit" name="register_submit_button"
                               value="<?= GetMessage("AUTH_REGISTER_NEW") ?>" class="red_submit shadow"
                               style="display:none;">
                        <input type="hidden" name="register_submit_button" class="bt_blue big shadow"
                               value="<?= GetMessage("AUTH_REGISTER_NEW") ?>" style="display:none;"/>
                        <a href="#Login" onclick="javascript:$('form[name=regform]').submit();"
                           class="btn mint iconlink" data-aos="fade-up"
                           data-aos-delay="500"><?= GetMessage("AUTH_REGISTER_NEW_BUTTON") ?><span
                                    class="lnr lnr-arrow-right"></span></a>
                        </div>
                    </form>


                    <p class="info_small aos-init aos-animate" data-aos="fade-up" data-aos-delay="550"><? echo GetMessage("REGISTER_TEXT_REGISTER_AFTERFORM") ?></p>
                <? endif ?>

            </div>
        </div>
    </div>


    <?$APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/include/advantages.php",
        Array(),
        Array("MODE"=>"text")
    );?>

</section>
