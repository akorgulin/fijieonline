<?
$MESS["AUTH_PLEASE_AUTH_NEW"] = "Пожалуйста, авторизуйтесь";
$MESS["AUTH_LOGIN_NEW"] = "Логин";
$MESS["AUTH_PASSWORD_NEW"] = "Пароль";
$MESS["AUTH_REMEMBER_ME_NEW"] = "Запомнить меня";
$MESS["AUTH_AUTHORIZE_NEW"] = "Войти";
$MESS["AUTH_REGISTER_NEW"] = "Авторизоваться";
$MESS["AUTH_FIRST_ONE_NEW"] = "Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2_NEW"] = "Забыли пароль?";
$MESS["AUTH_CAPTCHA_PROMT_NEW"] = "Введите слово на картинке";
$MESS["AUTH_TITLE_NEW"] = "Войти на сайт";
$MESS["AUTH_SECURE_NOTE_NEW"]="Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE_NEW"]="Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
?>