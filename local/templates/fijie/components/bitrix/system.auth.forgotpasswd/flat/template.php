<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>


<!--Forgot page-->
<section class="content_wrapper registration">
    <div class="parallax_01" data-aos="fade-down" data-aos-delay="350"></div>
    <div class="parallax_02" data-aos="fade-up" data-aos-delay="450"></div>
    <div class="parallax_03" data-aos="fade-down" data-aos-delay="450"></div>
    <div class="parallax_04" data-aos="fade-right" data-aos-delay="450"></div>
    <div class="container page_registration_wrapper">
        <div class="row">
            <div class="col-12 center">

                <h4 class="aos-init aos-animate" data-aos="flip-up" data-aos-delay="200"><?=GetMessage("AUTH_GET_CHECK_STRING")?></h4>
                <?
                if(!empty($arParams["~AUTH_RESULT"])):
                    $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
                    ?>
                    <div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
                <?endif?>

                <p class="description aos-init aos-animate" data-aos="fade-up" data-aos-delay="300"><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>

                <form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                    <?if($arResult["BACKURL"] <> ''):?>
                            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?endif?>
                    <input type="hidden" name="AUTH_FORM" value="Y">
                    <input type="hidden" name="TYPE" value="SEND_PWD">

                    <div class="forms-col-12">
                        <div class="border-form">
                            <p class="description aos-init aos-animate hidden-xs" data-aos="fade-up" data-aos-delay="300"><?echo GetMessage("AUTH_LOGIN_EMAIL")?></p>
                            <input style="background-color: #fff; width:60%; display: inline-block;" type="text" name="USER_LOGIN" maxlength="255" placeholder="<?echo GetMessage("AUTH_LOGIN_EMAIL")?>" value="<?=$arResult["LAST_LOGIN"]?>" />
                            <input type="hidden" name="USER_EMAIL" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" class="btn btn-primary" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
                        </div>
                    </div>

                    <?if ($arResult["USE_CAPTCHA"]):?>
                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

                            <div class="bx-authform-formgroup-container">
                                <div class="bx-authform-label-container"><?echo GetMessage("system_auth_captcha")?></div>
                                <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                                <div class="bx-authform-input-container">
                                    <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                                </div>
                            </div>

                    <?endif?>

                    <? /*
                    <div class="bx-authform-formgroup-container">
                        <input type="submit" class="btn btn-primary" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
                    </div> */ ?>

                    <div class="bx-authform-link-container hidden-xs">
                        <p class="description aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
                        <a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a>
                        </p>
                    </div>

                </form>

                <script type="text/javascript">
                document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
                document.bform.USER_LOGIN.focus();
                </script>
            </div>
        </div>
    </div>


    <?$APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/include/advantages.php",
        Array(),
        Array("MODE"=>"text")
    );?>


</section>