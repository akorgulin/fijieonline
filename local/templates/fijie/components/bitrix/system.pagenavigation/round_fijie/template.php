<? use Bitrix\Conversion\Internals\MobileDetect;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

$colorSchemes = array(
    "green" => "bx-green",
    "yellow" => "bx-yellow",
    "red" => "bx-red",
    "blue" => "bx-blue",
);
if (isset($colorSchemes[$arParams["TEMPLATE_THEME"]])) {
    $colorScheme = $colorSchemes[$arParams["TEMPLATE_THEME"]];
} else {
    $colorScheme = "";
}

$detect = new MobileDetect;
?>
<? if ($arResult["nEndPage"] > 1 && $arResult["NavPageNomer"] < $arResult["nEndPage"]) { ?>

    <div id="btn_" class="center btn_more" <?if(!$detect->isMobile()) { ?> data-aos="fade-up" data-aos-delay="400" <?}?>>
        <a data-ajax-id="" href="javascript:void(0)" data-record-count="<?= $arResult["NavRecordCount"]?>" data-show-more="<?= $arResult["NavNum"] ?>"
           data-next-page="<?= ($arResult["NavPageNomer"] + 1) ?>"
           data-max-page="<?= $arResult["nEndPage"] ?>" <? $arResult["sUrlPath"] ?><? $strNavQueryString ?><? $arResult["NavNum"] ?><? ($arResult["NavPageNomer"] + 1) ?>
           class="ajax_loadmore"><span class="lnr lnr-arrow-down"></span>Загрузить ещё</a>
    </div>
<? } ?>
<script type="text/javascript">
    $(document).ready(function ($) {

        $("div.btn_more").attr("id",bxAjaxIdBbitrix);
        $("div.btn_more>a").attr("data-ajax-id",bxAjaxIdBbitrix);
        $("div.btn_more>a").data("ajax-id",bxAjaxIdBbitrix);

        $(document).on('click', 'div.btn_more>a', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var btn = $(this);
            var page = btn.attr('data-next-page');
            var max_page = btn.attr('data-max-page');
            var id = btn.attr('data-show-more');
            var bx_ajax_id_bitrix = btn.attr('data-ajax-id');
            var record_count = btn.attr('data-record-count');
            var block_id = "#comp_" + bx_ajax_id_bitrix;

            var defaultData = {
                bxAjaxidBitrix: bx_ajax_id_bitrix
            };
            defaultData['PAGEN_' + id] = page;
            $.ajax({
                type: "POST",
                url: window.location.href,
                dataType: "html",
                data: defaultData,
                cache: false,
                timeout: 3000,
                success: function (data) {
                    var dom = $(data);
                    $blockAppend = dom.find(block_id).find("div.product-item-container");
					$more = dom.find("div.btn_more>a");
                    $(block_id).find("div.product-item-container").last().after($blockAppend);

					if($("div.product-item-container").length>=record_count) {
						$("div.btn_more").remove();
					}

            		$("div.btn_more>a").attr('data-next-page', $more.attr('data-next-page'));
					$("div.btn_more>a").attr('data-max-page', $more.attr('data-max-page'));

                    /* dynamic script eval required!!!!!!!!! work pagination all site extra required code */
					dom.filter('script').each(function(){
                        if(this.text.search("new JCCatalogItem")>0 && this.text.search("bxAjaxidBitrix")==-1) {
							$.globalEval(this.text || this.textContent || this.innerHTML || '');
                        }
					});
                    return false;
                }
            });
            return false;
        });
    });
</script>
