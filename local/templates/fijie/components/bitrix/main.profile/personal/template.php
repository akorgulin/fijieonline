<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


?>

<? if ($_REQUEST["edit"]) { ?>
    <section class="content_wrapper">
        <div class="wrapper center">
            <h1 class="page_heading small" data-aos="flip-up" data-aos-delay="400">Личный кабинет</h1>
            <ul class="pagelist">
                <li data-aos="fade-down" data-aos-delay="350"><a href="/personal/private/"
                                                                 class="btn <? if ($APPLICATION->getCurPage() == "/personal/private/" && !$_REQUEST["edit"]) {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">Информация профиля</a></li>
                <li data-aos="fade-down" data-aos-delay="450"><a href="/personal/orders/"
                                                                 class="btn <? if ($APPLICATION->getCurPage() == "/personal/orders/") {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">История заказов<span class="num"><?=intval($_SESSION["num"])?></span></a>
                </li>
                <li data-aos="fade-down" data-aos-delay="550"><a href="/personal/private/?edit=y"
                                                                 class="btn <? if ($_REQUEST["edit"]) {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">Редактировать данные</a></li>
                <li data-aos="fade-down" data-aos-delay="650"><a href="?logout=yes"
                                                                 class="btn <? if ($_REQUEST["logout"]) {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">Выйти</a></li>
            </ul>
        </div>
        <div class="wrapper">
            <div class="grid clearfix">
                <div class="item">
                    <div class="border">
                        <div class="white_wrapper personal_area personal_area_edit" data-aos="fade-up"
                             data-aos-delay="250">
                            <h6 class="area_title">Редактировать персональные данные</h6>

                            <? ShowError($arResult["strProfileError"]); ?>
                            <?
                            if ($arResult['DATA_SAVED'] == 'Y')
                                ShowNote(GetMessage('PROFILE_DATA_SAVED'));
                            ?>
                            <? if ($_REQUEST["succeed"] == "y")
                                ShowNote(GetMessage('PROFILE_DATA_SAVED'));
                            ?>

                            <script type="text/javascript">
                                <!--
                                var opened_sections = [<?
                                    $arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"] . "_user_profile_open"];
                                    $arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
                                    if (strlen($arResult["opened"]) > 0) {
                                        echo "'" . implode("', '", explode(",", $arResult["opened"])) . "'";
                                    } else {
                                        $arResult["opened"] = "reg";
                                        echo "'reg'";
                                    }
                                    ?>];
                                //-->

                                var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
                            </script>


                            <form method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>?edit=y"
                                  enctype="multipart/form-data">
                                <?= $arResult["BX_SESSION_CHECK"] ?>
                                <input type="hidden" name="lang" value="<?= LANG ?>"/>
                                <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>


                                <h6 class="title_subline"><?= GetMessage("REG_SHOW_HIDE1") ?></h6>
                                <div class="forms-grid">

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input"><?= GetMessage('LAST_UPDATE') ?>
                                                : <?= $arResult["arUser"]["TIMESTAMP_X"] ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div align="left"><?= GetMessage('LAST_LOGIN') ?>:<?= $arResult["arUser"]["LAST_LOGIN"] ?></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text"
                                                       placeholder="<? echo GetMessage("PL_main_profile_title") ?>"
                                                       name="TITLE" value="<?= $arResult["arUser"]["TITLE"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_NAME') ?>"
                                                       name="NAME" maxlength="50"
                                                       value="<?= $arResult["arUser"]["NAME"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_LAST_NAME') ?>"
                                                       name="LAST_NAME" maxlength="50"
                                                       value="<?= $arResult["arUser"]["LAST_NAME"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_SECOND_NAME') ?>"
                                                       name="SECOND_NAME" maxlength="50"
                                                       value="<?= $arResult["arUser"]["SECOND_NAME"] ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 left">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-phone-handset"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_MOBILE') ?>"
                                                       name="PERSONAL_MOBILE" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_MOBILE"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-envelope"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_EMAIL') ?>"
                                                       name="EMAIL" maxlength="50"
                                                       value="<? echo $arResult["arUser"]["EMAIL"] ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_LOGIN') ?>"
                                                       name="LOGIN" maxlength="50"
                                                       value="<? echo $arResult["arUser"]["LOGIN"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <?  if ($arResult['CAN_EDIT_PASSWORD']) { ?>
                                        <div class="forms-col-6 left">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input type="password"
                                                           placeholder="<?= GetMessage('PL_NEW_PASSWORD') ?>"
                                                           name="NEW_PASSWORD" maxlength="50" value=""
                                                           autocomplete="off"/>
                                                    <? if ($arResult["SECURE_AUTH"]) { ?>
                                                        <span class="bx-auth-secure" id="bx_auth_secure"
                                                              title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>"
                                                              style="display:none">
                                                         <div class="bx-auth-secure-icon"></div>
                                                    </span>
                                                        <noscript>
                                                    <span class="bx-auth-secure"
                                                          title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                                    </span>
                                                        </noscript>
                                                        <script type="text/javascript">
                                                            document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                                        </script>
                                                    <? } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="forms-col-6 right">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input type="password"
                                                           placeholder="<?= GetMessage('PL_NEW_PASSWORD_CONFIRM') ?>"
                                                           name="NEW_PASSWORD_CONFIRM" maxlength="50" value=""
                                                           autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>
                                    <? } ?>

                                    <? if ($arResult["TIME_ZONE_ENABLED"] == true) { ?>
                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <select name="AUTO_TIME_ZONE"
                                                            onchange="this.form.TIME_ZONE.disabled=(this.value != 'N')">
                                                        <option value=""><? echo GetMessage("main_profile_time_zones_auto_def") ?></option>
                                                        <option value="Y"<?= ($arResult["arUser"]["AUTO_TIME_ZONE"] == "Y" ? ' SELECTED="SELECTED"' : '') ?>><? echo GetMessage("main_profile_time_zones_auto_yes") ?></option>
                                                        <option value="N"<?= ($arResult["arUser"]["AUTO_TIME_ZONE"] == "N" ? ' SELECTED="SELECTED"' : '') ?>><? echo GetMessage("main_profile_time_zones_auto_no") ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="forms-col-6 right hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <select name="TIME_ZONE"<? if ($arResult["arUser"]["AUTO_TIME_ZONE"] <> "N") echo ' disabled="disabled"' ?>>
                                                        <? foreach ($arResult["TIME_ZONE_LIST"] as $tz => $tz_name): ?>
                                                            <option value="<?= htmlspecialcharsbx($tz) ?>"<?= ($arResult["arUser"]["TIME_ZONE"] == $tz ? ' SELECTED="SELECTED"' : '') ?>><?= htmlspecialcharsbx($tz_name) ?></option>
                                                        <? endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <? } ?>
                                </div>
                                <div class="forms-grid">
                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_PROFESSION') ?>"
                                                       name="PERSONAL_PROFESSION" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_PROFESSION"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_WWW') ?>"
                                                       name="PERSONAL_WWW" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_WWW"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_ICQ') ?>"
                                                       name="PERSONAL_ICQ" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_ICQ"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <div align="left">
                                                    <select name="PERSONAL_GENDER">
                                                        <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                                        <option value="M"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                                        <option value="F"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="forms-grid">
                                    <div class="clearfix"></div>
                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <?
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:main.calendar',
                                                    '',
                                                    array(
                                                        'SHOW_INPUT' => 'Y',
                                                        'FORM_NAME' => 'form1',
                                                        'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                                        'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                                        'SHOW_TIME' => 'N'
                                                    ),
                                                    null,
                                                    array('HIDE_ICONS' => 'Y')
                                                );

                                                //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "form1", "15")
                                                ?>
                                                <script type="text/javascript">$("input#PERSONAL_BIRTHDAY").attr("placeholder", '<?=GetMessage('USER_BIRTHDAY_DT')?> (<?=$arResult["DATE_FORMAT"]?>):')</script>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-phone-handset"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_PHONE') ?>"
                                                       name="PERSONAL_PHONE" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-phone-handset"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_FAX') ?>"
                                                       name="PERSONAL_FAX" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_FAX"] ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-phone-handset"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_PAGER') ?>"
                                                       name="PERSONAL_PAGER" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_PAGER"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-phone-handset"></span></div>
                                                <div align="left"><?= $arResult["COUNTRY_SELECT_WORK"] ?></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_STATE') ?>"
                                                       name="PERSONAL_STATE" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_STATE"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_CITY') ?>"
                                                       name="PERSONAL_CITY" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_CITY"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_ZIP') ?>"
                                                       name="PERSONAL_ZIP" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_ZIP"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <textarea cols="30" rows="5"
                                                          placeholder="<?= GetMessage('PL_USER_STREET') ?>"
                                                          name="PERSONAL_STREET"><?= $arResult["arUser"]["PERSONAL_STREET"] ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_MAILBOX') ?>"
                                                       name="PERSONAL_MAILBOX" maxlength="255"
                                                       value="<?= $arResult["arUser"]["PERSONAL_MAILBOX"] ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <textarea cols="30" rows="5"
                                                          placeholder="<?= GetMessage('PL_USER_NOTES') ?>"
                                                          name="PERSONAL_NOTES"><?= $arResult["arUser"]["PERSONAL_NOTES"] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h6 class="title_subline hidden-xs"><?= GetMessage("USER_WORK_INFO") ?></h6>
                                <div class="forms-grid">

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_COMPANY') ?>"
                                                       name="WORK_COMPANY" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_COMPANY"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_WWW') ?>"
                                                       name="WORK_WWW" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_WWW"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_DEPARTMENT') ?>"
                                                       name="WORK_DEPARTMENT" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_DEPARTMENT"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_POSITION') ?>"
                                                       name="WORK_POSITION" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_POSITION"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <textarea cols="30"
                                                          placeholder="<?= GetMessage('PL_USER_WORK_PROFILE') ?>"
                                                          rows="5"
                                                          name="WORK_PROFILE"><?= $arResult["arUser"]["WORK_PROFILE"] ?></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_PHONE') ?>"
                                                       name="WORK_PHONE" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_PHONE"] ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <?
                                    if (strlen($arResult["arUser"]["WORK_LOGO"]) == 546456450) {
                                        ?>
                                        <?= $arResult["arUser"]["WORK_LOGO"] ?>
                                        <?
                                    } else if (strlen($arResult["arUser"]["WORK_LOGO"]) == 546456450) {
                                        ?>
                                        <div class="hidden-xs">
                                            <div class="clearfix"></div>
                                            <h6 class="title_subline"><?= GetMessage("PL_USER_LOGO") ?></h6>
                                            <div class="clearfix"></div>
                                            <div class="drop">
                                                <div class="cont">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" width="76px"
                                                         height="70px">
                                                        <path fill-rule="evenodd" fill="rgb(234, 160, 162)"
                                                              d="M70.905,50.020 C67.592,53.664 63.138,55.844 58.371,56.187 C58.309,56.187 58.262,56.187 58.215,56.187 L46.619,56.187 C45.447,56.187 44.509,55.253 44.509,54.085 C44.509,52.917 45.447,51.982 46.619,51.982 L58.137,51.982 C65.670,51.406 71.780,44.678 71.780,36.939 C71.780,29.635 66.529,23.235 59.575,22.051 C58.637,21.895 57.918,21.132 57.824,20.182 C56.949,11.057 49.338,4.173 40.118,4.173 C33.257,4.173 26.943,8.160 24.020,14.342 C23.552,15.324 22.411,15.791 21.379,15.417 C20.535,15.106 19.629,14.950 18.707,14.950 C14.393,14.950 10.877,18.454 10.877,22.752 C10.877,23.702 11.033,24.589 11.346,25.430 C11.690,26.396 11.315,27.470 10.424,27.984 C6.595,30.211 4.220,34.323 4.220,38.730 C4.220,45.551 9.596,51.608 15.988,51.998 L29.365,51.998 C30.537,51.998 31.475,52.932 31.475,54.100 C31.475,55.268 30.537,56.202 29.365,56.202 L15.925,56.202 C15.878,56.202 15.847,56.202 15.800,56.202 C11.549,55.969 7.564,53.960 4.548,50.565 C1.610,47.248 -0.000,43.059 -0.000,38.745 C-0.000,33.404 2.610,28.389 6.908,25.275 C6.751,24.465 6.658,23.639 6.658,22.783 C6.658,16.165 12.065,10.776 18.707,10.776 C19.488,10.776 20.270,10.854 21.020,10.994 C22.755,7.973 25.208,5.404 28.162,3.504 C31.710,1.215 35.851,-0.000 40.102,-0.000 C45.603,-0.000 50.885,2.040 54.949,5.746 C58.606,9.079 60.997,13.470 61.809,18.283 C69.999,20.432 75.984,28.202 76.000,36.939 C76.000,41.766 74.203,46.407 70.905,50.020 ZM26.834,41.361 C26.302,41.361 25.755,41.143 25.349,40.739 C24.521,39.913 24.521,38.590 25.349,37.764 L36.523,26.630 C36.914,26.225 37.461,26.007 38.008,26.007 C38.555,26.007 39.102,26.240 39.492,26.630 L50.667,37.764 C51.495,38.590 51.495,39.913 50.667,40.739 C49.838,41.564 48.510,41.564 47.697,40.739 L40.118,33.186 L40.118,67.898 C40.118,69.066 39.180,70.000 38.008,70.000 C36.836,70.000 35.898,69.066 35.898,67.898 L35.898,33.186 L28.318,40.739 C27.912,41.159 27.365,41.361 26.834,41.361 Z"/>
                                                    </svg>
                                                    <div class="desc">нажмите сюда и выберите файлы</div>
                                                    <div class="browse">выбрать файл</div>
                                                </div>
                                                <output id="list"></output>
                                                <input id="files" multiple="true" name="WORK_LOGO" type="file"/>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                        </div>
                                    <? } ?>

                                    <div class="clearfix"></div>
                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_FAX') ?>"
                                                       name="WORK_FAX" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_FAX"] ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_PAGER') ?>"
                                                       name="WORK_PAGER" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_PAGER"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_STATE') ?>"
                                                       name="WORK_STATE" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_STATE"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_CITY') ?>"
                                                       name="WORK_CITY" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_CITY"] ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_ZIP') ?>"
                                                       name="WORK_ZIP" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_ZIP"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <textarea cols="30" placeholder="<?= GetMessage('PL_USER_STREET') ?>"
                                                          rows="5"
                                                          name="WORK_STREET"><?= $arResult["arUser"]["WORK_STREET"] ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 left hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                                <input type="text" placeholder="<?= GetMessage('PL_USER_MAILBOX') ?>"
                                                       name="WORK_MAILBOX" maxlength="255"
                                                       value="<?= $arResult["arUser"]["WORK_MAILBOX"] ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="forms-col-6 right hidden-xs">
                                        <div class="border-form">
                                            <div class="without_icon_input">
                                                <textarea cols="30" placeholder="<?= GetMessage('PL_USER_NOTES') ?>"
                                                          rows="5"
                                                          name="WORK_NOTES"><?= $arResult["arUser"]["WORK_NOTES"] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?
                                if ($arResult["INCLUDE_FORUM"] == "Y") {
                                    ?>
                                    <h6 class="title_subline hidden-xs"><?= GetMessage("forum_INFO") ?></h6>
                                    <div class="forms-grid">
                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input type="hidden"
                                                           placeholder="<?= GetMessage('PL_forum_SHOW_NAME') ?>"
                                                           name="forum_SHOW_NAME" value="N"/><input type="checkbox"
                                                                                                    name="forum_SHOW_NAME"
                                                                                                    value="Y" <?
                                                    if ($arResult["arForumUser"]["SHOW_NAME"] == "Y") echo "checked=\"checked\""; ?> />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 right hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input type="text"
                                                           placeholder="<?= GetMessage('PL_forum_DESCRIPTION') ?>"
                                                           name="forum_DESCRIPTION" maxlength="255"
                                                           value="<?= $arResult["arForumUser"]["DESCRIPTION"] ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <textarea cols="30"
                                                              placeholder="<?= GetMessage('PL_forum_INTERESTS') ?>"
                                                              rows="5"
                                                              name="forum_INTERESTS"><?= $arResult["arForumUser"]["INTERESTS"]; ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 right hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <textarea cols="30" rows="5"
                                                              placeholder="<?= GetMessage('PL_forum_SIGNATURE') ?>"
                                                              name="forum_SIGNATURE"><?= $arResult["arForumUser"]["SIGNATURE"]; ?></textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <?
                                        if (strlen($arResult["arForumUser"]["AVATAR"]) == 546456450) {
                                            ?>
                                            <?= $arResult["arForumUser"]["AVATAR_HTML"] ?>
                                            <?
                                        } else if (strlen($arResult["arForumUser"]["AVATAR"]) == 546456450) {
                                            ?>
                                            <div class="hidden-xs">
                                                <div class="clearfix"></div>
                                                <h6 class="title_subline"><?= GetMessage("PL_forum_AVATAR") ?></h6>
                                                <div class="clearfix"></div>
                                                <div class="drop">
                                                    <div class="cont">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="76px"
                                                             height="70px">
                                                            <path fill-rule="evenodd" fill="rgb(234, 160, 162)"
                                                                  d="M70.905,50.020 C67.592,53.664 63.138,55.844 58.371,56.187 C58.309,56.187 58.262,56.187 58.215,56.187 L46.619,56.187 C45.447,56.187 44.509,55.253 44.509,54.085 C44.509,52.917 45.447,51.982 46.619,51.982 L58.137,51.982 C65.670,51.406 71.780,44.678 71.780,36.939 C71.780,29.635 66.529,23.235 59.575,22.051 C58.637,21.895 57.918,21.132 57.824,20.182 C56.949,11.057 49.338,4.173 40.118,4.173 C33.257,4.173 26.943,8.160 24.020,14.342 C23.552,15.324 22.411,15.791 21.379,15.417 C20.535,15.106 19.629,14.950 18.707,14.950 C14.393,14.950 10.877,18.454 10.877,22.752 C10.877,23.702 11.033,24.589 11.346,25.430 C11.690,26.396 11.315,27.470 10.424,27.984 C6.595,30.211 4.220,34.323 4.220,38.730 C4.220,45.551 9.596,51.608 15.988,51.998 L29.365,51.998 C30.537,51.998 31.475,52.932 31.475,54.100 C31.475,55.268 30.537,56.202 29.365,56.202 L15.925,56.202 C15.878,56.202 15.847,56.202 15.800,56.202 C11.549,55.969 7.564,53.960 4.548,50.565 C1.610,47.248 -0.000,43.059 -0.000,38.745 C-0.000,33.404 2.610,28.389 6.908,25.275 C6.751,24.465 6.658,23.639 6.658,22.783 C6.658,16.165 12.065,10.776 18.707,10.776 C19.488,10.776 20.270,10.854 21.020,10.994 C22.755,7.973 25.208,5.404 28.162,3.504 C31.710,1.215 35.851,-0.000 40.102,-0.000 C45.603,-0.000 50.885,2.040 54.949,5.746 C58.606,9.079 60.997,13.470 61.809,18.283 C69.999,20.432 75.984,28.202 76.000,36.939 C76.000,41.766 74.203,46.407 70.905,50.020 ZM26.834,41.361 C26.302,41.361 25.755,41.143 25.349,40.739 C24.521,39.913 24.521,38.590 25.349,37.764 L36.523,26.630 C36.914,26.225 37.461,26.007 38.008,26.007 C38.555,26.007 39.102,26.240 39.492,26.630 L50.667,37.764 C51.495,38.590 51.495,39.913 50.667,40.739 C49.838,41.564 48.510,41.564 47.697,40.739 L40.118,33.186 L40.118,67.898 C40.118,69.066 39.180,70.000 38.008,70.000 C36.836,70.000 35.898,69.066 35.898,67.898 L35.898,33.186 L28.318,40.739 C27.912,41.159 27.365,41.361 26.834,41.361 Z"/>
                                                        </svg>
                                                        <div class="desc">нажмите сюда и выберите файлы</div>
                                                        <div class="browse">выбрать файл</div>
                                                    </div>
                                                    <output id="list"></output>
                                                    <input id="files" multiple="true" name="forum_AVATAR" type="file"/>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                            </div>
                                        <? } ?>

                                    </div>
                                    <?
                                }
                                ?>
                                <?
                                if ($arResult["INCLUDE_BLOG"] == "Y") {
                                    ?>
                                    <h6 class="title_subline hidden-xs"><?= GetMessage("blog_INFO") ?></h6>
                                    <div class="forms-grid">
                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input class="typeinput"
                                                           placeholder="<?= GetMessage('PL_blog_ALIAS') ?>" type="text"
                                                           name="blog_ALIAS" maxlength="255"
                                                           value="<?= $arResult["arBlogUser"]["ALIAS"] ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 right hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input class="typeinput"
                                                           placeholder="<?= GetMessage('PL_blog_DESCRIPTION') ?>"
                                                           type="text" name="blog_DESCRIPTION" maxlength="255"
                                                           value="<?= $arResult["arBlogUser"]["DESCRIPTION"] ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <textarea placeholder="<?= GetMessage('PL_blog_INTERESTS') ?>"
                                                              cols="30" rows="5" class="typearea" name="blog_INTERESTS"><?
                                                        echo $arResult["arBlogUser"]["INTERESTS"]; ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <?
                                        if (strlen($arResult["arBlogUser"]["AVATAR"]) == 546456450) {
                                            ?>
                                            <?= $arResult["arBlogUser"]["AVATAR_HTML"] ?>
                                            <?
                                        } else if (strlen($arResult["arBlogUser"]["AVATAR"]) == 546456450) {
                                            ?>
                                            <div class="hidden-xs">
                                                <div class="clearfix"></div>
                                                <h6 class="title_subline"><?= GetMessage("PL_blog_AVATAR") ?></h6>
                                                <div class="clearfix"></div>
                                                <div class="drop">
                                                    <div class="cont">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="76px"
                                                             height="70px">
                                                            <path fill-rule="evenodd" fill="rgb(234, 160, 162)"
                                                                  d="M70.905,50.020 C67.592,53.664 63.138,55.844 58.371,56.187 C58.309,56.187 58.262,56.187 58.215,56.187 L46.619,56.187 C45.447,56.187 44.509,55.253 44.509,54.085 C44.509,52.917 45.447,51.982 46.619,51.982 L58.137,51.982 C65.670,51.406 71.780,44.678 71.780,36.939 C71.780,29.635 66.529,23.235 59.575,22.051 C58.637,21.895 57.918,21.132 57.824,20.182 C56.949,11.057 49.338,4.173 40.118,4.173 C33.257,4.173 26.943,8.160 24.020,14.342 C23.552,15.324 22.411,15.791 21.379,15.417 C20.535,15.106 19.629,14.950 18.707,14.950 C14.393,14.950 10.877,18.454 10.877,22.752 C10.877,23.702 11.033,24.589 11.346,25.430 C11.690,26.396 11.315,27.470 10.424,27.984 C6.595,30.211 4.220,34.323 4.220,38.730 C4.220,45.551 9.596,51.608 15.988,51.998 L29.365,51.998 C30.537,51.998 31.475,52.932 31.475,54.100 C31.475,55.268 30.537,56.202 29.365,56.202 L15.925,56.202 C15.878,56.202 15.847,56.202 15.800,56.202 C11.549,55.969 7.564,53.960 4.548,50.565 C1.610,47.248 -0.000,43.059 -0.000,38.745 C-0.000,33.404 2.610,28.389 6.908,25.275 C6.751,24.465 6.658,23.639 6.658,22.783 C6.658,16.165 12.065,10.776 18.707,10.776 C19.488,10.776 20.270,10.854 21.020,10.994 C22.755,7.973 25.208,5.404 28.162,3.504 C31.710,1.215 35.851,-0.000 40.102,-0.000 C45.603,-0.000 50.885,2.040 54.949,5.746 C58.606,9.079 60.997,13.470 61.809,18.283 C69.999,20.432 75.984,28.202 76.000,36.939 C76.000,41.766 74.203,46.407 70.905,50.020 ZM26.834,41.361 C26.302,41.361 25.755,41.143 25.349,40.739 C24.521,39.913 24.521,38.590 25.349,37.764 L36.523,26.630 C36.914,26.225 37.461,26.007 38.008,26.007 C38.555,26.007 39.102,26.240 39.492,26.630 L50.667,37.764 C51.495,38.590 51.495,39.913 50.667,40.739 C49.838,41.564 48.510,41.564 47.697,40.739 L40.118,33.186 L40.118,67.898 C40.118,69.066 39.180,70.000 38.008,70.000 C36.836,70.000 35.898,69.066 35.898,67.898 L35.898,33.186 L28.318,40.739 C27.912,41.159 27.365,41.361 26.834,41.361 Z"/>
                                                        </svg>
                                                        <div class="desc">нажмите сюда и выберите файлы</div>
                                                        <div class="browse">выбрать файл</div>
                                                    </div>
                                                    <output id="list"></output>
                                                    <input id="files" multiple="true" name="blog_AVATAR" type="file"/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <?
                                }
                                ?>
                                <? if ($arResult["INCLUDE_LEARNING"] == "Y") { ?>
                                    <h6 class="title_subline hidden-xs"><?= GetMessage("learning_INFO") ?></h6></a>
                                    <div class="forms-grid">
                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <input type="hidden" name="student_PUBLIC_PROFILE" value="N"/>
                                                    <input type="checkbox" name="student_PUBLIC_PROFILE"
                                                           value="Y" <? if ($arResult["arStudent"]["PUBLIC_PROFILE"] == "Y") echo "checked=\"checked\""; ?> />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 right hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <textarea placeholder="<?= GetMessage('student_RESUME') ?>"
                                                              cols="30" rows="5"
                                                              name="student_RESUME"><?= $arResult["arStudent"]["RESUME"]; ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                                    <?= $arResult["arStudent"]["TRANSCRIPT"]; ?>-<?= $arResult["ID"] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? } ?>


                                <? if ($arResult["IS_ADMIN"]) { ?>
                                    <h6 class="title_subline hidden-xs"><?= GetMessage("USER_ADMIN_NOTES") ?></h6>
                                    <div class="forms-grid">
                                        <div class="forms-col-6 left hidden-xs">
                                            <div class="border-form">
                                                <div class="without_icon_input">
                                                    <textarea placeholder="<?= GetMessage('USER_ADMIN_NOTES') ?>"
                                                              cols="30" rows="5"
                                                              name="ADMIN_NOTES"><?= $arResult["arUser"]["ADMIN_NOTES"] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? } ?>


                                <h6 class="title_subline personal_prfile_block"><?= GetMessage("DELIVERY_TYPE_EDIT_TAB") ?></h6>
                                <div class="forms-grid personal_prfile_block">
                                    <? $APPLICATION->IncludeComponent("bitrix:sale.personal.profile.detail", "", Array(
                                        "COMPATIBLE_LOCATION_MODE" => "N",
                                        "PATH_TO_LIST" => "/personal/private/?edit=y&succeed=y",
                                        "PATH_TO_DETAIL" => "profile_detail.php?ID=#ID#",
                                        "ID" => $arResult["BAYER_ID"],
                                        "USE_AJAX_LOCATIONS" => "N",
                                        "SET_TITLE" => "N"
                                    )); ?>
                                </div>

                                <? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y") { ?>
                                    <h6 class="title_subline"><?= GetMessage("CERTIFICATE_TYPE_EDIT_TAB") ?></h6>
                                    <? $i = 0; ?>
                                    <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) { ?>
                                        <? $res = $i % 2; ?>
                                        <? $class = (($res) ? "left" : "right"); ?>
                                        <? $APPLICATION->IncludeComponent(
                                            "fijie:system.field.edit",
                                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS" => "Y")); ?>
                                        <? $i++; ?>
                                    <? }
                                } ?>

                                <div class="buttons">
                                    <p class="hidden-xs"><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
                                    <input type="submit" name="save"
                                           value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>"
                                           style="display: none;">
                                    <input type="reset" name="reset" value="<?= GetMessage('MAIN_RESET'); ?>"
                                           style="display: none;">
                                    <input type="hidden" name="save"
                                           value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>">
                                    <a href="#save" onclick="javascript:$('form[name=form1]').submit();"
                                       class="btn mint">
                                        &nbsp;<?= GetMessage("PL_SAVE") ?>
                                    </a>
                                    <a href="#reset"
                                       onclick="javascript:$('form[name=form1] input[type=reset]').click();"
                                       class="btn mint hidden-xs">
                                        <?= GetMessage("PL_CLEAR") ?>
                                    </a>
                                </div>
                                <?
                                /*if($arResult["SOCSERV_ENABLED"])
                            {
                                $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                                    "SHOW_PROFILES" => "Y",
                                    "ALLOW_DELETE" => "Y"
                                ),
                                    false
                                );
                            }*/
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
<? } else { ?>

    <?
    ob_start();

    \Bitrix\Main\Loader::includeModule('sale');
    $discount = [];
    $UserDiscount = [];
    CModule::IncludeModule('sale');
    $db_res = CSaleDiscount::GetList(
        array("SORT" => "ASC"),
        array(
            "LID" => SITE_ID,
            "ACTIVE" => "Y",
            "USER_GROUPS" => $USER->GetUserGroupArray(),
        ),
        false,
        false,
        array()
    );
    while ($ar_res = $db_res->Fetch()) {
        $resDisc = CSaleDiscount::GetByID($ar_res["ID"]);
        $conditions = unserialize($resDisc["CONDITIONS"]);
        $actions = unserialize($resDisc["ACTIONS"]);
        //print_r($resDisc["NAME"]);
        //print_r($ar_res["DISCOUNT_TYPE"]);
        //print_r($resDisc["UNPACK"]); echo "<br><br>";
        if ($ar_res["DISCOUNT_TYPE"] == "P" && strstr($resDisc["UNPACK"], "\CSaleCondCumulativeCtrl::getCumulativeValue")) {

            $appl = str_replace('function (&$arOrder){\Bitrix\Sale\Discount\Actions::applyToBasket($arOrder, array (', 'array(', $resDisc["APPLICATION"]);
            $appl = str_replace('), "");};', ');', $appl);

            if (!strstr($appl, "\Bitrix\Sale\Discount\Actions::applyToBasket") && !strstr($appl, "\Bitrix\Sale\Discount\Actions::applySimpleGift")) {
                eval('$mass = ' . $appl);

                $discount[intval(str_replace("-", "", $mass["VALUE"]))] = array(
                    "percent" => intval(str_replace("-", "", $mass["VALUE"])) . "%",
                    "price" => $conditions["CHILDREN"][0]["DATA"]["Value"],
                );
            }
        }


        if(strstr($resDisc["UNPACK"], "\Main\Discount\UserConditionControl::checkBasket")) {
            $appl = str_replace('function (&$arOrder){\Bitrix\Sale\Discount\Actions::applyToBasket($arOrder, array (', 'array(', $resDisc["APPLICATION"]);
            $appl = str_replace('), "");};', ');', $appl);

            if (!strstr($appl, "\Bitrix\Sale\Discount\Actions::applyToBasket") && !strstr($appl, "\Bitrix\Sale\Discount\Actions::applySimpleGift")) {
                eval('$mass = ' . $appl);

                $UserDiscount[] = array(
                    "percent" => intval(str_replace("-", "", $mass["VALUE"])) . "%",
                    "price" => $conditions["CHILDREN"][0]["DATA"]["Value"],
                    "user" => $conditions["CHILDREN"][0]["DATA"]["value"][0],
                );
            }
        }
    }

    $discount = array_values($discount);

    //PF - Статус завершён
    $getListParams['filter'] = array('USER_ID' => $USER->getID(),'STATUS_ID'=>'PF');

    $getListParams['select'] = array('*');

    $getListParams['runtime'] = array(
        new \Bitrix\Main\Entity\ReferenceField(
            'PROPS',
            '\Bitrix\Sale\Internals\OrderPropsValueTable',
            array(
                '=this.ID' => 'ref.ORDER_ID',
            ),
            array(
                "join_type" => 'inner'
            )
        )
    );
    $getListParams['order'] = array();
    $koef = 0;
    $res = \Bitrix\Sale\Order::getList($getListParams);
    $totalPriceRow = [];
    while ($row = $res->fetch()) {
        $totalPriceRow[$row["ID"]] = intval($row["PRICE"]);
    }


    $totalPrice = 0;
    foreach ($totalPriceRow as $numrows => $price) {
        $totalPrice += intval($price);
        foreach ($discount as $numrow => $discRow) {
            if ($totalPrice > $discRow["price"]) {
                $koef = $numrow;
            }
        }
    }
    if(empty($totalPriceRow)) {
        $discount = [];
    }

    if($koef>0) {
        $discount = array_splice($discount, $koef - 1, 3);
    } else {
        $discount = array_splice($discount, $koef, 3);
    }

    if ($koef > count($discount)) {
        $koef = count($discount) - 2;
    }

    $resultPrice = $totalPrice - $discount[$koef + 1]["price"];
    $begin = 0;
    if(empty($totalPriceRow)) {
        $begin = 1;
        $discount[$koef]["price"] = 20000;
        $discount[$koef]["percent"] = "5%";
        $resultPrice = $discount[$koef]["price"];
    }
    if(!array_key_exists($koef,$discount)){
        $koef -=1;
    }

    if(count($UserDiscount)>0 && $UserDiscount[0]["user"]==$USER->getID()) {
        ?>
        <div class="item" data-aos="fade-left" data-aos-delay="250">
            <div class="border">
                <div class="discount">
                    <h6 class="area_title">Текущая скидка:
                        <?= $UserDiscount[0]["percent"] ?>
                    </h6>
                    <div class="discount_lines">
                        <? foreach ($UserDiscount as $iRow => $priceLine) { ?>
                            <? $class = "x3"; ?>
                            <div class="line">
                                <div class="scroll_area <?= $class ?>"></div>
                                <div class="descr"><?= $priceLine["percent"] ?> скидка</div>
                            </div>
                        <?  break; } ?>
                    </div>
                </div>
            </div>
        </div>
        <?
    } else {
        ?>
        <div class="item" data-aos="fade-left" data-aos-delay="250">
            <div class="border">
                <div class="discount">
                    <h6 class="area_title">Текущая скидка:
                        <?if($begin==1) { ?>
                            0%
                        <? } else { ?>
                            <?= $discount[$koef]["percent"] ?>
                        <? } ?>
                    </h6>
                    <p><strong>До скидки - <?= $discount[$koef + 1]["percent"] ?> осталось купить товаров на
                            сумму <?= CurrencyFormat($resultPrice, "RUB") ?></strong></p>
                    <div class="discount_lines">
                        <? foreach ($discount as $iRow => $priceLine) { ?>
                            <? if ($iRow == 1) $class = "x2"; ?>
                            <? if ($iRow == 2) $class = "x3"; ?>
                            <div class="line">
                                <div class="scroll_area <?= $class ?>"></div>
                                <div class="descr">От <?= CurrencyFormat($priceLine["price"], "RUB") ?>
                                    – <?= $priceLine["percent"] ?> скидка
                                </div>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        <?
    }
    $output = ob_get_contents();
    ob_end_clean();
    ?>
    <section class="content_wrapper">
        <div class="wrapper center">
            <h1 class="page_heading small" data-aos="flip-up" data-aos-delay="400">Личный кабинет</h1>
            <ul class="pagelist">
                <li data-aos="fade-down" data-aos-delay="350"><a href="/personal/private/"
                                                                 class="btn <? if ($APPLICATION->getCurPage() == "/personal/private/" && !$_REQUEST["edit"]) {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">Информация профиля</a></li>
                <li data-aos="fade-down" data-aos-delay="450"><a href="/personal/orders/"
                                                                 class="btn <? if ($APPLICATION->getCurPage() == "/personal/orders/") {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">История заказов<span class="num"><?=intval($_SESSION["num"])?></span></a>
                </li>
                <li data-aos="fade-down" data-aos-delay="550"><a href="/personal/private/?edit=y"
                                                                 class="btn <? if ($_REQUEST["edit"]) {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">Редактировать данные</a></li>
                <li data-aos="fade-down" data-aos-delay="650"><a href="?logout=yes"
                                                                 class="btn <? if ($_REQUEST["logout"]) {
                                                                     echo 'mint';
                                                                 } else {
                                                                     echo 'grey';
                                                                 } ?>">Выйти</a></li>
            </ul>
        </div>
        <div class="wrapper">
            <div class="grid x3 clearfix">
                <div class="item x2">
                    <div class="border">
                        <div class="white_wrapper personal_area" data-aos="fade-up" data-aos-delay="250">
                            <div class="bx-auth-profile">

                                <? ShowError($arResult["strProfileError"]); ?>
                                <?
                                if ($arResult['DATA_SAVED'] == 'Y')
                                    ShowNote(GetMessage('PROFILE_DATA_SAVED'));
                                ?>
                                <script type="text/javascript">
                                    <!--
                                    var opened_sections = [<?
                                        $arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"] . "_user_profile_open"];
                                        $arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
                                        if (strlen($arResult["opened"]) > 0) {
                                            echo "'" . implode("', '", explode(",", $arResult["opened"])) . "'";
                                        } else {
                                            $arResult["opened"] = "reg";
                                            echo "'reg'";
                                        }
                                        ?>];
                                    //-->

                                    var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';


                                    $(document).ready(function () {
                                        $("ul.params").each(function () {
                                            if ($(this).find("li").length == 0) {
                                                $(this).parent().prev().hide();
                                            }
                                        });
                                    });
                                </script>


                                <form method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
                                      enctype="multipart/form-data">
                                    <?= $arResult["BX_SESSION_CHECK"] ?>
                                    <input type="hidden" name="lang" value="<?= LANG ?>"/>
                                    <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
                                    <h6 class="area_title">Информация профиля</h6>

                                    <div class="profile-link profile-user-div-link"><a title="<?= GetMessage("REG_SHOW_HIDE") ?>" href="javascript:void(0)"><h6 class="title_subline"><?= GetMessage("REG_SHOW_HIDE") ?></h6></a></div>
                                    <div class="profile-block-shown <? /*profile-block-*/
                                    (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>"
                                         id="user_div_reg">
                                        <? if (!$_REQUEST["edit"]) { ?>
                                            <ul class="params">
                                                <?
                                                if ($arResult["ID"] > 0) {
                                                    ?>
                                                    <?
                                                    if (strlen($arResult["arUser"]["TIMESTAMP_X"]) > 0) {
                                                        ?>
                                                        <li><?= GetMessage('LAST_UPDATE') ?>: <?= $arResult["arUser"]["TIMESTAMP_X"] ?></li>
                                                        <?
                                                    }
                                                    ?>
                                                    <?
                                                    if (strlen($arResult["arUser"]["LAST_LOGIN"]) > 0) {
                                                        ?>
                                                        <li><?= GetMessage('LAST_LOGIN') ?>: <?= $arResult["arUser"]["LAST_LOGIN"] ?></li>
                                                        <?
                                                    }
                                                    ?>
                                                    <?
                                                }
                                                ?>
                                                <? if ($arResult["arUser"]["TITLE"]) { ?>
                                                    <li><? echo GetMessage("main_profile_title") ?>:<?= $arResult["arUser"]["TITLE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["LAST_NAME"]) {   ?>
                                                    <li><?= GetMessage('LAST_NAME') ?>: <?= $arResult["arUser"]["NAME"] ?> <?= $arResult["arUser"]["LAST_NAME"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["SECOND_NAME"]) { ?>
                                                    <li><?= GetMessage('SECOND_NAME') ?>: <?= $arResult["arUser"]["SECOND_NAME"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["EMAIL"]) { ?>
                                                    <li><?= GetMessage('EMAIL') ?>: <?= $arResult["arUser"]["EMAIL"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["LOGIN"]) { ?>
                                                    <li><?= GetMessage('LOGIN') ?>: <?= $arResult["arUser"]["LOGIN"] ?></li>
                                                <? } ?>

                                                <? if ($arResult["TIME_ZONE_ENABLED"] == true): ?>
                                                    <li>
                                                        <? echo GetMessage("main_profile_time_zones_auto") ?>:
                                                        <select name="AUTO_TIME_ZONE"
                                                                onchange="this.form.TIME_ZONE.disabled=(this.value != 'N')">
                                                            <option value=""><? echo GetMessage("main_profile_time_zones_auto_def") ?></option>
                                                            <option value="Y"<?= ($arResult["arUser"]["AUTO_TIME_ZONE"] == "Y" ? ' SELECTED="SELECTED"' : '') ?>><? echo GetMessage("main_profile_time_zones_auto_yes") ?></option>
                                                            <option value="N"<?= ($arResult["arUser"]["AUTO_TIME_ZONE"] == "N" ? ' SELECTED="SELECTED"' : '') ?>><? echo GetMessage("main_profile_time_zones_auto_no") ?></option>
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <? echo GetMessage("main_profile_time_zones_zones") ?>:
                                                        <select name="TIME_ZONE"<? if ($arResult["arUser"]["AUTO_TIME_ZONE"] <> "N") echo ' disabled="disabled"' ?>>
                                                            <? foreach ($arResult["TIME_ZONE_LIST"] as $tz => $tz_name): ?>
                                                                <option value="<?= htmlspecialcharsbx($tz) ?>"<?= ($arResult["arUser"]["TIME_ZONE"] == $tz ? ' SELECTED="SELECTED"' : '') ?>><?= htmlspecialcharsbx($tz_name) ?></option>
                                                            <? endforeach ?>
                                                        </select>
                                                    </li>
                                                <? endif ?>
                                            </ul>
                                        <? } else { ?>
                                            <table class="profile-table data-table">
                                                <tbody>
                                                <?
                                                if ($arResult["ID"] > 0) {
                                                    ?>
                                                    <?
                                                    if (strlen($arResult["arUser"]["TIMESTAMP_X"]) > 0) {
                                                        ?>
                                                        <tr>
                                                            <td><?= GetMessage('LAST_UPDATE') ?></td>
                                                            <td><?= $arResult["arUser"]["TIMESTAMP_X"] ?></td>
                                                        </tr>
                                                        <?
                                                    }
                                                    ?>
                                                    <?
                                                    if (strlen($arResult["arUser"]["LAST_LOGIN"]) > 0) {
                                                        ?>
                                                        <tr>
                                                            <td><?= GetMessage('LAST_LOGIN') ?></td>
                                                            <td><?= $arResult["arUser"]["LAST_LOGIN"] ?></td>
                                                        </tr>
                                                        <?
                                                    }
                                                    ?>
                                                    <?
                                                }
                                                ?>
                                                <tr>
                                                    <td><? echo GetMessage("main_profile_title") ?></td>
                                                    <td><input type="text" name="TITLE"
                                                               value="<?= $arResult["arUser"]["TITLE"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('NAME') ?></td>
                                                    <td><input type="text" name="NAME" maxlength="50"
                                                               value="<?= $arResult["arUser"]["NAME"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('LAST_NAME') ?></td>
                                                    <td><input type="text" name="LAST_NAME" maxlength="50"
                                                               value="<?= $arResult["arUser"]["LAST_NAME"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('SECOND_NAME') ?></td>
                                                    <td><input type="text" name="SECOND_NAME" maxlength="50"
                                                               value="<?= $arResult["arUser"]["SECOND_NAME"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('EMAIL') ?><? if ($arResult["EMAIL_REQUIRED"]): ?>
                                                            <span class="starrequired">*</span><? endif ?></td>
                                                    <td><input type="text" name="EMAIL" maxlength="50"
                                                               value="<? echo $arResult["arUser"]["EMAIL"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('LOGIN') ?><span class="starrequired">*</span>
                                                    </td>
                                                    <td><input type="text" name="LOGIN" maxlength="50"
                                                               value="<? echo $arResult["arUser"]["LOGIN"] ?>"/></td>
                                                </tr>
                                                <? if ($arResult['CAN_EDIT_PASSWORD']): ?>
                                                    <tr>
                                                    <td><?= GetMessage('NEW_PASSWORD_REQ') ?></td>
                                                    <td><input type="password" name="NEW_PASSWORD" maxlength="50"
                                                               value="" autocomplete="off" class="bx-auth-input"/>
                                                    <? if ($arResult["SECURE_AUTH"]): ?>
                                                        <span class="bx-auth-secure" id="bx_auth_secure"
                                                              title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>"
                                                              style="display:none">
                                            <div class="bx-auth-secure-icon"></div>
                                        </span>
                                                        <noscript>
                                        <span class="bx-auth-secure"
                                              title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                                            <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                        </span>
                                                        </noscript>
                                                        <script type="text/javascript">
                                                            document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                                        </script>
                                                        </td>
                                                        </tr>
                                                    <? endif ?>
                                                    <tr>
                                                        <td><?= GetMessage('NEW_PASSWORD_CONFIRM') ?></td>
                                                        <td><input type="password" name="NEW_PASSWORD_CONFIRM"
                                                                   maxlength="50" value="" autocomplete="off"/></td>
                                                    </tr>
                                                <? endif ?>
                                                <? if ($arResult["TIME_ZONE_ENABLED"] == true): ?>
                                                    <tr>
                                                        <td colspan="2"
                                                            class="profile-header"><? echo GetMessage("main_profile_time_zones") ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><? echo GetMessage("main_profile_time_zones_auto") ?></td>
                                                        <td>
                                                            <select name="AUTO_TIME_ZONE"
                                                                    onchange="this.form.TIME_ZONE.disabled=(this.value != 'N')">
                                                                <option value=""><? echo GetMessage("main_profile_time_zones_auto_def") ?></option>
                                                                <option value="Y"<?= ($arResult["arUser"]["AUTO_TIME_ZONE"] == "Y" ? ' SELECTED="SELECTED"' : '') ?>><? echo GetMessage("main_profile_time_zones_auto_yes") ?></option>
                                                                <option value="N"<?= ($arResult["arUser"]["AUTO_TIME_ZONE"] == "N" ? ' SELECTED="SELECTED"' : '') ?>><? echo GetMessage("main_profile_time_zones_auto_no") ?></option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><? echo GetMessage("main_profile_time_zones_zones") ?></td>
                                                        <td>
                                                            <select name="TIME_ZONE"<? if ($arResult["arUser"]["AUTO_TIME_ZONE"] <> "N") echo ' disabled="disabled"' ?>>
                                                                <? foreach ($arResult["TIME_ZONE_LIST"] as $tz => $tz_name): ?>
                                                                    <option value="<?= htmlspecialcharsbx($tz) ?>"<?= ($arResult["arUser"]["TIME_ZONE"] == $tz ? ' SELECTED="SELECTED"' : '') ?>><?= htmlspecialcharsbx($tz_name) ?></option>
                                                                <? endforeach ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                <? endif ?>
                                                </tbody>
                                            </table>
                                        <? } ?>
                                    </div>
                                    <div class="profile-link profile-user-div-link"><a title="<?= GetMessage("USER_SHOW_HIDE") ?>" href="javascript:void(0)"><h6 class="title_subline"><?= GetMessage("USER_PERSONAL_INFO") ?></h6></a></div>
                                    <div id="user_div_personal"
                                         class="profile-block-shown <? /*profile-block-*/
                                         (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                        <? if (!$_REQUEST["edit"]) { ?>
                                            <ul class="params">
                                                <? if ($arResult["arUser"]["USER_PROFESSION"]) { ?>
                                                    <li><?= GetMessage('USER_PROFESSION') ?>: <?= $arResult["arUser"]["USER_PROFESSION"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_WWW"]) { ?>
                                                    <li><?= GetMessage('USER_WWW') ?>: <?= $arResult["arUser"]["PERSONAL_WWW"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_ICQ"]) { ?>
                                                    <li><?= GetMessage('USER_ICQ') ?>: <?= $arResult["arUser"]["PERSONAL_ICQ"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_GENDER"]) { ?>
                                                    <li><?= GetMessage('USER_GENDER') ?>:
                                                        <select name="PERSONAL_GENDER">
                                                            <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                                            <option value="M"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                                            <option value="F"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                                                        </select>
                                                    </li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_BIRTHDAY"]) { ?>
                                                    <li><?= GetMessage('USER_BIRTHDAY_DT') ?>
                                                        (<?= $arResult["DATE_FORMAT"] ?>):
                                                        <?
                                                        $APPLICATION->IncludeComponent(
                                                            'bitrix:main.calendar',
                                                            '',
                                                            array(
                                                                'SHOW_INPUT' => 'Y',
                                                                'FORM_NAME' => 'form1',
                                                                'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                                                'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                                                'SHOW_TIME' => 'N'
                                                            ),
                                                            null,
                                                            array('HIDE_ICONS' => 'Y')
                                                        );

                                                        //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "form1", "15")
                                                        ?>
                                                    </li>
                                                <? } ?>

                                                <? if ($arResult["arUser"]["PERSONAL_PHOTO_HTML"]) { ?>
                                                    <li>
                                                        <h6 class="title"><?= GetMessage('USER_PHOTO') ?></h6>
                                                        <?
                                                        if (strlen($arResult["arUser"]["PERSONAL_PHOTO"]) > 0) {
                                                            ?>
                                                            <?= $arResult["arUser"]["PERSONAL_PHOTO_HTML"] ?>
                                                            <?
                                                        }
                                                        ?>
                                                    </li>
                                                <? } ?>


                                                <? if ($arResult["arUser"]["PERSONAL_PHONE"]) { ?>
                                                    <li><?= GetMessage('USER_PHONE') ?>: <?= $arResult["arUser"]["PERSONAL_PHONE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_FAX"]) { ?>
                                                    <li><?= GetMessage('USER_FAX') ?>: <?= $arResult["arUser"]["PERSONAL_FAX"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_MOBILE"]) { ?>
                                                    <li><?= GetMessage('USER_MOBILE') ?>: <?= $arResult["arUser"]["PERSONAL_MOBILE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_PAGER"]) { ?>
                                                    <li><?= GetMessage('USER_PAGER') ?>: <?= $arResult["arUser"]["PERSONAL_PAGER"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["COUNTRY_SELECT"]) { ?>
                                                    <li><?= GetMessage('USER_COUNTRY') ?>: <?= $arResult["arUser"]["COUNTRY_SELECT"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_STATE"]) { ?>
                                                    <li><?= GetMessage('USER_STATE') ?>: <?= $arResult["arUser"]["PERSONAL_STATE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_CITY"]) { ?>
                                                    <li><?= GetMessage('USER_CITY') ?>: <?= $arResult["arUser"]["PERSONAL_CITY"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_ZIP"]) { ?>
                                                    <li><?= GetMessage('USER_ZIP') ?>: <?= $arResult["arUser"]["PERSONAL_ZIP"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_STREET"]) { ?>
                                                    <li><?= GetMessage('USER_STREET') ?>: <?= $arResult["arUser"]["PERSONAL_STREET"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_MAILBOX"]) { ?>
                                                    <li><?= GetMessage('USER_MAILBOX') ?>: <?= $arResult["arUser"]["PERSONAL_MAILBOX"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["PERSONAL_NOTES"]) { ?>
                                                    <li><?= GetMessage('USER_NOTES') ?>: <?= $arResult["arUser"]["PERSONAL_NOTES"] ?></li>
                                                <? } ?>
                                            </ul>
                                        <? } else { ?>
                                            <table class="data-table profile-table">
                                                <thead>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><?= GetMessage('USER_PROFESSION') ?></td>
                                                    <td><input type="text" name="PERSONAL_PROFESSION" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_PROFESSION"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_WWW') ?></td>
                                                    <td><input type="text" name="PERSONAL_WWW" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_WWW"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_ICQ') ?></td>
                                                    <td><input type="text" name="PERSONAL_ICQ" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_ICQ"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_GENDER') ?></td>
                                                    <td><select name="PERSONAL_GENDER">
                                                            <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                                                            <option value="M"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                                                            <option value="F"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_BIRTHDAY_DT") ?>
                                                        (<?= $arResult["DATE_FORMAT"] ?>):
                                                    </td>
                                                    <td><?
                                                        $APPLICATION->IncludeComponent(
                                                            'bitrix:main.calendar',
                                                            '',
                                                            array(
                                                                'SHOW_INPUT' => 'Y',
                                                                'FORM_NAME' => 'form1',
                                                                'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                                                'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                                                'SHOW_TIME' => 'N'
                                                            ),
                                                            null,
                                                            array('HIDE_ICONS' => 'Y')
                                                        );

                                                        //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "form1", "15")
                                                        ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_PHOTO") ?></td>
                                                    <td>
                                                        <?= $arResult["arUser"]["PERSONAL_PHOTO_INPUT"] ?>
                                                        <?
                                                        if (strlen($arResult["arUser"]["PERSONAL_PHOTO"]) > 0) {
                                                            ?>
                                                            <br/>
                                                            <?= $arResult["arUser"]["PERSONAL_PHOTO_HTML"] ?>
                                                            <?
                                                        }
                                                        ?></td>
                                                <tr>
                                                    <td colspan="2"
                                                        class="profile-header"><?= GetMessage("USER_PHONES") ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_PHONE') ?></td>
                                                    <td><input type="text" name="PERSONAL_PHONE" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_FAX') ?></td>
                                                    <td><input type="text" name="PERSONAL_FAX" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_FAX"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_MOBILE') ?></td>
                                                    <td><input type="text" name="PERSONAL_MOBILE" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_MOBILE"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_PAGER') ?></td>
                                                    <td><input type="text" name="PERSONAL_PAGER" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_PAGER"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"
                                                        class="profile-header"><?= GetMessage("USER_POST_ADDRESS") ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_COUNTRY') ?></td>
                                                    <td><?= $arResult["COUNTRY_SELECT"] ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_STATE') ?></td>
                                                    <td><input type="text" name="PERSONAL_STATE" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_STATE"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_CITY') ?></td>
                                                    <td><input type="text" name="PERSONAL_CITY" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_CITY"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_ZIP') ?></td>
                                                    <td><input type="text" name="PERSONAL_ZIP" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_ZIP"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_STREET") ?></td>
                                                    <td><textarea cols="30" rows="5"
                                                                  name="PERSONAL_STREET"><?= $arResult["arUser"]["PERSONAL_STREET"] ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_MAILBOX') ?></td>
                                                    <td><input type="text" name="PERSONAL_MAILBOX" maxlength="255"
                                                               value="<?= $arResult["arUser"]["PERSONAL_MAILBOX"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_NOTES") ?></td>
                                                    <td><textarea cols="30" rows="5"
                                                                  name="PERSONAL_NOTES"><?= $arResult["arUser"]["PERSONAL_NOTES"] ?></textarea>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        <? } ?>
                                    </div>

                                    <div class="profile-link profile-user-div-link hidden-xs"><a title="<?= GetMessage("USER_SHOW_HIDE") ?>" href="javascript:void(0)" ><h6 class="title_subline"><?= GetMessage("USER_WORK_INFO") ?></h6></a></div>
                                    <div id="user_div_work"
                                         class="profile-block-hidden <? /*profile-block-*/
                                         (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                        <? if (!$_REQUEST["edit"]) { ?>
                                            <ul class="params">
                                                <? if ($arResult["arUser"]["WORK_COMPANY"]) { ?>
                                                    <li><?= GetMessage('USER_COMPANY') ?>: <?= $arResult["arUser"]["WORK_COMPANY"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_WWW"]) { ?>
                                                    <li><?= GetMessage('USER_WWW') ?>: <?= $arResult["arUser"]["WORK_WWW"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_DEPARTMENT"]) { ?>
                                                    <li><?= GetMessage('USER_DEPARTMENT') ?>: <?= $arResult["arUser"]["WORK_DEPARTMENT"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_POSITION"]) { ?>
                                                    <li><?= GetMessage('USER_POSITION') ?>: <?= $arResult["arUser"]["WORK_POSITION"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_PROFILE"]) { ?>
                                                    <li><?= GetMessage('USER_WORK_PROFILE') ?>: <?= $arResult["arUser"]["WORK_PROFILE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_LOGO"]) { ?>
                                                    <li><?= GetMessage('USER_LOGO') ?>: <?= $arResult["arUser"]["WORK_LOGO_HTML"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_PHONE"]) { ?>
                                                    <li><?= GetMessage('USER_PHONE') ?>: <?= $arResult["arUser"]["WORK_PHONE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_FAX"]) { ?>
                                                    <li><?= GetMessage('USER_FAX') ?>: <?= $arResult["arUser"]["WORK_FAX"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_PAGER"]) { ?>
                                                    <li><?= GetMessage('USER_PAGER') ?>: <?= $arResult["arUser"]["WORK_PAGER"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["COUNTRY_SELECT_WORK"]) { ?>
                                                    <li><?= GetMessage('USER_COUNTRY') ?>: <?= $arResult["COUNTRY_SELECT_WORK"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_STATE"]) { ?>
                                                    <li><?= GetMessage('USER_STATE') ?>: <?= $arResult["arUser"]["WORK_STATE"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_CITY"]) { ?>
                                                    <li><?= GetMessage('USER_CITY') ?>: <?= $arResult["arUser"]["WORK_CITY"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_ZIP"]) { ?>
                                                    <li><?= GetMessage('USER_ZIP') ?>: <?= $arResult["arUser"]["WORK_ZIP"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_STREET"]) { ?>
                                                    <li><?= GetMessage('USER_STREET') ?>: <?= $arResult["arUser"]["WORK_STREET"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_MAILBOX"]) { ?>
                                                    <li><?= GetMessage('USER_MAILBOX') ?>: <?= $arResult["arUser"]["WORK_MAILBOX"] ?></li>
                                                <? } ?>
                                                <? if ($arResult["arUser"]["WORK_NOTES"]) { ?>
                                                    <li><?= GetMessage('USER_NOTES') ?>: <?= $arResult["arUser"]["WORK_NOTES"] ?></li>
                                                <? } ?>
                                            </ul>
                                        <? } else { ?>
                                            <table class="data-table profile-table">
                                                <thead>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><?= GetMessage('USER_COMPANY') ?></td>
                                                    <td><input type="text" name="WORK_COMPANY" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_COMPANY"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_WWW') ?></td>
                                                    <td><input type="text" name="WORK_WWW" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_WWW"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_DEPARTMENT') ?></td>
                                                    <td><input type="text" name="WORK_DEPARTMENT" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_DEPARTMENT"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_POSITION') ?></td>
                                                    <td><input type="text" name="WORK_POSITION" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_POSITION"] ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_WORK_PROFILE") ?></td>
                                                    <td><textarea cols="30" rows="5"
                                                                  name="WORK_PROFILE"><?= $arResult["arUser"]["WORK_PROFILE"] ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_LOGO") ?></td>
                                                    <td>
                                                        <?= $arResult["arUser"]["WORK_LOGO_INPUT"] ?>
                                                        <?
                                                        if (strlen($arResult["arUser"]["WORK_LOGO"]) > 0) {
                                                            ?>
                                                            <br/><?= $arResult["arUser"]["WORK_LOGO_HTML"] ?>
                                                            <?
                                                        }
                                                        ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"
                                                        class="profile-header"><?= GetMessage("USER_PHONES") ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_PHONE') ?></td>
                                                    <td><input type="text" name="WORK_PHONE" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_PHONE"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_FAX') ?></td>
                                                    <td><input type="text" name="WORK_FAX" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_FAX"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_PAGER') ?></td>
                                                    <td><input type="text" name="WORK_PAGER" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_PAGER"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"
                                                        class="profile-header"><?= GetMessage("USER_POST_ADDRESS") ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_COUNTRY') ?></td>
                                                    <td><?= $arResult["COUNTRY_SELECT_WORK"] ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_STATE') ?></td>
                                                    <td><input type="text" name="WORK_STATE" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_STATE"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_CITY') ?></td>
                                                    <td><input type="text" name="WORK_CITY" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_CITY"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_ZIP') ?></td>
                                                    <td><input type="text" name="WORK_ZIP" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_ZIP"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_STREET") ?></td>
                                                    <td><textarea cols="30" rows="5"
                                                                  name="WORK_STREET"><?= $arResult["arUser"]["WORK_STREET"] ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage('USER_MAILBOX') ?></td>
                                                    <td><input type="text" name="WORK_MAILBOX" maxlength="255"
                                                               value="<?= $arResult["arUser"]["WORK_MAILBOX"] ?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td><?= GetMessage("USER_NOTES") ?></td>
                                                    <td><textarea cols="30" rows="5"
                                                                  name="WORK_NOTES"><?= $arResult["arUser"]["WORK_NOTES"] ?></textarea>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        <? } ?>
                                    </div>
                                    <?
                                    if ($arResult["INCLUDE_FORUM"] == "N") {
                                        ?>

                                        <div class="profile-link profile-user-div-link"><a
                                                    title="<?= GetMessage("USER_SHOW_HIDE") ?>"
                                                    href="javascript:void(0)"><h6
                                                        class="title_subline"><?= GetMessage("forum_INFO") ?></h6></a>
                                        </div>
                                        <div id="user_div_forum"
                                             class="profile-block-shown <? /*profile-block-*/
                                             (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                            <?
                                            if (!$_REQUEST["edit"]) { ?>
                                                <ul class="params">
                                                    <? if ($arResult["arForumUser"]["SHOW_NAME"]) { ?>
                                                        <li><?= GetMessage('forum_SHOW_NAME') ?>: <? if ($arResult["arForumUser"]["SHOW_NAME"] == "Y") echo "Выбрано"; ?></li>
                                                    <? } ?>
                                                    <? if ($arResult["arForumUser"]["DESCRIPTION"]) { ?>
                                                        <li><?= GetMessage('forum_DESCRIPTION') ?>: <?= $arResult["arForumUser"]["DESCRIPTION"] ?></li>
                                                    <? } ?>
                                                    <? if ($arResult["arForumUser"]["INTERESTS"]) { ?>
                                                        <li><?= GetMessage('forum_INTERESTS') ?>: <?= $arResult["arForumUser"]["INTERESTS"] ?></li>
                                                    <? } ?>
                                                    <? if ($arResult["arForumUser"]["SIGNATURE"]) { ?>
                                                        <li><?= GetMessage('forum_SIGNATURE') ?>: <?= $arResult["arForumUser"]["SIGNATURE"] ?></li>
                                                    <? } ?>
                                                    <? if (strlen($arResult["arForumUser"]["AVATAR"]) > 0) { ?>
                                                        <li><?= GetMessage('forum_AVATAR') ?>: <?= $arResult["arForumUser"]["AVATAR"] ?></li>
                                                    <? } ?>
                                                </ul>
                                            <? } else { ?>
                                                <table class="data-table profile-table">
                                                    <thead>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?= GetMessage("forum_SHOW_NAME") ?></td>
                                                        <td><input type="hidden" name="forum_SHOW_NAME"
                                                                   value="N"/><input type="checkbox"
                                                                                     name="forum_SHOW_NAME"
                                                                                     value="Y" <? if ($arResult["arForumUser"]["SHOW_NAME"] == "Y") echo "checked=\"checked\""; ?> />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage('forum_DESCRIPTION') ?></td>
                                                        <td><input type="text" name="forum_DESCRIPTION" maxlength="255"
                                                                   value="<?= $arResult["arForumUser"]["DESCRIPTION"] ?>"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage('forum_INTERESTS') ?></td>
                                                        <td><textarea cols="30" rows="5"
                                                                      name="forum_INTERESTS"><?= $arResult["arForumUser"]["INTERESTS"]; ?></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage("forum_SIGNATURE") ?></td>
                                                        <td><textarea cols="30" rows="5"
                                                                      name="forum_SIGNATURE"><?= $arResult["arForumUser"]["SIGNATURE"]; ?></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage("forum_AVATAR") ?></td>
                                                        <td><?= $arResult["arForumUser"]["AVATAR_INPUT"] ?>
                                                            <?
                                                            if (strlen($arResult["arForumUser"]["AVATAR"]) > 0) {
                                                                ?>
                                                                <br/><?= $arResult["arForumUser"]["AVATAR_HTML"] ?>
                                                                <?
                                                            }
                                                            ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            <? } ?>
                                        </div>

                                        <?
                                    }
                                    ?>
                                    <?
                                    if ($arResult["INCLUDE_BLOG"] == "N") {
                                        ?>
                                        <div class="profile-link profile-user-div-link"><a
                                                    title="<?= GetMessage("USER_SHOW_HIDE") ?>"
                                                    href="javascript:void(0)"><h6
                                                        class="title_subline"><?= GetMessage("blog_INFO") ?></h6></a>
                                        </div>
                                        <div id="user_div_blog"
                                             class="profile-block-shown <? /*profile-block-*/
                                             (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                            <?
                                            if (!$_REQUEST["edit"]) { ?>
                                                <ul class="params">
                                                    <? if (strlen($arResult["arBlogUser"]["ALIAS"]) > 0) { ?>
                                                        <li><?= GetMessage('blog_ALIAS') ?>: <?= $arResult["arBlogUser"]["ALIAS"] ?></li>
                                                    <? } ?>
                                                    <? if (strlen($arResult["arBlogUser"]["DESCRIPTION"]) > 0) { ?>
                                                        <li><?= GetMessage('blog_DESCRIPTION') ?>: <?= $arResult["arBlogUser"]["DESCRIPTION"] ?></li>
                                                    <? } ?>
                                                    <? if (strlen($arResult["arBlogUser"]["INTERESTS"]) > 0) { ?>
                                                        <li><?= GetMessage('blog_INTERESTS') ?>: <?= $arResult["arBlogUser"]["INTERESTS"] ?></li>
                                                    <? } ?>
                                                    <? if (strlen($arResult["arBlogUser"]["AVATAR"]) > 0) { ?>
                                                        <li><?= GetMessage('blog_AVATAR') ?>: <?= $arResult["arBlogUser"]["AVATAR_HTML"] ?></li>
                                                    <? } ?>
                                                </ul>
                                            <? } else { ?>
                                                <table class="data-table profile-table">
                                                    <thead>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?= GetMessage('blog_ALIAS') ?></td>
                                                        <td><input class="typeinput" type="text" name="blog_ALIAS"
                                                                   maxlength="255"
                                                                   value="<?= $arResult["arBlogUser"]["ALIAS"] ?>"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage('blog_DESCRIPTION') ?></td>
                                                        <td><input class="typeinput" type="text" name="blog_DESCRIPTION"
                                                                   maxlength="255"
                                                                   value="<?= $arResult["arBlogUser"]["DESCRIPTION"] ?>"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage('blog_INTERESTS') ?></td>
                                                        <td><textarea cols="30" rows="5" class="typearea"
                                                                      name="blog_INTERESTS"><? echo $arResult["arBlogUser"]["INTERESTS"]; ?></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage("blog_AVATAR") ?></td>
                                                        <td><?= $arResult["arBlogUser"]["AVATAR_INPUT"] ?>
                                                            <?
                                                            if (strlen($arResult["arBlogUser"]["AVATAR"]) > 0) {
                                                                ?>
                                                                <br/><?= $arResult["arBlogUser"]["AVATAR_HTML"] ?>
                                                                <?
                                                            }
                                                            ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            <? } ?>
                                        </div>
                                        <?
                                    }
                                    ?>
                                    <? if ($arResult["INCLUDE_LEARNING"] == "N"): ?>
                                        <div class="profile-link profile-user-div-link"><a
                                                    title="<?= GetMessage("USER_SHOW_HIDE") ?>"
                                                    href="javascript:void(0)"><h6
                                                        class="title_subline"><?= GetMessage("learning_INFO") ?></h6>
                                            </a></div>
                                        <div id="user_div_learning"
                                             class="profile-block-shown <? /*profile-block-*/
                                             (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                            <? if (!$_REQUEST["edit"]) { ?>
                                                <ul class="params">
                                                    <? if (strlen($arResult["arStudent"]["PUBLIC_PROFILE"]) > 0) { ?>
                                                        <li><?= GetMessage('learning_PUBLIC_PROFILE') ?>: <? if ($arResult["arStudent"]["PUBLIC_PROFILE"] == "Y") echo "Выбрано"; ?></li>
                                                    <? } ?>
                                                    <? if (strlen($arResult["arStudent"]["RESUME"]) > 0) { ?>
                                                        <li><?= GetMessage('learning_RESUME') ?>: <?= $arResult["arStudent"]["RESUME"]; ?></li>
                                                    <? } ?>
                                                    <? if (strlen($arResult["arStudent"]["TRANSCRIPT"]) > 0) { ?>
                                                        <li><?= GetMessage('learning_TRANSCRIPT') ?>: <?= $arResult["arStudent"]["TRANSCRIPT"]; ?>-<?= $arResult["ID"] ?></li>
                                                    <? } ?>
                                                </ul>
                                            <? } else { ?>
                                                <table class="data-table profile-table">
                                                    <thead>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?= GetMessage("learning_PUBLIC_PROFILE"); ?>:</td>
                                                        <td><input type="hidden" name="student_PUBLIC_PROFILE"
                                                                   value="N"/><input type="checkbox"
                                                                                     name="student_PUBLIC_PROFILE"
                                                                                     value="Y" <? if ($arResult["arStudent"]["PUBLIC_PROFILE"] == "Y") echo "checked=\"checked\""; ?> />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= GetMessage("learning_RESUME"); ?>:</td>
                                                        <td><textarea cols="30" rows="5"
                                                                      name="student_RESUME"><?= $arResult["arStudent"]["RESUME"]; ?></textarea>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><?= GetMessage("learning_TRANSCRIPT"); ?>:</td>
                                                        <td><?= $arResult["arStudent"]["TRANSCRIPT"]; ?>
                                                            -<?= $arResult["ID"] ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            <? } ?>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["IS_ADMIN"]): ?>
                                        <div class="profile-link profile-user-div-link"><a
                                                    title="<?= GetMessage("USER_SHOW_HIDE") ?>"
                                                    href="javascript:void(0)"><h6
                                                        class="title_subline"><?= GetMessage("USER_ADMIN_NOTES") ?></h6>
                                            </a></div>
                                        <div id="user_div_admin"
                                             class="profile-block-shown <? /*profile-block-*/
                                             (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                            <? if (!$_REQUEST["edit"]) { ?>
                                                <ul class="params">
                                                    <? if (strlen($arResult["arUser"]["ADMIN_NOTES"]) > 0) { ?>
                                                        <li><?= GetMessage('USER_ADMIN_NOTES') ?>: <?= $arResult["arUser"]["ADMIN_NOTES"]; ?></li>
                                                    <? } ?>
                                                </ul>
                                            <? } else { ?>
                                                <table class="data-table profile-table">
                                                    <thead>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?= GetMessage("USER_ADMIN_NOTES") ?>:</td>
                                                        <td><textarea cols="30" rows="5"
                                                                      name="ADMIN_NOTES"><?= $arResult["arUser"]["ADMIN_NOTES"] ?></textarea>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            <? } ?>
                                        </div>
                                    <? endif; ?>

									<h6 class="title_subline personal_prfile_block"><?= GetMessage("DELIVERY_TYPE_EDIT_TAB") ?></h6>
                                    <div class="forms-grid personal_prfile_block">
                                        <? $APPLICATION->IncludeComponent("bitrix:sale.personal.profile.detail", "", Array(
                                            "LIST_PROFILE" => true,
                                            "COMPATIBLE_LOCATION_MODE" => "N",
                                            "PATH_TO_LIST" => "/personal/private/?edit=y&succeed=y",
                                            "PATH_TO_DETAIL" => "profile_detail.php?ID=#ID#",
                                            "ID" => $arResult["BAYER_ID"],
                                            "USE_AJAX_LOCATIONS" => "N",
                                            "SET_TITLE" => "N"
                                        )); ?>
                                    </div>

                                    <? // ********************* User properties ***************************************************?>
                                    <? if ($arResult["USER_PROPERTIES"]["SHOW"] == "N"): ?>
                                        <div class="profile-link profile-user-div-link"><a
                                                    title="<?= GetMessage("USER_SHOW_HIDE") ?>"
                                                    href="javascript:void(0)">
                                                <h6 class="title_subline"><?= strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB") ?></h6>
                                            </a></div>
                                        <div id="user_div_user_properties"
                                             class="profile-block-shown <? /*profile-block-*/
                                             (strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown") ?>">
                                            <? if (!$_REQUEST["edit"]) { ?>
                                                <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
                                                    <li class="field-name">
                                                        <?= $arUserField["EDIT_FORM_LABEL"] ?>:
                                                        <? $APPLICATION->IncludeComponent(
                                                            "bitrix:system.field.edit",
                                                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS" => "Y")); ?>
                                                    </li>
                                                <? endforeach; ?>
                                            <? } else { ?>
                                                <table class="data-table profile-table">
                                                    <thead>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <? $first = true; ?>
                                                    <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
                                                        <tr>
                                                            <td class="field-name">
                                                                <? if ($arUserField["MANDATORY"] == "Y"): ?>
                                                                    <span class="starrequired">*</span>
                                                                <? endif; ?>
                                                                <?= $arUserField["EDIT_FORM_LABEL"] ?>:
                                                            </td>
                                                            <td class="field-value">
                                                                <? $APPLICATION->IncludeComponent(
                                                                    "bitrix:system.field.edit",
                                                                    $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                                                    array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS" => "Y")); ?></td>
                                                        </tr>
                                                    <? endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <? } ?>
                                        </div>
                                    <? endif; ?>
                                    <? if ($_REQUEST["edit"]) {

                                        /*if($arResult["SOCSERV_ENABLED"])
                                {
                                    $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                                        "SHOW_PROFILES" => "Y",
                                        "ALLOW_DELETE" => "Y"
                                    ),
                                        false
                                    );
                                }*/
                                        ?>
                                    <? } ?>
                                    <? // ******************** /User properties ***************************************************?>
                                    <? if ($_REQUEST["edit"]) { ?>
                                        <div class="buttons">
                                            <p><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
                                            <input type="submit" name="save"
                                                   value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>"
                                                   style="display:none;">
                                            <input type="reset" name="reset" value="<?= GetMessage('MAIN_RESET'); ?>"
                                                   style="display:none;">
                                            <input type="hidden" name="save"
                                                   value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>">
                                            <a href="#save"
                                               onclick="javascript:$('form[name=form1] input[type=submit]').click();"
                                               class="btn mint">
                                                &nbsp;<?= GetMessage("PL_SAVE") ?>
                                            </a>
                                            <a href="#reset"
                                               onclick="javascript:$('form[name=form1] input[type=reset]').click();"
                                               class="btn mint">
                                                <?= GetMessage("PL_CLEAR") ?>
                                            </a>
                                        </div>
                                    <? } else { ?>
                                        <div class="buttons">
                                            <a href="/personal/private/?edit=y" class="btn mint">Редактировать
                                                данные</a>
                                        </div>
                                    <? } ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $output ?>
            </div>
        </div>
    </section>


<? } ?>