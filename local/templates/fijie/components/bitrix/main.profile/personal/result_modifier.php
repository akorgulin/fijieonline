<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$userID = $USER->GetID();
$db_sales = CSaleOrderUserProps::GetList( array("DATE_UPDATE" => "DESC"), array("USER_ID" =>$userID) );
$profile_delivery = $db_sales->fetch();


//Нет профиля создаём профиль покупателя
if(!$profile_delivery) {
    //Создаём профиль пользователя
    $arFields = array(
        "NAME" => "Профиль покупателя ".$userPropsID,
        "USER_ID" => $userID,
        "PERSON_TYPE_ID" => 1
    );
    $userPropsID = CSaleOrderUserProps::Add($arFields);
    foreach ($arParams['propsProfile'] as $name => $propsID) {
        $arFieldsPr = array(
            "USER_PROPS_ID" => $userPropsID,
            "ORDER_PROPS_ID" => $propsID,
            "NAME" => $arParams['propsProfileName'][$name],
            "VALUE" => $_REQUEST[$name]
        );
        CSaleOrderUserPropsValue::Add($arFieldsPr);
        if ($arFieldsPr['VALUE']){
            $arResult['profile'][mb_strtolower($arFieldsPr['NAME'])] = $arFieldsPr['VALUE'];
        }
        $arResult['profileEn'][$name] = $arFieldsPr['VALUE'];
    }

    $arResult["BAYER_ID"] = $userPropsID;
} else {
    $arResult["BAYER_ID"] = $profile_delivery["ID"];
}
?>