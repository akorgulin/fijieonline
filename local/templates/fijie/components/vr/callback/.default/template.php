<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$dirName = dirname(__FILE__);
$arResult["TEMPLATE_DIR"] = substr($dirName, strpos($dirName,"/bitrix/"));

$com_path = '//'.SITE_SERVER_NAME.'/local/templates/fijie/components/vr/callback/.default';
$this->addExternalCss($com_path."/vrcallback.css");
$this->addExternalJS($com_path."/vrcallback.js");

?>
<?if($arParams["INCLUDE_JQUERY"]!="N"):?><script type="text/javascript" src="<?=$arResult["TEMPLATE_DIR"];?>/js/jquery-1.9.js"></script>
<?endif;?>
<a data-fancybox="" data-src="#recall" href="javascript:void(0);" onclick="switchCallbackForm(this);" class="iconlink mobile_restyle" data-aos="fade-right" data-aos-delay="600">
    <span class="lnr lnr-phone-handset"></span><span class="hidden_mobile"><?=GetMessage("VR_BUILD_OBRATNYY")?> <?=GetMessage("VR_BUILD_ZVONOK")?></span>
</a>
<script type="text/javascript">
	function submitCallbackData(data)
	{
		//alert(data);
		data += "&IBLOCK_ID=<?=$arParams["IBLOCK_ID"]?>";
		$.ajax(
		{
			url: "<?=$arResult["CALLBACK_AJAX_PROCESSOR"]?>",
			//url: "\\bitrix\\components\\vr\\callback\\ajax\\process.php",
			type: "GET",	
			data: data,
			cache: false,
			error: function (er, ajaxOptions, thrownError) {
				alert('Sorry, unexpected error. Please try again later.');
				return false;
			}, 
			success: function (html) 
			{		
				if (html!=1) 
				{
					alert(html);
					alert("<?=GetMessageJS("VR_BUILD_NE_UDALOSQ_OTPRAVITQ")?>");
					return false;
				}
                $(".vr-callback .policy_info").css("display","none");
                $(".vr-callback .policy_info_1").css("display","none");
                $(".vr-callback p.socessful").css("display","block");
				closeCallbackForm(null);
				$('.vr-callback-overlayBox').fadeOut();
				$('.vr-callback-popup-overlay').fadeIn('slow');
				setTimeout(' $(".vr-callback-popup-overlay").fadeOut("slow")', 5000);

			}		
		});
		return false;
	}
</script>
<style type="text/css">
.noneButton{display:none;}
.showButton{display:none;}
.vr-callback-popup-overlay, .vr-callback {display:none;}
p.socessful {margin:0px;}
</style>

<div class="vr-callback-popup-overlay">
	<div class="popup-close">
		<a href="javascript:void(0)" class="popup__close"  onclick="closeSuccessForm(this);">
			x
		</a>
		<h2 style="font-family:arial; font-weight:normal;"><?=GetMessage("VR_CALLBACK_SPASIBO");?></h2>
		<p style=" font-size:20px;"><?=GetMessage("VR_CALLBACK_VASA_ZAAVKA_OTPRAVLE");?></p>
		<p style=" font-size:20px; margin-top:15px;"><?=GetMessage("VR_CALLBACK_V_BLIJAYSEE_VREMA_S");?><br /><?=GetMessage("VR_CALLBACK_DLA_UTOCNENIA_DETALE")?></p>
	</div>
</div>
<div class="vr-callback popup center animated-modal" id="recall">
    <div class="logo_header aos-init aos-animate" data-aos="flip-left" data-aos-delay="500"><a href="#">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/include/company_title.php",
                Array(),
                Array("MODE"=>"html")
            );?>
        </a>
    </div>

    <h5>Обратный звонок</h5>
    <p class="policy_info_1">Оставьте контактные данные и мы свяжемся с Вами в течение 2 часов</p>
    <p class="socessful">Спасибо, Ваша заявка отправлена! В Ближайшее время с Вами свяжется наш менеджер для уточнения всех интересующих Вас деталей!</p>
	<form action="" method="post" name="callbackForm" id="callbackForm" onsubmit="submitCallbackForm(this); yaCounter55176931.reachGoal('callback'); return false;">
		<div class="popup_form" style="display: none;" onclick="closeCallbackForm(this);"></div>
		<div class="popup-w" style="display: none;">
			<a href="javascript:void(0)" class="popup__close" onclick="closeCallbackForm(this);">
				x
			</a>
			<div class="inner forms-grid">
				<h2 id="lol" style="margin: 30px 0 40px 0; font-size: 23px; color:#333; font-weight:normal; ">
					<?=GetMessage("VR_CALLBACK_ZAPOLNITE_FORMU_I_MY")?>
					<br/>
					<?=GetMessage("VR_CALLBACK_OBAZATELQNO_PEREZVON")?>
				</h2>
				<h2 id="lol2" style="margin: 30px 0 40px 0; font-size: 23px; color:#333; font-weight:normal; ">
					<?=GetMessage("VR_BUILD_POLUCITE_PRIMEROV")?> 
					<br/>
					<?=GetMessage("VR_BUILD_NASIH_RABOT_NA_POCTU")?>
				</h2>
				<h2 id="lol3" style="margin: 30px 0 40px 0; font-size: 23px; color:#333; font-weight:normal; ">
					<?=GetMessage("VR_BUILD_BESPLATNYY_RASCET")?>  
					<br/>
					<?=GetMessage("VR_BUILD_STOIMOSTI")?>
				</h2>
				
				<?
				$valid_data=(($arParams["EMAIL_TO"]=='')&&($arParams["IBLOCK_ID"]==''))?false:true;
				if(!($valid_data)):?>
					<div class="error-text" style="margin-bottom:40px;">
					<?=GetMessage("VR_CALLBACK_EMAIL_IBLOCK_ERROR");?>
					</div>
				<?endif;?>
				<?$valid_data = ($arParams["EMAIL_TO"] != "")?check_email($arParams["EMAIL_TO"]):true;
				if(!($valid_data)):?>
					<div class="error-text" style="margin-bottom:40px;">
					<?=GetMessage("VR_CALLBACK_EMAIL_ERROR");?>
					'<i><?=$arParams["EMAIL_TO"]?></i>')
					</div>
				<?endif;?>
				<div class="input_boxes">
				    <input type="text" class="input-text" name="kvadrat" value="" id="kvadrat" title="" placeholder="<?=GetMessage("VR_BUILD_KOLICESTVO_M")?>"/>

                    <div class="forms-col-12">
                        <div class="border-form">
                            <div class="without_icon_input">
                                <div class="icon"><span class="lnr lnr-user"></span></div>
                                <input type="text" class="input-text" name="fio" value="" title="<?=GetMessage("VR_CALLBACK_VVEDITE_IMA")?>"  placeholder="<?=GetMessage("VR_CALLBACK_VVEDITE_IMA")?>"/>
                            </div>
                        </div>
                    </div>

					<input type="text" class="input-text" name="email" value="" id="noneDisp" title="" placeholder="<?=GetMessage("VR_BUILD_VVEDITE")?> email"/>

                    <div class="forms-col-12">
                        <div class="border-form">
                            <div class="without_icon_input">
                                <div class="icon"><span class="lnr lnr-phone-handset"></span></div>
                                <input type="text" class="input-text" name="phone" value="" title="<?=GetMessage("VR_CALLBACK_VVEDITE_KONTAKTNYY_T")?>"  placeholder="<?=GetMessage("VR_CALLBACK_VVEDITE_KONTAKTNYY_T")?>"/><br/>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="email_to" value="<?=$arParams["EMAIL_TO"];?>"/>
					<input type="hidden" name="PROPERTY_FIO" value="<?=$arParams["PROPERTY_FIO"];?>"/>
					<input type="hidden" name="PROPERTY_FORM_NAME" value="<?=$arParams["PROPERTY_FORM_NAME"];?>"/>
					<input type="hidden" name="page" value="<?=$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];?>"/>
					<input type="hidden" name="PROPERTY_PAGE" value="<?=$arParams["PROPERTY_PAGE"];?>"/>
					<input type="hidden" name="MAIL_TEMPLATE" value="<?=$arParams["MAIL_TEMPLATE"];?>"/>
                    <div class="forms-col-12">
                        <div class="border-form">
					        <a class="btn mint subm"><input class="btn mint button hideButton" type="submit" <?if(!($valid_data)):?>disabled="disabled"<?endif;?> name="submit" value="<?=GetMessage("VR_CALLBACK_ZAKAZATQ_ZVONOK")?>" style="width: 100%;"/></a>
                        </div>
                    </div>
                    <div class="forms-col-12 hidden-xs">
                        <div class="border-form">
                            <input class="button noneButton" type="submit" <?if(!($valid_data)):?>disabled="disabled"<?endif;?> name="submit"  style="background:#ff6550; width:320px;" value="<?=GetMessage("VR_BUILD_OTPRAVITQ")?>" style="width: 100%;"/>
                        </div>
                    </div>
                    <input class="button showButton" type="submit" <?if(!($valid_data)):?>disabled="disabled"<?endif;?> name="submit"  style="background:#ff6550; width:320px;" value="<?=GetMessage("VR_BUILD_POLUCITQ")?>!" style="width: 100%;"/>
				</div>				
				<br/>
			</div>
		</div>
	</form>
    <div class="policy_info">Нажимая на кнопку «Перезвоните мне!», я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a href="/policy">политикой конфиденциальности</a>.</div>
</div>