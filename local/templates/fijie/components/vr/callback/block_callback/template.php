<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><??>
<section class="contact_block center">
    <div class="wrapper">
        <h3 data-aos="flip-up">Ничего не нашли? Спросите нас!</h3>
        <div class="description" data-aos="fade-up">Если возникли трудности с пониманием данного раздела, мы рады Вам помочь в телефонном режиме:</div>
		<div class="phone" data-aos="fade-up"><span>+7 (903) 797-16-60</span> <a data-fancybox data-src="#recall" href="javascript:void(0);" onclick="switchCallbackForm(this);" class="btn mint iconlink mobile_restyle aos-init aos-animate">Обратный звонок</a></div>
        <div class="worktime" data-aos="fade-up"><strong>Ежедневно с 10:00 до 19:00</strong></div>
    </div>
</section>