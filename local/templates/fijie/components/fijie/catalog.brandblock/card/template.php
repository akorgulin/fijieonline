<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
 
if (empty($arResult["BRAND_BLOCKS"]))
    return;


$strRand = $this->randString();
$strObName = 'obIblockBrand_' . $strRand;
$blockID = 'bx_IblockBrand_' . $strRand;
$mouseEvents = 'onmouseover="' . $strObName . '.itemOver(this);" onmouseout="' . $strObName . '.itemOut(this)"';


if ($arParams['SINGLE_COMPONENT'] == "Y")
    echo '<div class="bx_item_detail_inc_two_' . count($arResult['BRAND_BLOCKS']) . ' general" id="' . $blockID . '">';
else
    echo '<div class="bx_item_detail_inc_two" id="' . $blockID . '">';

$handlerIDS = array();

foreach ($arResult["BRAND_BLOCKS"] as $blockId => $arBB) {
    $brandID = 'brand_' . $arResult['ID'] . '_' . $blockId . '_' . $strRand;
    $popupID = $brandID . '_popup';

    $popupContext = '';
    $shortDescr = '';
    $useLink = $arBB['LINK'] !== false;
    $usePopup = $arBB['FULL_DESCRIPTION'] !== false;
    if ($usePopup) {
        if (preg_match('/<a[^>]+>[^<]+<\/a>/', $arBB['FULL_DESCRIPTION']) == 1)
            $useLink = false;
        $popupContext = '<span class="bx_popup" id="' . $popupID . '">' .
            '<span class="arrow"></span>' .
            '<span class="text">' . $arBB['FULL_DESCRIPTION'] . '</span>' .
            '</span>';
    }

    switch ($arBB['TYPE']) {
        case 'ONLY_PIC':
            $tagAttrs = 'id="' . $brandID . '_vidget" class="brandblock-block"' .
                ' ';
            break;
        default:
            $tagAttrs = 'id="' . $brandID . '_vidget"' . (
                empty($arBB['PICT'])
                    ? ' class="brandblock-block"'
                    : ' class="brandblock-block icon"'
                );
            if ($arBB['DESCRIPTION'] !== false)
                $shortDescr = '<span class="brandblock-text">' . htmlspecialcharsbx($arBB['DESCRIPTION']) . '</span>';
            break;
    }
     
    if ($usePopup)
        $tagAttrs .= ' data-popup="' . $popupID . '"';

    ?>
    <div class="item_brand clearfix" id="<?= $brandID; ?>"
         class="brandblock-container"<? echo($usePopup ? ' data-popup="' . $popupID . '"' : ''); ?>>
        <?
        if ($useLink) {
            ?>
            <div class="brand_logo">
            <a href="<?= htmlspecialcharsbx($arBB['LINK']); ?>" <?= $tagAttrs; ?> target="_blank">
                <? $popupContext . $shortDescr; ?>
                <img src="<?= $arBB['PICT']['SRC'] ?>" alt="">
            </a>
            </div><?
        } else {
            ?>
            <div class="brand_logo">
            <a href="javascript:void()" <?= $tagAttrs; ?> target="_blank">
                <?
                $popupContext . $shortDescr; ?>
                <img src="<?= $arBB['PICT']['SRC'] ?>" alt="">
            </a>
            </div><?
        }
        ?>
        <div class="brand_info">Бренд:<span><?= $arBB["NAME"] ?></span></div>
    </div>

    <?


    if ($usePopup)
        $handlerIDS[] = $brandID;
}
?>
    </div>
    <div style="clear: both;"></div>
<?
if (!empty($handlerIDS)) {
    $jsParams = array(
        'blockID' => $blockID
    );
    ?>
    <script type="text/javascript">
        var <? echo $strObName; ?> =
        new JCIblockBrands(<? echo CUtil::PhpToJSObject($jsParams); ?>);
    </script>
    <?
}