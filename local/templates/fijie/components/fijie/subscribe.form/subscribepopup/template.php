<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>
<? $APPLICATION->IncludeComponent("fijie:subscribe.simple", ".default", Array(
    "AJAX_MODE" => "Y",
    "SHOW_HIDDEN" => "Y",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "SET_TITLE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
)); ?>
