<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Logos area -->
<section class="logos_area">
    <div class="container">
        <div class="row left">
            <h1 class="page_header aos-init aos-animate left" data-aos="fade-up" data-aos-delay="300"><?$APPLICATION->ShowTitle()?>:</h1>
            <? foreach($arResult["BRANDS_LIST"] as $i=>$itemBrand) { ?>
                <?if($itemBrand["PREVIEW_PICTURE_SRC"]) { ?>
                <div class="logo_item center" data-aos="fade-up" data-aos-delay="200">
                    <a href="<?=$itemBrand["URL"]?>" >
                        <img src="<?=$itemBrand["PREVIEW_PICTURE_SRC"]?>" alt="">
                    </a>
                </div>
                <? } ?>
            <? } ?>
        </div>
    </div>
</section>
