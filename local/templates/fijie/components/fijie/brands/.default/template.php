<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--Logos-->
<section class="logos">
    <div class="container">
        <? foreach($arResult['BRANDS'] as $itemBrandRow) { ?>
            <div class="row">
                <? foreach($itemBrandRow as $itemBrand) { ?>
                    <?if($itemBrand["PREVIEW_PICTURE"]) {  ?>
                        <div class="col-3 center" data-aos="fade-right" data-aos-delay="200">
                            <div class="vertical_align"><a href="<?=$itemBrand["URL"]?>"><img src="<?=CFile::GetPath($itemBrand["PREVIEW_PICTURE"])?>" alt=""></a></div>
                        </div>
                    <? } ?>
                <? } ?>
            </div>
        <? } ?>
    </div>
</section>
