<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult['BRANDS']['PROFESSIONAL'])) { ?>
    <h2 data-aos="fade-up" data-aos-delay="200">Для профессионалов</h2>
    <? foreach($arResult['BRANDS']['PROFESSIONAL'] as $i=>$itemBrandProf) {  ?>
        <?php if($itemBrandProf["PREVIEW_PICTURE"]) { ?>
            <div class="logo_item center" data-aos="fade-up" data-aos-delay="200">
                <a <?if($itemBrandProf["URL"]) { ?> href="<?=$itemBrandProf["URL"]?>"  <? } ?>>
                    <img src="<?=CFile::GetPath($itemBrandProf["PREVIEW_PICTURE"])?>" alt="">
                </a>
            </div>
        <?php } ?>
    <? } ?>
<? }?>
