<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- Logos area -->
<section class="logos_area">
    <div class="container">
        <div class="row">
            <?php if(!empty($arResult['TOP_BRANDS'])) {  ?>
                <h2 class="logo_block" data-aos="fade-up" data-aos-delay="200">Институт красоты FIJIE представляет следующие бренды</h2>
                <?php foreach($arResult['TOP_BRANDS'] as $i=>$itemBrand) {  ?>
                    <?php if($itemBrand["PREVIEW_PICTURE"]) { ?>
                    <div class="logo_item center" data-aos="fade-up" data-aos-delay="200">
                        <a <?if($itemBrand["URL"]) { ?> href="<?=$itemBrand["URL"]?>"  <? } ?>>
                            <img src="<?=CFile::GetPath($itemBrand["PREVIEW_PICTURE"])?>" alt="">
                        </a>
                    </div>
                    <?php } ?>
                <?php } ?>
                <hr data-aos="fade-up" data-aos-delay="200">
            <?php }?>
        </div>
    </div>
</section>
