<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? foreach($arResult["BRANDS_PROPERTY"] as $i=>$itemBrand) { ?>
    <?if($itemBrand["PREVIEW_PICTURE"]["src"]) { ?>
        <div class="item center">
            <a href="/brands/<?=strtolower($itemBrand["UF_XML_ID"])?>/" >
                <img src="<?=$itemBrand["PREVIEW_PICTURE"]["src"]?>" alt="">
            </a>
        </div>
    <? } ?>
<? } ?>

