$(document).ready(function ($) {





	/* ==========================================================================
		Tabs
		========================================================================== */
	$('ul.tabs li').click(function () {
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#" + tab_id).addClass('current');
	})

	/* ==========================================================================
		Single item Tabs
		========================================================================== */
	$('ul.tabs_single li').click(function () {
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs_single li').removeClass('current');
		$('.tab-content-single').removeClass('current');

		$(this).addClass('current');
		$("#" + tab_id).addClass('current');
	})


	/* ==========================================================================
		Mobile menu button
		========================================================================== */

	$(".toggle-menu").click(function () {
		$(this).toggleClass("on");
		$(".mobile_menu").toggleClass("on").slideToggle();
		return false;
	});


	/* ==========================================================================
		Mobile dropdown menu
		========================================================================== */
	$('.sub-menu ul').hide();
	$(".sub-menu a").click(function () {
		$(this).parent(".sub-menu").children("ul").slideToggle("100");
		$(this).find(".right").toggleClass("openmenu");
	});


	/* ==========================================================================
		Mobile category dropdown menu
		========================================================================== */
	$('.categories_list').hide();
	$(".cat_list_open").click(function () {
		$(this).toggleClass("openmenu");
		$('.categories_list').toggleClass('openmenu');
	});


	/* ==========================================================================
		Desktop main tabs hover menu
		========================================================================== */
	$(function () {
		var block = false;
		var tabWait = -1;
		$('#v-nav>ul>li').mouseout(function () {
			block = true;
			setTimeout(function () {
				block = false;
			}, 100);
		});
		var items = $('#v-nav>ul>li').each(function () {
			$(this).mouseover(function () {
				var me = this;
				if (block) {
					clearTimeout(tabWait);
					tabWait = setTimeout(function () {
						$(me).mouseover();
					}, 100);
					return;
				}
				items.removeClass('current');
				$(this).addClass('current');

				$('#v-nav>div.tab-content').hide().eq(items.index($(this))).show();
			});
		});

	});

	/* ==========================================================================
		Spoiler JS
		========================================================================== */
	$('.spoiler .spoiler_title').click(function () {
		$(this).toggleClass('active').next()[$(this).next().is(':hidden') ? "slideDown" : "slideUp"](200);
	});




	/* ==========================================================================
		Files area
		========================================================================== */
	var drop = $("input");
	drop.on('dragenter', function (e) {
		$(".drop").css({
			"border": "1px dashed #000",
			"background": "rgba(0, 153, 255, .05)"
		});
		$(".cont").css({
			"color": "#09f"
		});
	}).on('dragleave dragend mouseout drop', function (e) {
		$(".drop").css({
			"border": "1px dashed #d9535f",
			"background": "transparent"
		});
		$(".cont").css({
			"color": "#8E99A5"
		});
	});



	function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object

		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {

			// Only process image files.
			if (!f.type.match('image.*')) {
				continue;
			}

			var reader = new FileReader();

			// Closure to capture the file information.
			reader.onload = (function (theFile) {
				return function (e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = ['<img class="thumb" src="', e.target.result,
                          '" title="', escape(theFile.name), '"/>'].join('');
					document.getElementById('list').insertBefore(span, null);
				};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	$('input[type="file"]').change(handleFileSelect);

	/* ==========================================================================
		Checkbox recipient
		========================================================================== */
	$(document).on('ifToggled', '.recipient_check', function (event) {
		$('.recipient_wrapper').toggleClass('open');
	});

    $.fn.hasAttr = function(name) {
        return this.attr(name) !== undefined;
    };

	/* ==========================================================================
		Quantity input
		========================================================================== */
	function wcqib_refresh_quantity_increments() {
		jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function (a, b) {
			var c = jQuery(b);
			c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
		})
	}
	String.prototype.getDecimals || (String.prototype.getDecimals = function () {
		var a = this,
			b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
		return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
	}), jQuery(document).ready(function () {
		wcqib_refresh_quantity_increments()
	}), jQuery(document).on("updated_wc_div", function () {
		wcqib_refresh_quantity_increments()
	}), jQuery(document).on("click", ".plus, .minus", function () {
		var a = jQuery(this).closest(".quantity").find(".qty"),
			b = parseFloat(a.val()),
			c = parseFloat(a.attr("max")),
			d = parseFloat(a.attr("min")),
			e = a.attr("step");
		    if(a.hasAttr('step')) {
                b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
            }
	});



	/* ==========================================================================
		Scroll to Top
		========================================================================== */
	$(window).scroll(function () {
		if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
			$('#return-to-top').fadeIn(200); // Fade in the arrow
		} else {
			$('#return-to-top').fadeOut(200); // Else fade out the arrow
		}
	});
	$('#return-to-top').click(function () { // When arrow is clicked
		$('body,html').animate({
			scrollTop: 0 // Scroll to top of body
		}, 500);
	});



	/* ==========================================================================
		Search
		========================================================================== */
	$(".navigation_area .search").click(function () {
		$(this).toggleClass("open");
		$('.search_area').toggleClass('open');
	});
	$(".close-btn").click(function () {
		$(this).toggleClass("open");
		$('.search_area').toggleClass('open');
	});


})
