function deleteCookie (cookie_name) {
    BX.setCookie('giftID', false, {expires: -1, path: "/"});
}

function validateform(form_id,email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.forms[form_id].elements[email].value;
    if(reg.test(address) == false) {
        return false;
    } else {
        return true;
    }
    return false;
}

$(document).ready(function ($) {
    $(document).on("click","div.lnr-chevron-down",function (e) {
        e.stopPropagation();
        e.preventDefault();
        return false;
    });

    $(document).on("click",".subscribe_block #subscribeform .subscribe_up",function (e) {
        if($("section.subscribe_block input[name='sf_EMAIL']").val()){

            if(validateform('subscribeform','sf_EMAIL')) {
                $("#subscribe input[name='anonym']").val($("section.subscribe_block input[name='sf_EMAIL']").val());
                $("#subscribe form input[type='submit']").click();
            } else {
                $("section.subscribe_block input[name='sf_EMAIL']").addClass("red");
                $("section.subscribe_block input[name='sf_EMAIL']").attr("placeholder","Введите корректный e-mail");
                e.stopPropagation();
                e.preventDefault();
                return false;
            }

        } else {
            $("section.subscribe_block input[name='sf_EMAIL']").addClass("red");
            $("section.subscribe_block input[name='sf_EMAIL']").attr("placeholder","Введите ваш e-mail");
            e.stopPropagation();
            e.preventDefault();
            return false;
        }
    });

    $(document).on("click","#subscribe input[type='submit']",function (e) {
        $.post($("#subscribe form").attr('action'), $("#subscribe form").serialize(), function(jsondata) {
            response = JSON.parse(jsondata);
            $("#subscribe div.forms-grid>div").html("<p>"+response.message+"</p>");
            setTimeout(location.reload(true),2000);
        });
        e.stopPropagation();
        e.preventDefault();
        return false;
    });

    $(document).on("click","div.play_video", function(){
        $("iframe.fancybox-iframe").parent().parent().parent().parent().prev().addClass("sliderbgabout");
    });

    $("div.personal_area div.fields span.bx-input-file-desc").contents().eq(0).wrap("<div class='linebreak hidden-xs'></div>");
    $("div.personal_area div.fields span.bx-input-file-desc").contents().eq(1).wrap("<div class='linebreak hidden-xs'></div>");
    $("div.personal_area div.fields span.bx-input-file-desc").contents().eq(2).wrap("<div class='linebreak hidden-xs'></div>");
    $("div.personal_area div.fields span.bx-input-file-desc").contents().eq(3).wrap("<div class='linebreak hidden-xs'></div>");
    $("div.personal_area div.fields span.bx-input-file-desc").contents().eq(4).wrap("<div class='linebreak hidden-xs'></div>");

});




